<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- xsltproc v2_to_v3.xsl alternatives-1.in.v2.xml -->

<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:xmcda="http://www.decision-deck.org/2012/XMCDA-2.2.1"
>
<xsl:output omit-xml-declaration="no" indent="yes"/>

<!-- v2 to v3 -->

<xsl:template match="/xmcda:XMCDA">
 <xmcda>
  <xsl:apply-templates select="alternatives"/>
 </xmcda>
</xsl:template>

<!-- alternatives -->
<xsl:template match="alternatives">
 <alternatives>
  <xsl:copy-of select="@*" />
  <xsl:apply-templates select="description[1]"/> 
  <xsl:apply-templates select="alternative"/>
 </alternatives>
</xsl:template>

<!-- alternative -->
<xsl:template match="alternative">
<alternative>
  <xsl:copy-of select="@*" />
    <xsl:apply-templates select="description[1]"/> 
    <xsl:apply-templates select="active"/> 
    <xsl:apply-templates select="type"/>
    <!-- reference: ignored -->
</alternative>
</xsl:template>

<!-- description -->

<xsl:template match="description">
<description>
  <xsl:copy-of select="@*" />
  <xsl:apply-templates select="author"/>
  <xsl:apply-templates select="comment"/>
  <keyword></keyword>
  <xsl:apply-templates select="bibliography"/>
  <xsl:apply-templates select="creationDate"/>
  <xsl:apply-templates select="lastModificationDate"/>
</description>
</xsl:template>

<!--
<xsl:template match="description">
  <xsl:copy>  
    <xsl:apply-templates select="@*|author|comment|bibliography|creationDate|lastModificationDate"/>  
  </xsl:copy>
</xsl:template>
-->

<!-- bibliography -->
<xsl:template match="bibliography">
<bibliography>
  <xsl:apply-templates select="description[1]"/>
  <xsl:apply-templates select="bibEntry"/>
</bibliography>
</xsl:template>

<!-- criteria -->
<xsl:template match="criteria">
 <alternatives>
  <xsl:copy-of select="@*" />
  <xsl:apply-templates select="description[1]"/> 
  <xsl:apply-templates select="criterion"/>
 </alternatives>
</xsl:template>

<!-- criterion -->
<xsl:template match="criterion">
<criterion>
  <xsl:copy-of select="@*" />
    <xsl:apply-templates select="description[1]"/> 
    <xsl:apply-templates select="active"/> 
    <xsl:apply-templates select="type"/>
    <!-- reference: ignored -->
</criterion>
</xsl:template>


<!-- catch-all: identity -->
<xsl:template match="@*|node()">  
	<xsl:copy>  
		<xsl:apply-templates select="@*|node()"/>  
    </xsl:copy>  
</xsl:template>


</xsl:stylesheet>
