package org.xmcda;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import javax.xml.bind.JAXBElement;

import org.junit.Before;
import org.junit.Test;
import org.xmcda.converters.v2_v3.XMCDAConverter;

public class Test_CategoriesValues
{
	XMCDA xmcda;
	CategoriesValues<Integer> CategoriesValues = null;

	@Before
	public void init()
	{
		xmcda = new XMCDA();
		CategoriesValues = new CategoriesValues<>();
		xmcda.categoriesValuesList.add(CategoriesValues);
	}

	public org.xmcda.v2.CategoryValue categoryValue(org.xmcda.v2.XMCDA xmcda_v2)
	{
		org.xmcda.v2.CategoriesValues CategoriesValues_v2 = null;
		for (JAXBElement<?> element: xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters())
		{
			if (element.getValue() instanceof org.xmcda.v2.CategoriesValues)
				CategoriesValues_v2 = (org.xmcda.v2.CategoriesValues) element.getValue();
		}
		assertNotNull(CategoriesValues_v2);
		assertEquals(1, CategoriesValues_v2.getCategoryValue().size());

		return CategoriesValues_v2.getCategoryValue().get(0);
	}

	@Test
	public void testToV2WithOrWithoutValues()
	{
		Category a1 = xmcda.categories.get("a01", true);
		CategoriesValues.put(a1, 42);
		org.xmcda.v2.XMCDA xmcda_v2;

		// 1 value, no <values>
		XMCDAConverter.toV2Parameters.get().setOmitValuesInCategoriesValues(true);
		xmcda_v2 = XMCDAConverter.convertTo_v2(xmcda);

		org.xmcda.v2.CategoryValue categoryValue_v2 = categoryValue(xmcda_v2);
		assertEquals(1, categoryValue_v2.getValueOrValues().size());
		assertTrue(categoryValue_v2.getValueOrValues().get(0) instanceof org.xmcda.v2.Value);

		// 2 values, no <values>
		CategoriesValues.clear();
		CategoriesValues.put(a1, 42);
		CategoriesValues.get(a1).add(new QualifiedValue<>(77));
		XMCDAConverter.toV2Parameters.get().setOmitValuesInCategoriesValues(true);
		xmcda_v2 = XMCDAConverter.convertTo_v2(xmcda);

		categoryValue_v2 = categoryValue(xmcda_v2);
		assertEquals(2, categoryValue_v2.getValueOrValues().size());
		assertTrue(categoryValue_v2.getValueOrValues().get(0) instanceof org.xmcda.v2.Value);
		assertTrue(categoryValue_v2.getValueOrValues().get(1) instanceof org.xmcda.v2.Value);

		// 1 value, with <values>
		CategoriesValues.clear();
		CategoriesValues.put(a1, 42);
		XMCDAConverter.toV2Parameters.get().setOmitValuesInCategoriesValues(false);
		xmcda_v2 = XMCDAConverter.convertTo_v2(xmcda);

		categoryValue_v2 = categoryValue(xmcda_v2);
		assertEquals(1, categoryValue_v2.getValueOrValues().size());
		assertTrue(categoryValue_v2.getValueOrValues().get(0) instanceof org.xmcda.v2.Values);
		assertEquals(1, ((org.xmcda.v2.Values) categoryValue_v2.getValueOrValues().get(0)).getValue().size());

		// 2 values, with <values>
		CategoriesValues.clear();
		CategoriesValues.put(a1, 42);
		CategoriesValues.get(a1).add(new QualifiedValue<>(77));
		XMCDAConverter.toV2Parameters.get().setOmitValuesInCategoriesValues(false);
		xmcda_v2 = XMCDAConverter.convertTo_v2(xmcda);

		categoryValue_v2 = categoryValue(xmcda_v2);
		assertEquals(1, categoryValue_v2.getValueOrValues().size());
		assertTrue(categoryValue_v2.getValueOrValues().get(0) instanceof org.xmcda.v2.Values);
		assertEquals(2, ((org.xmcda.v2.Values) categoryValue_v2.getValueOrValues().get(0)).getValue().size());
	}

}
