package org.xmcda;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import javax.xml.bind.JAXBElement;

import org.junit.Before;
import org.junit.Test;
import org.xmcda.converters.v2_v3.XMCDAConverter;

public class Test_CriteriaValues
{
	XMCDA xmcda;
	CriteriaValues<Integer> CriteriaValues = null;

	@Before
	public void init()
	{
		xmcda = new XMCDA();
		CriteriaValues = new CriteriaValues<>();
		xmcda.criteriaValuesList.add(CriteriaValues);
	}

	public org.xmcda.v2.CriterionValue criterionValue(org.xmcda.v2.XMCDA xmcda_v2)
	{
		org.xmcda.v2.CriteriaValues CriteriaValues_v2 = null;
		for (JAXBElement<?> element: xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters())
		{
			if (element.getValue() instanceof org.xmcda.v2.CriteriaValues)
				CriteriaValues_v2 = (org.xmcda.v2.CriteriaValues) element.getValue();
		}
		assertNotNull(CriteriaValues_v2);
		assertEquals(1, CriteriaValues_v2.getCriterionValue().size());

		return CriteriaValues_v2.getCriterionValue().get(0);
	}

	@Test
	public void testToV2WithOrWithoutValues()
	{
		Criterion a1 = xmcda.criteria.get("a01", true);
		CriteriaValues.put(a1, 42);
		org.xmcda.v2.XMCDA xmcda_v2;

		// 1 value, no <values>
		XMCDAConverter.toV2Parameters.get().setOmitValuesInCriteriaValues(true);
		xmcda_v2 = XMCDAConverter.convertTo_v2(xmcda);

		org.xmcda.v2.CriterionValue criterionValue_v2 = criterionValue(xmcda_v2);
		assertEquals(1, criterionValue_v2.getValueOrValues().size());
		assertTrue(criterionValue_v2.getValueOrValues().get(0) instanceof org.xmcda.v2.Value);

		// 2 values, no <values>
		CriteriaValues.clear();
		CriteriaValues.put(a1, 42);
		CriteriaValues.get(a1).add(new QualifiedValue<>(77));
		XMCDAConverter.toV2Parameters.get().setOmitValuesInCriteriaValues(true);
		xmcda_v2 = XMCDAConverter.convertTo_v2(xmcda);

		criterionValue_v2 = criterionValue(xmcda_v2);
		assertEquals(2, criterionValue_v2.getValueOrValues().size());
		assertTrue(criterionValue_v2.getValueOrValues().get(0) instanceof org.xmcda.v2.Value);
		assertTrue(criterionValue_v2.getValueOrValues().get(1) instanceof org.xmcda.v2.Value);

		// 1 value, with <values>
		CriteriaValues.clear();
		CriteriaValues.put(a1, 42);
		XMCDAConverter.toV2Parameters.get().setOmitValuesInCriteriaValues(false);
		xmcda_v2 = XMCDAConverter.convertTo_v2(xmcda);

		criterionValue_v2 = criterionValue(xmcda_v2);
		assertEquals(1, criterionValue_v2.getValueOrValues().size());
		assertTrue(criterionValue_v2.getValueOrValues().get(0) instanceof org.xmcda.v2.Values);
		assertEquals(1, ((org.xmcda.v2.Values) criterionValue_v2.getValueOrValues().get(0)).getValue().size());

		// 2 values, with <values>
		CriteriaValues.clear();
		CriteriaValues.put(a1, 42);
		CriteriaValues.get(a1).add(new QualifiedValue<>(77));
		XMCDAConverter.toV2Parameters.get().setOmitValuesInCriteriaValues(false);
		xmcda_v2 = XMCDAConverter.convertTo_v2(xmcda);

		criterionValue_v2 = criterionValue(xmcda_v2);
		assertEquals(1, criterionValue_v2.getValueOrValues().size());
		assertTrue(criterionValue_v2.getValueOrValues().get(0) instanceof org.xmcda.v2.Values);
		assertEquals(2, ((org.xmcda.v2.Values) criterionValue_v2.getValueOrValues().get(0)).getValue().size());
	}

}
