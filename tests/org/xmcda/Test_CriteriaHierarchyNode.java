package org.xmcda;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

public class Test_CriteriaHierarchyNode
{
	CriterionHierarchyNode n1 = null;

	CriterionHierarchyNode design_node()
	{
		CriterionHierarchyNode node = new CriterionHierarchyNode(new Criterion("design"));
		node.addChild(new Criterion("graphic"));
		node.addChild(new Criterion("brand"));

		node.getChild("graphic").addChild(new Criterion("color"));
		node.getChild("graphic").addChild(new Criterion("shape"));
		return node;
	}

	@Before
	public void init()
	{
		n1 = new CriterionHierarchyNode(new Criterion("c1"));
	}

	@Test(expected=NullPointerException.class)
	public void test_constructor_null_parameter()
	{
		new CriterionHierarchyNode(null);
	}

	@Test
	public void test_non_null_empty_children()
	{
		assertNotNull(n1.getChildren().size());
		assertEquals(0, n1.getChildren().size());
	}

	@Test
	public void test_addChild_n_getChildren()
	{
		Criterion c11 = new Criterion("c11");
		Criterion c12 = new Criterion("c12");

		assertTrue(n1.addChild(c11));
		assertTrue(n1.addChild(c12));

		// adding a node with an already registered criterion id fails
		assertFalse(n1.addChild(c12));
		assertFalse(n1.addChild(new Criterion("c12")));

		// check that both are there
		assertEquals(2, n1.getChildren().size());
		Set<Criterion> children_criteria =
				n1.getChildren().stream().map(node->node.getCriterion()).collect(Collectors.toSet());
		assertTrue(children_criteria.contains(c11));
		assertTrue(children_criteria.contains(c12));
	}

	@Test
	public void test_removeChild()
	{
		n1.addChild(new Criterion("c11"));
		n1.addChild(new Criterion("c12"));
		n1.addChild(new Criterion("c13"));

		assertFalse(n1.removeChild(new Criterion("cXX")));
		assertEquals(3, n1.getChildren().size());

		assertTrue(n1.removeChild(new Criterion("c12")));
		assertEquals(2, n1.getChildren().size());
	}

	@Test
	public void test_getChild()
	{
		n1.addChild(new Criterion("c11"));
		n1.addChild(new Criterion("c12"));
		assertEquals("c11", n1.getChild("c11").getCriterion().id());
		assertNull(n1.getChild("cXX"));
	}

	@Test(expected=UnsupportedOperationException.class)
	public void test_change_getChildren_unmodifiable()
	{
		new CriterionHierarchyNode().getChildren().add(n1);
	}

	@Test
	public void test_change_getChildren_hierarchy()
	{		
		// when modifying getChildren() hierarchy, the original node is changed as well
		CriterionHierarchyNode design = design_node();
		Set<CriterionHierarchyNode> design_children = design.getChildren();

		CriterionHierarchyNode brand = design_children.stream().filter(node -> node.getCriterion().id().equals("brand"))
		        .findFirst().orElse(null);
		assertTrue(brand.addChild(new Criterion("foo")));
		assertTrue(brand.addChild(new Criterion("bar")));
		
		assertEquals(2, design.getChild("brand").getChildren().size());
	}

	@Test
	public void test_deepCopy()
	{		
		// when modifying a deepCopy() hierarchy, the original node is unchanged
		CriterionHierarchyNode design = design_node();
		CriterionHierarchyNode design_copy = design.deepCopy();

		// add nodes to the copy's child node "brand"
		assertTrue(design_copy.getChild("brand").addChild(new Criterion("foo")));
		assertTrue(design_copy.getChild("brand").addChild(new Criterion("bar")));
		
		assertEquals(0, design.getChild("brand").getChildren().size()); // unchanged, no child
		
		// Modify the copy's immediate children
		assertTrue(design_copy.removeChild(new Criterion("graphic")));
		assertNotNull(design.getChild("graphic")); // original unchanged.
	}

}
