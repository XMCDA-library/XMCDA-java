package org.xmcda.converters.v2_v3;

import static org.junit.Assert.fail;

import java.util.Arrays;

import org.junit.Test;
import org.xmcda.CriteriaHierarchy;
import org.xmcda.CriteriaSetsHierarchy;

/**
 * Tests generic behavior of the HierarchyConverter. See the more specific tests (@link
 * {@link Test_CriteriaHierarchyConverter}, {@link Test_CriteriaSetsHierarchyConverter}) for tests on "real"
 * conversions.
 * 
 * @author Sébastien Bigaret
 */
public class Test_HierarchyConverter
{
	private void assertConvertTo_v2RaisesIllegalArgumentExceptionWith(Object... objects)
	{
		try {
			new HierarchyConverter().convertTo_v2(Arrays.asList(objects), new org.xmcda.v2.XMCDA());
			fail("Expected an IllegalArgumentException to be thrown");
		} catch (IllegalArgumentException exception) { /* left intentionally empty */ }
	}
	@Test
	public void test_convertTo_v2()
	{
		assertConvertTo_v2RaisesIllegalArgumentExceptionWith("a");
		assertConvertTo_v2RaisesIllegalArgumentExceptionWith(new CriteriaHierarchy(), new CriteriaSetsHierarchy());
		assertConvertTo_v2RaisesIllegalArgumentExceptionWith(new CriteriaHierarchy(), "string");
		assertConvertTo_v2RaisesIllegalArgumentExceptionWith(new CriteriaSetsHierarchy(), "string");
	}

}
