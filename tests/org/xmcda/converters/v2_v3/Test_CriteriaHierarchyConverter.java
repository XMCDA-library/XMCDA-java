package org.xmcda.converters.v2_v3;

import static org.hamcrest.core.Is.*;
import static org.junit.Assert.*;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.List;
import java.util.function.BiFunction;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.xmcda.CriteriaHierarchy;
import org.xmcda.CriterionHierarchyNode;
import org.xmcda.parsers.xml.xmcda_v2.TestUtils;
import org.xmcda.v2.XMCDA;
import org.xml.sax.SAXException;

public class Test_CriteriaHierarchyConverter
{
	private final String xmcda_v2_header = 
			"<xmcda:XMCDA" +
			"  xsi:schemaLocation=\"http://www.decision-deck.org/2012/XMCDA-2.2.1 http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.2.1.xsd\"\n" + 
			"  xmlns:xmcda=\"http://www.decision-deck.org/2012/XMCDA-2.2.1\"\n" +
			"  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">";

	private final String xmcda_v2_footer = "</xmcda:XMCDA>";
	private final String hierarchy_1 = xmcda_v2_header +
			"	<hierarchy>\n" + 
			"		<node id=\"nodes\">\n" + 
			"			<node id=\"nodes1\">\n" + 
			"				<node id=\"nodes11\">\n" + 
			"					<criterionID>c01</criterionID>\n" + 
			"				</node>\n" + 
			"				<node id=\"nodes12\">\n" + 
			"					<criterionID>c02</criterionID>\n" + 
			"				</node>\n" + 
			"			</node>\n" + 
			"			<node id=\"nodes2\">\n" + 
			"				<criterionID>c03</criterionID>\n" + 
			"			</node>\n" + 
			"		</node>\n" + 
			"		<node id=\"nodes\">\n" + 
			"			<criterionID>c04</criterionID>\n" + 
			"		</node>" +
			"	</hierarchy>\n" + 
			xmcda_v2_footer;

	@Test
	public void testConvertTo_v3HierarchyXMCDA() throws UnsupportedEncodingException, IOException, JAXBException, SAXException
	{
		XMCDA xmcda_v2 = TestUtils.readXMCDA(hierarchy_1);
		//Hierarchy hierarchy_v2 = (Hierarchy) xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters().get(0).getValue();
		//new CriteriaHierarchyConverter().convertTo_v3(hierarchy_v2, xmcda_v3);

		org.xmcda.XMCDA xmcda_v3 = XMCDAConverter.convertTo_v3(xmcda_v2);
		assertEquals(6, xmcda_v3.criteria.size());
		assertEquals(1, xmcda_v3.criteriaHierarchiesList.size());
		CriteriaHierarchy hierarchy = xmcda_v3.criteriaHierarchiesList.get(0);
		assertEquals(2, hierarchy.getRootNodes().size());

		Iterator<CriterionHierarchyNode> rootNodes = hierarchy.getRootNodes().iterator();

		CriterionHierarchyNode firstNode = rootNodes.next();
		assertNotNull(firstNode);
		assertEquals("hierarchy criterion nodes", firstNode.getCriterion().id()); // a criterion was created with the (v2) node's id

		// now just check the structure
		assertEquals(2, firstNode.getChildren().size());
		assertEquals(2, firstNode.getChild("hierarchy criterion nodes1").getChildren().size());

		assertNull(firstNode.getChild("hierarchy criterion nodes2"));
		assertNotNull(firstNode.getChild("c03"));
		assertEquals(0, firstNode.getChild("c03").getChildren().size());

		CriterionHierarchyNode secondNode = rootNodes.next();
		assertEquals("c04", secondNode.getCriterion().id());

		assertFalse(rootNodes.hasNext());
	}
	
	private final String invalid_hierarchy_1 = xmcda_v2_header +
			"	<hierarchy>\n" + 
			"		<node>\n" + 
			"			<criterionID>c01</criterionID>\n" + 
			"		</node>\n" + 
			"		<node>\n" + 
			"			<alternativeID>c02</alternativeID>\n" + 
			"		</node>\n" + 
			"	</hierarchy>\n" + 
			xmcda_v2_footer;
	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public void test_invalid() throws UnsupportedEncodingException, IOException, JAXBException, SAXException
	{
		try
		{
			XMCDA xmcda_v2 = TestUtils.readXMCDA(invalid_hierarchy_1);
			XMCDAConverter.convertTo_v3(xmcda_v2);
			fail("Expected an IllegalArgumentException to be thrown");
		} catch (IllegalArgumentException exception) {
			assertThat(exception.getMessage(), is("XMCDA v2 hierarchy mixes alternatives, criteria and/or categories"));
		}
	}

	private static org.xmcda.XMCDA getHierarchy_1()
	{
		org.xmcda.XMCDA xmcda = new org.xmcda.XMCDA();
		CriteriaHierarchy hierarchy_1 = new CriteriaHierarchy();
		xmcda.criteriaHierarchiesList.add(hierarchy_1);

		org.xmcda.Description d = new org.xmcda.Description();
		d.setComment("cH1 comment");
		hierarchy_1.setDescription(d);

		hierarchy_1.setId("cH1");
		hierarchy_1.setName("hierarchy 1");
		hierarchy_1.setMcdaConcept("cH1 mcdaConcept");

		hierarchy_1.addChild(xmcda.criteria.get("design", true));
		CriterionHierarchyNode node = hierarchy_1.getChild("design");
		node.addChild(xmcda.criteria.get("shape", true));
		node.addChild(xmcda.criteria.get("brand", true));

		hierarchy_1.addChild(xmcda.criteria.get("price", true));
		return xmcda;
	}

	/**
	 * Checks the criteria hierarchy returned by {@link #getHierarchy_1()} after it has been converted to xmcda v2
	 * 
	 * @param hierarchy the criteria hierarchy to be checked
	 */
	private static void checkHierarchy_1(org.xmcda.v2.XMCDA xmcda_v2)
	{
		// 2 created: criteria & hierarchy
		assertEquals(2, xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters().size());
		org.xmcda.v2.Criteria criteria = null;
		org.xmcda.v2.Hierarchy hierarchy = null;
		Iterator<JAXBElement<?>> iterator = xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters().iterator();
		Object o = iterator.next().getValue();
		if (o instanceof org.xmcda.v2.Criteria) criteria = (org.xmcda.v2.Criteria) o;
		else hierarchy = (org.xmcda.v2.Hierarchy) o;

		o = iterator.next().getValue();
		if (o instanceof org.xmcda.v2.Criteria) criteria = (org.xmcda.v2.Criteria) o;
		else hierarchy = (org.xmcda.v2.Hierarchy) o;
		
		assertEquals("cH1", hierarchy.getId());
		assertEquals("hierarchy 1", hierarchy.getName());
		assertEquals("cH1 mcdaConcept", hierarchy.getMcdaConcept());

		//assertEquals("cH1 comment", hierarchy.getDescription());
		
		assertEquals(4, criteria.getCriterion().size());
		assertEquals(2, hierarchy.getNode().size());
		
		BiFunction<List<org.xmcda.v2.Node>, String, org.xmcda.v2.Node> getNode = 
				(List<org.xmcda.v2.Node> nodes, String id) -> nodes.stream().filter(n -> id.equals(n.getCriterionID())).findFirst().orElse(null); 

		org.xmcda.v2.Node design = getNode.apply(hierarchy.getNode(), "design");
		assertNotNull(design);
		assertEquals(2, design.getNode().size());

		org.xmcda.v2.Node shape = getNode.apply(design.getNode(), "shape");
		org.xmcda.v2.Node brand = getNode.apply(design.getNode(), "brand");

		assertNotNull(shape);
		assertNotNull(brand);
		assertTrue(shape.getNode().isEmpty()); // shape has no child
		assertTrue(brand.getNode().isEmpty()); // brand has no child

		org.xmcda.v2.Node price = getNode.apply(hierarchy.getNode(), "price");
		assertNotNull(price);
		assertTrue(price.getNode().isEmpty());
	}

	@Test
	public void testConvertTo_v2()
	{
		XMCDA xmcda_v2 = XMCDAConverter.convertTo_v2(getHierarchy_1());
		checkHierarchy_1(xmcda_v2);
	}
}
