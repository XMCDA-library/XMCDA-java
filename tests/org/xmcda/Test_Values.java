package org.xmcda;

import static org.junit.Assert.assertEquals;

/**
 * @author Sébastien Bigaret <sebastien.bigaret@telecom-bretagne.eu>
 */
public class Test_Values
{
	@org.junit.Test
	public void test_TAG()
	{
		String xml="<values>\n" + 
			"	<value>\n" + 
			"		<label></label>\n" + 
			"		</value>\n" + 
			"</values>\n";
		//values_v2 = 
		assertEquals("alternatives", Alternatives.TAG);
	}

}
