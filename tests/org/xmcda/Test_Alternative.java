package org.xmcda;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class Test_Alternative
{
	Alternative a1 = null;

	@Before
	public void init()
	{
		a1 = new Alternative("a01");
	}
	

	@Test
	public void test_container()
	{
		a1.setId("another id"); // should not fail
	}

	@Test(expected=IllegalStateException.class)
	public void id_immutable_when_in_collection()
	{
		Alternatives alternatives = new Alternatives();
		alternatives.add(a1);
		a1.setId("should raise");
	}

	@Test(expected=IllegalStateException.class)
	public void id_immutable_when_in_collection_2()
	{
		Alternatives alternatives = new Alternatives();
		HashSet<Alternative> set = new HashSet<Alternative>();
		set.add(a1);
		alternatives.addAll(set);
		a1.setId("should raise");
	}

	@Test
	public void remove_resets_container()
	{
		Alternatives alternatives = new Alternatives();
		alternatives.add(a1);
		assertEquals(alternatives, a1.getContainer());
		assertTrue(alternatives.remove(a1));
		assertNull(a1.getContainer());
		a1.setId("should not raise");
	}

	@Test
	public void retain_resets_container()
	{
		Alternatives alternatives = new Alternatives();
		Set<Alternative> set = new HashSet<Alternative>();
		set.add(a1);
		alternatives.add(new Alternative("a2"));
		alternatives.retainAll(set);
		assertEquals(0, alternatives.size());
		assertNull(a1.getContainer());
		a1.setId("should not raise");
	}

}
