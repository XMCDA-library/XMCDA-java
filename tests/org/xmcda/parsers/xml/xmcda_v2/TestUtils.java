package org.xmcda.parsers.xml.xmcda_v2;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import javax.xml.bind.JAXBException;

import org.xmcda.v2.XMCDA;
import org.xml.sax.SAXException;

public abstract class TestUtils
{
	public static XMCDA readXMCDA(String xmlString) throws UnsupportedEncodingException, IOException, JAXBException, SAXException
	{
		return XMCDAParser.readXMCDA(new ByteArrayInputStream(xmlString.getBytes(StandardCharsets.UTF_8)));
	}
}
