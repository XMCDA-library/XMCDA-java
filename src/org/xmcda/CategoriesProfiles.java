package org.xmcda;

import java.util.ArrayList;


/**
 * @author Sébastien Bigaret
 */
public class CategoriesProfiles <VALUE_TYPE>
    extends ArrayList<CategoryProfile<VALUE_TYPE>>
    implements HasDescription, CommonAttributes, XMCDARootElement
{
	private static final long serialVersionUID = 1L;

	public static final String TAG= "categoriesProfiles";

	public CategoriesProfiles()
	{ super(); }

	public CategoryProfile<VALUE_TYPE> get(String id)
	{
		for (CategoryProfile<VALUE_TYPE> categoryProfile: this)
			if (categoryProfile.id().equals(id))
			    return categoryProfile;
		return null;
	}

	public ArrayList<String> getIDs()
	{
		ArrayList<String> ids = new ArrayList<String>();
		for (CategoryProfile<VALUE_TYPE> categoryProfile: this)
			ids.add(categoryProfile.id());
		return ids;
	}

	public ArrayList<String> getCategoriesProfilesNames()
	{
		ArrayList<String> names = new ArrayList<String>();
		for (CategoryProfile<VALUE_TYPE> categoryProfile: this)
			names.add(categoryProfile.name());
		return names;
	}

	@Override
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append("[");
		for (CategoryProfile<VALUE_TYPE> categoryProfile: this)
			sb.append(categoryProfile.toString()).append(", ");
		sb.append("]");
		return sb.toString();
	}

	public void printCategoriesProfiles()
	{
		System.out.println("This are the categoriesProfiles:");
		for (int i = 0; i < this.size(); i++)
		{
			System.out.println("CategoryProfile" + ( i + 1 ) + ":\t" + "id = " + this.get(i).id() + "\t, name = "
			                   + this.get(i).name());
		}
	}

	// CommonAttributes (start)

	/** The id attribute allows to identify the underlying piece of data by a program. */
	private String            id;

	/** The name attribute contains the human readable name of the object or concept. */
	private String            name;

	/**
	 * The mcdaConcept attribute allows to specify to what mcda concept a tag is related. It is used by an algorithm to
	 * make choices which will have an influence on the output. The documentation of the program should therefore
	 * specify, if necessary, what mcdaConcept should be used for the input data. In particular, if an algorithm
	 * requires, among other things, twice the same input tag, they can be differenciated by the mcdaConcept (this is
	 * even mandatory if they are present in the same file, but should be optional if the two tags can be in different
	 * input files, or originate from two different programs). The algorithm should therefore not be too strict on these
	 * mcdaConcepts, as this will reduce the compatibility between the various programs.
	 */
	private String            mcdaConcept;

	public String id()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String name()
	{
		return name;
	}

	public String mcdaConcept()
	{
		return mcdaConcept;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setMcdaConcept(String mcdaConcept)
	{
		this.mcdaConcept = mcdaConcept;
	}

	// CommonAttributes (end)

	// HasDescription (start)

	private Description description;
	public void setDescription(Description description)
	{
		this.description = description;
	}

	public Description getDescription()
	{
		return description;
	}

	// HasDescription (end)

}
