package org.xmcda;

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.StringJoiner;

/**
 * A node of a hierarchy of criteria, representing a {@link #getCriterion() criterion} and its {@link #getChildren()}.
 * 
 * @author Sébastien Bigaret
 */
public class CriterionHierarchyNode
    implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	/** The criterion attached to this node. */
	private final Criterion criterion;

	private final Set<CriterionHierarchyNode> nodes = new LinkedHashSet<>();
	
	public static final String TAG = "criteriaHierarchy";

	/**
	 * Builds a new node.
	 * 
	 * @param aCriterion the criterion attached to the new node
	 * @throw {@link NullPointerException} if aCriterion is {@code null}.
	 */
	public CriterionHierarchyNode(Criterion aCriterion)
	{
		if ( aCriterion == null )
			throw new NullPointerException("Parameter aCriterion should not be null");
		this.criterion = aCriterion;
	}

	/**
	 * Builds a root node.  This is used by {@link CriteriaHierarchy} to create the "super" node containing all
	 * the root nodes. A node object created by this method must not be exposed through the public API since
	 * the public API expects a node's criterion to be non-null.
	 */
	CriterionHierarchyNode()
	{
		this.criterion = null;
	}

	/**
	 * Returns the criterion attached to this node.
	 * @return the criterion for this node.
	 */
	public Criterion getCriterion()
	{
		return criterion;
	}

	/**
	 * Adds a child to this node if its criterion is not already registered as a child.
	 * @param aNode the node to be added
	 * @return true if this node did not already contain the specified node's criterion in its children.
	 */
	public boolean addChild(CriterionHierarchyNode aNode)
	{
		if (nodes.stream().anyMatch((node) -> node.getCriterion().equals(aNode.getCriterion())))
			return false;
		return nodes.add(aNode);
	}

	/**
	 * Adds a child to this node if the criterion is not already registered as a child.
	 * @param aNode the node to be added
	 * @return true if the node did not already contain the criterion in its children.
	 */
	public boolean addChild(Criterion aCriterion)
	{
		return this.addChild(new CriterionHierarchyNode(aCriterion));
	}
	
	/**
	 * Removes a child from this node's children.
	 * 
	 * @param aChild the node to be removed.
	 * @return {@code true} if the element was removed, {@code false} if it was not in this node's children.
	 */
	public boolean removeChild(CriterionHierarchyNode aChild)
	{
		return nodes.remove(aChild);
	}

	/**
	 * Removes from this node's children the child whose criterion is the one supplied.
	 * 
	 * @param aChild the criterion for the node to be removed.
	 * @return {@code true} if the corresponding child could be found and removed.
	 */
	public boolean removeChild(Criterion aCriterion)
	{
		return nodes.removeIf((node) -> node.criterion.equals(aCriterion));
	}

	/**
	 * Returns the children of this node. The returned set is unmodifiable, however if you {@link #addChild(Criterion)
	 * add} or {@link #removeChild(Criterion) remove} children from one of the returned set's nodes, this will
	 * directly change the sub-hierarchy held by this node as well. If you want to manipulate the hierarchy of the
	 * returned nodes without affecting this node's hierarchy, call {@link #deepCopy()} on this object and then call
	 * this method on the copy instead.
	 * 
	 * @return this node's children.
	 */
	public Set<CriterionHierarchyNode> getChildren()
	{
		return Collections.unmodifiableSet(nodes);
	}

	/**
	 * Returns the child node for the supplied criterion.
	 * 
	 * @param aCriterion the criterion to search in this node's children
	 * @return the corresponding child node, or {@code null} if no such child could be found.
	 */
	public CriterionHierarchyNode getChild(Criterion aCriterion)
	{
		return nodes.stream().filter((node) -> node.getCriterion().equals(aCriterion)).findFirst().orElse(null);
	}

	/**
	 * Returns the child node with the supplied criterion ID.
	 * 
	 * @param aCriterionID the ID of a criterion to search in this node's children
	 * @return the corresponding child node, or {@code null} if no such child could be found.
	 */
	public CriterionHierarchyNode getChild(String aCriterionID)
	{
		return nodes.stream().filter((node) -> node.getCriterion().id().equals(aCriterionID)).findFirst().orElse(null);
	}

	public boolean isLeaf()
	{
		return nodes.isEmpty();
	}

	/**
	 * Returns a deep copy of this node. The returned node and its children, and their children etc. can then be
	 * modified without affecting this node and the hierarchy underneath.
	 * 
	 * @return a deep copy of this node.
	 */
	public CriterionHierarchyNode deepCopy()
	{
		CriterionHierarchyNode copy = new CriterionHierarchyNode(this.criterion);
		this.nodes.stream().forEachOrdered(node -> copy.nodes.add((CriterionHierarchyNode)node.deepCopy()));
		return copy;
	}

	@Override
	public String toString()
	{
		return toString(":<", ">");
	}
	
	protected String toString(String start, String end)
	{
		StringBuffer sb = new StringBuffer();
		if (criterion!=null) // it is the case when called from CriteriaHierarchy.toString()
			sb.append(criterion.id());
		StringJoiner sj = new StringJoiner(",", start, end).setEmptyValue("");
		getChildren().stream().forEachOrdered(node -> sj.add(node.toString()));
		sb.append(sj.toString());
		return sb.toString();
	}
}
