package org.xmcda;

public class CriteriaSetsLinearConstraints <CRITERIA_SET_VALUE_TYPE, VALUE_TYPE>
    extends LinearConstraints<CriteriaSet<CRITERIA_SET_VALUE_TYPE>, VALUE_TYPE>
	implements XMCDARootElement
{
	private static final long     serialVersionUID = 1L;

	public static final String TAG = "criteriaSetsLinearConstraints";

	public CriteriaSetsLinearConstraints()
	{ super(); }

}
