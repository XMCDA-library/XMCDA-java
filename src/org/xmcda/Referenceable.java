package org.xmcda;

public interface Referenceable
    extends CommonAttributes
{
	/**
	 * A class implements this interface when it is interested in being notified when a Referenceable object is created.
	 */
	public static interface CreationObserver
	{
		/**
		 * Called when a Referenceable object is created.
		 * @param object the object that has just been created.
		 */
		public void objectCreated(Referenceable object);
	}

	/**
	 * The default creation observer used by {@link Alternative}, {@link Category}, {@link Criterion},
	 * {@link AlternativesSet}, {@link CategoriesSet} and {@link CriteriaSet}.
	 */
	public static class DefaultCreationObserver implements CreationObserver
	{
		public static Object currentMarker = null;

		public void objectCreated(Referenceable object)
		{
			object.setMarker(currentMarker);
		}
	}

	public static final DefaultCreationObserver defaultCreationObserver = new DefaultCreationObserver();

	/** id can be modified when the object is not in a {@link ReferenceableContainer container} */
	public void setId(String id);

	public void merge(Referenceable object);

	public void setContainer(Object container);

	public void setMarker(Object marker);

	public Object getMarker();

}
