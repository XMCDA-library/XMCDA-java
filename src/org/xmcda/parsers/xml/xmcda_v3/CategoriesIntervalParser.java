package org.xmcda.parsers.xml.xmcda_v3;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.CategoriesInterval;
import org.xmcda.Factory;
import org.xmcda.XMCDA;

/**
 * @author Sébastien Bigaret
 */
public class CategoriesIntervalParser
{
	public static final String CATEGORIES_INTERVAL = "categoriesInterval";

	public static final String LOWER_BOUND         = "lowerBound";

	public static final String UPPER_BOUND         = "upperBound";

	public static final String CATEGORY_ID         = "categoryID";

	public CategoriesInterval fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader) throws XMLStreamException
	{
		CategoriesInterval categoriesInterval = Factory.categoriesInterval();
		new CommonAttributesParser().handleAttributes(categoriesInterval, startElement);
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (CATEGORIES_INTERVAL.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				categoriesInterval.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (LOWER_BOUND.equals(startElement.getName().getLocalPart()))
			{
				// get <categoryID>
				startElement = Utils.getNextStartElement(startElement, eventReader);
				categoriesInterval.setLowerBound(xmcda.categories.get(Utils.getTextContent(startElement, eventReader)));
			}
			if (UPPER_BOUND.equals(startElement.getName().getLocalPart()))
			{
				// get <categoryID>
				startElement = Utils.getNextStartElement(startElement, eventReader);
				categoriesInterval.setUpperBound(xmcda.categories.get(Utils.getTextContent(startElement, eventReader)));
			}
		}
		return categoriesInterval;
	}

	public void toXML(CategoriesInterval categoryInterval, XMLStreamWriter writer) throws XMLStreamException
	{
		if (categoryInterval == null)
		    return;
		writer.writeStartElement(CATEGORIES_INTERVAL);
		new CommonAttributesParser().toXML(categoryInterval, writer);
		writer.writeln();
		new DescriptionParser().toXML(categoryInterval.getDescription(), writer);

		if (categoryInterval.getLowerBound() != null)
		{
			writer.writeStartElement(LOWER_BOUND);
			writer.writeln();
			writer.writeElementChars(CATEGORY_ID, categoryInterval.getLowerBound().id());
			writer.writeEndElement(); // LOWER_BOUND
			writer.writeln();
		}
		if (categoryInterval.getUpperBound() != null)
		{
			writer.writeStartElement(UPPER_BOUND);
			writer.writeln();
			writer.writeElementChars(CATEGORY_ID, categoryInterval.getUpperBound().id());
			writer.writeEndElement(); // UPPER_BOUND
			writer.writeln();
		}

		writer.writeEndElement(); // CATEGORIES_INTERVAL
		writer.writeln();
	}
}
