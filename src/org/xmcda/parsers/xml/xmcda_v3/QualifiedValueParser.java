/**
 *
 */
package org.xmcda.parsers.xml.xmcda_v3;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.QualifiedValue;
import org.xmcda.XMCDA;

/**
 * @author Sébastien Bigaret
 *
 */
public class QualifiedValueParser<VALUE_TYPE>
{
	public static final String VALUE = "value";

	public QualifiedValue<VALUE_TYPE> fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader) throws XMLStreamException
	{
		// will be used to fill in the common attributes into the value after it is built
		StartElement initialElement = startElement;

		// may be value or lowerBound or upperBound
		final String initialTag = startElement.getName().getLocalPart();

		QualifiedValue<VALUE_TYPE> qvalue = new QualifiedValue<>(null);

		while (eventReader.hasNext())
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
				if (initialTag.equals(event.asEndElement().getName().getLocalPart()))
					break;

			if ( qvalue.getValue() != null ) // already got it, just wait for the event for </value>
				continue;
			if ( ! event.isStartElement())
				continue;

			startElement = event.asStartElement();

			qvalue.setValue(new ValueParser<VALUE_TYPE>().fromXML(xmcda, startElement, eventReader));
		}
		new CommonAttributesParser().handleAttributes(qvalue, initialElement);
		return qvalue;
	}

	// tag; value, abscissa, ordinate
	public void toXML(String tag, QualifiedValue<VALUE_TYPE> qvalue, XMLStreamWriter writer) throws XMLStreamException
	{
		if (qvalue == null)
			return; // TODO normal ça?

		writer.writeStartElement(tag);
		new CommonAttributesParser().toXML(qvalue, writer);
		writer.writeln();
		new ValueParser<VALUE_TYPE>().toXML(qvalue.getValue(), writer);
		writer.writeEndElement();
		writer.writeln();
	}
}
