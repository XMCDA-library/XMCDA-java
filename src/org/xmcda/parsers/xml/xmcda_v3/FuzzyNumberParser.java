package org.xmcda.parsers.xml.xmcda_v3;

import org.xmcda.QualifiedValue;
import org.xmcda.XMCDA;
import org.xmcda.value.FuzzyNumber;
import org.xmcda.value.MembershipFunction;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;

/**
 * @author Sébastien Bigaret
 */
public class FuzzyNumberParser
{
	public static final String FUZZY_NUMBER = QualifiedValue.XMCDATypes.FUZZY_NUMBER.getTag();

	public static final String PLMF         = "piecewiseLinearMembershipFunction";

	public static final String SEGMENT      = "segment";

	public static final String HEAD         = "head";

	public static final String TAIL         = "tail";

	public FuzzyNumber<?, ?> fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		// a fuzzy number IS a MembershipFunction within XMCDA: we only have to read the function,
		// and store it within the fuzzyNumber.
		FuzzyNumber fuzzyNumber = new FuzzyNumber();
		fuzzyNumber.setFunction((MembershipFunction) new FunctionParser().fromXML(xmcda, startElement, eventReader));
		return fuzzyNumber;
	}


	public void toXML(FuzzyNumber<?, ?> fuzzyNumber, XMLStreamWriter writer) throws XMLStreamException
	{
		if (fuzzyNumber == null)
		    return; // TODO normal ça?
		new FunctionParser().toXML(FUZZY_NUMBER, fuzzyNumber.getFunction(), writer);
	}

}
