package org.xmcda.parsers.xml.xmcda_v3;

import java.util.List;
import java.util.Map.Entry;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.CriteriaScales;
import org.xmcda.Criterion;
import org.xmcda.CriterionScales;
import org.xmcda.Factory;
import org.xmcda.XMCDA;

/**
 * @author Sébastien Bigaret
 */
public class CriteriaScalesParser
{
	public static final String CRITERIA_SCALES = "criteriaScales";

	public CriteriaScales fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		CriteriaScales scales = Factory.criteriaScales();
		new CommonAttributesParser().handleAttributes(scales, startElement);
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (CRITERIA_SCALES.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				scales.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (CriterionScalesParser.CRITERION_SCALE.equals(startElement.getName().getLocalPart()))
			    new CriterionScalesParser().fromXML(xmcda, scales, startElement, eventReader);
		}
		return scales;

	}

	public void toXML(List<CriteriaScales> list, XMLStreamWriter writer) throws XMLStreamException
	{
		if (list == null || list.size()==0 )
			return;
		for (CriteriaScales criteriaScales: list)
			toXML(criteriaScales, writer);
	}

	public void toXML(CriteriaScales criteriaScales, XMLStreamWriter writer) throws XMLStreamException
	{
		if (criteriaScales == null)
		    return;

		writer.writeStartElement(CRITERIA_SCALES);
		new CommonAttributesParser().toXML(criteriaScales, writer);
		writer.writeln();

		new DescriptionParser().toXML(criteriaScales.getDescription(), writer);

		for (Entry<Criterion, CriterionScales> entry: criteriaScales.entrySet())
			new CriterionScalesParser().toXML(entry.getKey(), entry.getValue(), writer);

		writer.writeEndElement();
		writer.writeln();
	}

}
