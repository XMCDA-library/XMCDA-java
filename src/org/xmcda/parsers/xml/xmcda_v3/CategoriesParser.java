package org.xmcda.parsers.xml.xmcda_v3;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.Categories;
import org.xmcda.Category;
import org.xmcda.XMCDA;

public class CategoriesParser
{
	public static final String CATEGORIES = "categories";
	public void fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader) throws XMLStreamException
	{
		Categories categories = xmcda.categories;
		new CommonAttributesParser().handleAttributes(categories, startElement);
		while (eventReader.hasNext())
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (CATEGORIES.equals(event.asEndElement().getName().getLocalPart()))
			        break;
			
			if ( ! event.isStartElement())
				continue;
			
			startElement = event.asStartElement();
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				categories.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (CategoryParser.CATEGORY.equals(startElement.getName().getLocalPart()))
			{
				categories.add(new CategoryParser().fromXML(xmcda, startElement, eventReader));
			}
		}
	}

	public void toXML(Categories categories, XMLStreamWriter writer) throws XMLStreamException
	{
		if (categories == null)
			return;
		if (categories.isVoid())
		    return;

		writer.writeStartElement(CATEGORIES);
		new CommonAttributesParser().toXML(categories, writer);
		writer.writeln();
		new DescriptionParser().toXML(categories.getDescription(), writer);

		for (Category category: categories)
			new CategoryParser().toXML(category, writer);
		
		writer.writeEndElement();
		writer.writeln();

	}

}
