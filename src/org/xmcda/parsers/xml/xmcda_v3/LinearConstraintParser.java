package org.xmcda.parsers.xml.xmcda_v3;

import java.util.ArrayList;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.CommonAttributes;
import org.xmcda.Factory;
import org.xmcda.LinearConstraint;
import org.xmcda.LinearConstraint.Element;
import org.xmcda.XMCDA;

public class LinearConstraintParser <ELEMENT extends CommonAttributes, VALUE_TYPE>
{
	public static abstract class LinearConstraintParserHelper<ELEMENT>
	{
		public abstract ELEMENT buildObject(XMCDA xmcda, String id);
	}
	
	public static final String CONSTRAINTS = "constraints";

	public static final String CONSTRAINT  = "constraint";

	public static final String ELEMENTS    = "elements";

	public static final String ELEMENT     = "element";

	public static final String OPERATOR    = "operator";

	public static final String RHS         = "rhs";

	public static final String VALUES      = "values";

	public static final String VARIABLE_ID = "variableID";

	public static final String COEFFICIENT = "coefficient";

	protected LinearConstraint<ELEMENT, VALUE_TYPE> constraintFromXML(XMCDA xmcda, LinearConstraintParserHelper<ELEMENT> helper, StartElement startElement,
	                                                                  XMLEventReader eventReader)
	    throws XMLStreamException
	{
		LinearConstraint<ELEMENT, VALUE_TYPE> constraint = Factory.<ELEMENT, VALUE_TYPE> linearConstraint();
		new CommonAttributesParser().handleAttributes(constraint, startElement);

		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (CONSTRAINT.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				constraint.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			else if (ELEMENTS.equals(startElement.getName().getLocalPart()))
			{
				constraint.setElements(elementsFromXML(xmcda, helper, startElement, eventReader));
			}
			else if (OPERATOR.equals(startElement.getName().getLocalPart()))
			{
				constraint.setOperator(LinearConstraint.Operator.valueOf(Utils
				        .getTextContent(startElement, eventReader).toUpperCase()));
			}
			else if (RHS.equals(startElement.getName().getLocalPart()))
			{
				constraint.setRhs(new QualifiedValueParser<VALUE_TYPE>().fromXML(xmcda, startElement, eventReader));
			}
			else if (QualifiedValuesParser.VALUES.equals(startElement.getName().getLocalPart()))
			{
				constraint.setValues(new QualifiedValuesParser<VALUE_TYPE>().fromXML(xmcda, startElement, eventReader));
			}
		}
		return constraint;
	}

	private ArrayList<Element<ELEMENT, VALUE_TYPE>> elementsFromXML(XMCDA xmcda, LinearConstraintParserHelper<ELEMENT> helper, StartElement startElement,
	                                                                XMLEventReader eventReader)
	    throws XMLStreamException
	{
		ArrayList<Element<ELEMENT, VALUE_TYPE>> elements = new ArrayList<LinearConstraint.Element<ELEMENT, VALUE_TYPE>>();
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (ELEMENTS.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (ELEMENT.equals(startElement.getName().getLocalPart()))
			{
				elements.add(elementFromXML(xmcda, helper, startElement, eventReader));
			}
		}
		return elements;
	}

	private Element<ELEMENT, VALUE_TYPE> elementFromXML(XMCDA xmcda,
	                                                    LinearConstraintParserHelper<ELEMENT> helper,
	                                                    StartElement startElement,
	                                                    XMLEventReader eventReader) throws XMLStreamException
	{
		Element<ELEMENT, VALUE_TYPE> element = Factory.<ELEMENT, VALUE_TYPE> linearConstraintElement();
		new CommonAttributesParser().handleAttributes(element, startElement);
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (ELEMENT.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (VARIABLE_ID.equals(startElement.getName().getLocalPart()))
			{
				element.setVariableID(Utils.getTextContent(startElement, eventReader));
			}
			else if (COEFFICIENT.equals(startElement.getName().getLocalPart()))
			{
				element.setCoefficient(new QualifiedValueParser<VALUE_TYPE>().fromXML(xmcda, startElement, eventReader));
			}
			else
			// criterionID, criteriaSetID, alternativeID,, etc.
			{
				element.setUnknown(helper.buildObject(xmcda, Utils.getTextContent(startElement, eventReader)));
			}
		}
		return element;
	}

	public void toXML(String tagForT, LinearConstraint<ELEMENT, VALUE_TYPE> constraint, XMLStreamWriter writer)
	    throws XMLStreamException
	{
		if (constraint == null)
		    return;

		writer.writeStartElement(CONSTRAINT);
		new CommonAttributesParser().toXML(constraint, writer);
		writer.writeln();
		new DescriptionParser().toXML(constraint.getDescription(), writer);
		// elements
		if (constraint.getElements() != null && constraint.getElements().size() > 0)
		{
			writer.writeStartElement(ELEMENTS);
			writer.writeln();
			for (Element<ELEMENT, VALUE_TYPE> element: constraint.getElements())
			{
				writer.writeStartElement(ELEMENT);
				writer.writeln();
				if (element.getUnknown() != null)
				{
					writer.writeElementChars(tagForT, ( (CommonAttributes) element.getUnknown() ).id());
				}
				else
				{
					writer.writeElementChars(VARIABLE_ID, element.getVariableID());
				}
				new QualifiedValueParser<VALUE_TYPE>().toXML(COEFFICIENT, element.getCoefficient(), writer);
				writer.writeEndElement(); // ELEMENT
				writer.writeln();
			}
			writer.writeEndElement(); // ELEMENTS
			writer.writeln();
		}
		// operator
		writer.writeElementChars(OPERATOR, constraint.getOperator().name().toLowerCase());

		// rhs
		new QualifiedValueParser<VALUE_TYPE>().toXML(RHS, constraint.getRhs(), writer);

		// values
		new QualifiedValuesParser().toXML(constraint.getValues(), writer);
		writer.writeEndElement(); // CONSTRAINT
		writer.writeln();
	}
}
