/**
 * 
 */
package org.xmcda.parsers.xml.xmcda_v3;

import java.util.List;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;

import org.xmcda.CategoriesSet;
import org.xmcda.CategoriesSetsMatrix;
import org.xmcda.Factory;
import org.xmcda.XMCDA;
import org.xmcda.utils.Matrix;

/**
 * @author Sébastien Bigaret
 */
public class CategoriesSetsMatrixParser <CATEGORIES_SET_VALUE_TYPE, VALUE_TYPE>
    extends MatrixParser<CategoriesSet<CATEGORIES_SET_VALUE_TYPE>, VALUE_TYPE>
{
	public static final String CATEGORIES_SETS_MATRIX = org.xmcda.CategoriesSetsMatrix.TAG;

	@Override
	public String rootTag()
	{
		return CATEGORIES_SETS_MATRIX;
	}

	@Override
	public String dimensionTag()
	{
		return "categoriesSetID";
	}

	@Override
	public CategoriesSet<CATEGORIES_SET_VALUE_TYPE> buildDimension(XMCDA xmcda, String id)
	{
		return (CategoriesSet<CATEGORIES_SET_VALUE_TYPE>) xmcda.categoriesSets.get(id);
	}

	@Override
	public String dimensionID(CategoriesSet<CATEGORIES_SET_VALUE_TYPE> categoriesSet)
	{
		return categoriesSet.id();
	}

	@Override
	public CategoriesSetsMatrix<CATEGORIES_SET_VALUE_TYPE, VALUE_TYPE> fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		Matrix<CategoriesSet<CATEGORIES_SET_VALUE_TYPE>, VALUE_TYPE> matrix = super.fromXML(xmcda, startElement, eventReader);
		CategoriesSetsMatrix<CATEGORIES_SET_VALUE_TYPE, VALUE_TYPE> categoriesSetsMatrix = Factory.categoriesSetsMatrix();

		categoriesSetsMatrix.setId(matrix.id());
		categoriesSetsMatrix.setName(matrix.name());
		categoriesSetsMatrix.setMcdaConcept(matrix.mcdaConcept());
		categoriesSetsMatrix.setDescription(matrix.getDescription());
		categoriesSetsMatrix.setValuation(matrix.getValuation());
		categoriesSetsMatrix.putAll(matrix);
		return categoriesSetsMatrix;
	}

	public void toXML(List<CategoriesSetsMatrix<CATEGORIES_SET_VALUE_TYPE, VALUE_TYPE>> matricesList, XMLStreamWriter writer)
		    throws XMLStreamException
	{
		if (matricesList == null || matricesList.size() == 0)
		    return;

		for (CategoriesSetsMatrix<CATEGORIES_SET_VALUE_TYPE, VALUE_TYPE> matrix: matricesList)
			toXML(matrix, writer);
	}

	public void toXML(CategoriesSetsMatrix<CATEGORIES_SET_VALUE_TYPE, VALUE_TYPE> matrix, XMLStreamWriter writer)
	    throws XMLStreamException
	{
		super.toXML(matrix, writer);
	}
}
