package org.xmcda.parsers.xml.xmcda_v3;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.AlternativesSetsValues;
import org.xmcda.AlternativesSet;
import org.xmcda.Factory;
import org.xmcda.LabelledQValues;
import org.xmcda.XMCDA;

/**
 * @author Sébastien Bigaret
 */
public class AlternativesSetValuesParser<ALTERNATIVES_SET_VALUE_TYPE, VALUE_TYPE>
{
	public static final String ALTERNATIVES_SET_VALUE = "alternativesSetValue";

	public static final String ALTERNATIVES_SET_ID       = "alternativesSetID";

	/**
	 * Builds the list of values attached to an ELEMENT and inserts it into the provided objectsValues object
	 * using the ELEMENT as the key.
	 * @param objectsValues
	 * @param startElement
	 * @param eventReader
	 * @return the ELEMENT object that was used to update the objectsValues with the built list of values
	 * @throws XMLStreamException
	 */
	public AlternativesSet<ALTERNATIVES_SET_VALUE_TYPE> fromXML(XMCDA xmcda, AlternativesSetsValues<ALTERNATIVES_SET_VALUE_TYPE, VALUE_TYPE> objectsValues, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		LabelledQValues<VALUE_TYPE> alternativesSetValues = Factory.labelledQValues();
		new CommonAttributesParser().handleAttributes(alternativesSetValues, startElement);

		AlternativesSet<ALTERNATIVES_SET_VALUE_TYPE> alternativesSet = null;
		
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (ALTERNATIVES_SET_VALUE.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				alternativesSetValues.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (QualifiedValuesParser.VALUES.equals(startElement.asStartElement().getName()
			        .getLocalPart()))
			{
				alternativesSetValues.addAll(new QualifiedValuesParser<VALUE_TYPE>().fromXML(xmcda, startElement, eventReader));
			}

			if (ALTERNATIVES_SET_ID.equals(startElement.getName().getLocalPart()))
			{
				final String id = Utils.getTextContent(startElement, eventReader);
				alternativesSet = Factory.alternativesSet();
				alternativesSet.setId(id);
			}
		}
		objectsValues.put(alternativesSet, alternativesSetValues);
		return alternativesSet;
	}

	public void toXML(AlternativesSet<ALTERNATIVES_SET_VALUE_TYPE> alternativesSet, LabelledQValues<VALUE_TYPE> alternativesSetValues, XMLStreamWriter writer) throws XMLStreamException
    {
		if (alternativesSetValues == null)
			return;

		writer.writeStartElement(ALTERNATIVES_SET_VALUE);
		new CommonAttributesParser().toXML(alternativesSetValues, writer);
		writer.writeln();
		new DescriptionParser().toXML(alternativesSetValues.getDescription(), writer);
		writer.writeElementChars(ALTERNATIVES_SET_ID, alternativesSet.id());
		new QualifiedValuesParser<VALUE_TYPE>().toXML(alternativesSetValues, writer);
		writer.writeEndElement();
		writer.writeln();
    }

}
