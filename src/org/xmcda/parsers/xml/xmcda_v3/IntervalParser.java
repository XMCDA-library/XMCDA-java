/**
 * 
 */
package org.xmcda.parsers.xml.xmcda_v3;

import org.xmcda.QualifiedValue;
import org.xmcda.XMCDA;
import org.xmcda.value.Interval;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/**
 * @author Sébastien Bigaret
 *
 */
public class IntervalParser
{
	public static final String INTERVAL = QualifiedValue.XMCDATypes.INTERVAL.getTag();
	public static final String LOWER_BOUND = "lowerBound";
	public static final String UPPER_BOUND = "upperBound";

	public Interval<?> fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader) throws XMLStreamException
	{
		QualifiedValue<?> lowerBound = null, upperBound = null;
		boolean lower_isOpen = false, upper_isOpen = false;
		while (eventReader.hasNext())
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (INTERVAL.equals(event.asEndElement().getName().getLocalPart()))
			        break;
			
			if ( ! event.isStartElement())
				continue;
			
			startElement = event.asStartElement();
			if (LOWER_BOUND.equals(startElement.getName().getLocalPart()))
			{
				Attribute open = startElement.getAttributeByName(new QName("open"));
				if (open!=null)
					lower_isOpen = Utils.booleanValue(open.getValue());
				lowerBound = new QualifiedValueParser().fromXML(xmcda, startElement, eventReader);
				continue;
			}
			if (UPPER_BOUND.equals(startElement.getName().getLocalPart()))
			{
				Attribute open = startElement.getAttributeByName(new QName("open"));
				if (open!=null)
					upper_isOpen = Utils.booleanValue(open.getValue());
				upperBound = new QualifiedValueParser().fromXML(xmcda, startElement, eventReader);
			}
		}

		// TODO if (!lowerBound.getValue().getClass().equals(upperBound.getValue().getClass()))
			;//throw new RuntimeException("Interval lower / upper must have the same type");
		Interval interval = new Interval();
		interval.setLowerBound(lowerBound);
		interval.setIsLeftClosed(!lower_isOpen);
		interval.setUpperBound(upperBound);
		interval.setIsRightClosed(!upper_isOpen);
		return interval;
	}

	public void toXML(Interval<?> interval, XMLStreamWriter writer) throws XMLStreamException
	{
		writer.writeStartElement(INTERVAL);
		writer.writeln();
		if ( interval.getLowerBound() != null )
		{
			writer.writeStartElement(LOWER_BOUND);
			new CommonAttributesParser().toXML(interval.getLowerBound(), writer);
			writer.writeAttribute("open", interval.isLeftClosed() ? "false" : "true");
			writer.writeln();
			new ValueParser().toXML(interval.getLowerBound().getValue(), writer);
			writer.writeEndElement();
			writer.writeln();
		}
		if ( interval.getUpperBound() != null )
		{
			writer.writeStartElement(UPPER_BOUND);
			new CommonAttributesParser().toXML(interval.getUpperBound(), writer);
			writer.writeAttribute("open", interval.isRightClosed() ? "false" : "true");
			writer.writeln();
			new ValueParser().toXML(interval.getUpperBound().getValue(), writer);
			writer.writeEndElement();
			writer.writeln();
		}
		writer.writeEndElement();
		writer.writeln();
	}


}
