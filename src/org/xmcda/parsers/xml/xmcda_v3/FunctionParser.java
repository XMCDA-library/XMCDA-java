package org.xmcda.parsers.xml.xmcda_v3;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.XMCDA;
import org.xmcda.value.AffineFunction;
import org.xmcda.value.ConstantFunction;
import org.xmcda.value.DiscreteFunction;
import org.xmcda.value.Function;
import org.xmcda.value.PiecewiseLinearFunction;

/**
 * @author Sébastien Bigaret
 */
public class FunctionParser
{
	public static final String FUNCTION = "function";

	public Function fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader) throws XMLStreamException
	{
		// we may enter here with: <function>, <fuzzyNumber>
		Function function = null;
		final StartElement initialElement = startElement;
		final String initialTag = initialElement.getName().getLocalPart();
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (initialTag.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;
			startElement = event.asStartElement();

			final String currentTagName = startElement.getName().getLocalPart();
			if (ConstantFunctionParser.CONSTANT.equals(currentTagName))
				function = new ConstantFunctionParser().fromXML(xmcda, startElement, eventReader);
			else if (AffineFunctionParser.AFFINE.equals(currentTagName))
				function = new AffineFunctionParser().fromXML(xmcda, startElement, eventReader);
			else if (PiecewiseLinearFunctionParser.PIECEWISE_LINEAR.equals(currentTagName))
				function = new PiecewiseLinearFunctionParser().fromXML(xmcda, startElement, eventReader);
			else if (DiscreteFunctionParser.DISCRETE.equals(currentTagName))
				function = new DiscreteFunctionParser().fromXML(xmcda, startElement, eventReader);
			// else ERROR TODO
		}
		// TODO if null
		new CommonAttributesParser().handleAttributes(function, initialElement);
		return function;
	}

	
	public void toXML(String tag, Function function, XMLStreamWriter writer) throws XMLStreamException
    {
		if (function == null)
			return; // TODO normal ça?

		writer.writeStartElement(tag);
		new CommonAttributesParser().toXML(function, writer);
		writer.writeln();
		if (function instanceof ConstantFunction)
			new ConstantFunctionParser().toXML((ConstantFunction<?>) function, writer);
		else if (function instanceof AffineFunction)
			new AffineFunctionParser().toXML((AffineFunction<?>) function, writer);
		else if (function instanceof PiecewiseLinearFunction)
			new PiecewiseLinearFunctionParser().toXML((PiecewiseLinearFunction<?, ?>) function, writer);
		else if (function instanceof DiscreteFunction)
			new DiscreteFunctionParser().toXML((DiscreteFunction<?, ?>) function, writer);
		// else ERROR TODO
		writer.writeEndElement();
		writer.writeln();
    }

}
