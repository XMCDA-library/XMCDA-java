package org.xmcda.parsers.xml.xmcda_v3;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.AlternativesValues;
import org.xmcda.Alternative;
import org.xmcda.Factory;
import org.xmcda.LabelledQValues;
import org.xmcda.XMCDA;

/**
 * @author Sébastien Bigaret
 */
public class AlternativeValuesParser<VALUE_TYPE>
{
	public static final String ALTERNATIVE_VALUE = "alternativeValue";

	public static final String ALTERNATIVE_ID       = "alternativeID";

	/**
	 * Builds the list of values attached to an ELEMENT and inserts it into the provided objectsValues object
	 * using the ELEMENT as the key.
	 * @param objectsValues
	 * @param startElement
	 * @param eventReader
	 * @return the ELEMENT object that was used to update the objectsValues with the built list of values
	 * @throws XMLStreamException
	 */
	public Alternative fromXML(XMCDA xmcda, AlternativesValues<VALUE_TYPE> objectsValues, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		LabelledQValues<VALUE_TYPE> alternativeValues = Factory.labelledQValues();
		new CommonAttributesParser().handleAttributes(alternativeValues, startElement);

		Alternative alternative = null;
		
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (ALTERNATIVE_VALUE.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				alternativeValues.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (QualifiedValuesParser.VALUES.equals(startElement.asStartElement().getName()
			        .getLocalPart()))
			{
				alternativeValues.addAll(new QualifiedValuesParser<VALUE_TYPE>().fromXML(xmcda, startElement, eventReader));
			}

			if (ALTERNATIVE_ID.equals(startElement.getName().getLocalPart()))
			{
				final String id = Utils.getTextContent(startElement, eventReader);
				alternative = xmcda.alternatives.get(id);
			}
		}
		objectsValues.put(alternative, alternativeValues);
		return alternative;
	}

	public void toXML(Alternative alternative, LabelledQValues<VALUE_TYPE> alternativeValues, XMLStreamWriter writer) throws XMLStreamException
    {
		if (alternativeValues == null)
			return;

		writer.writeStartElement(ALTERNATIVE_VALUE);
		new CommonAttributesParser().toXML(alternativeValues, writer);
		writer.writeln();
		new DescriptionParser().toXML(alternativeValues.getDescription(), writer);
		writer.writeElementChars(ALTERNATIVE_ID, alternative.id());
		new QualifiedValuesParser<VALUE_TYPE>().toXML(alternativeValues, writer);
		writer.writeEndElement();
		writer.writeln();
    }

}
