package org.xmcda.parsers.xml.xmcda_v3;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.XMCDA;
import org.xmcda.value.Point;

/**
 * 
 * @author Sébastien Bigaret
 *
 */
public class PointParser
{
	public static final String POINT = "point";
	public static final String ABSCISSA = "abscissa";
	public static final String ORDINATE = "ordinate";
	
	public Point<?,?> fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader) throws XMLStreamException
	{
		final String initialTag = startElement.getName().getLocalPart();
		Point point = new Point();
		while ( eventReader.hasNext() )
		{
			// abscissa
			// ordinate
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
				if (initialTag.equals(event.asEndElement().getName().getLocalPart()))
				    break;

			if (!event.isStartElement())
				continue;

			startElement = event.asStartElement();
			if (ABSCISSA.equals(startElement.asStartElement().getName().getLocalPart()))
				point.setAbscissa(new QualifiedValueParser().fromXML(xmcda, startElement, eventReader));
			else if (ORDINATE.equals(startElement.asStartElement().getName().getLocalPart()))
				point.setOrdinate(new QualifiedValueParser().fromXML(xmcda, startElement, eventReader));
		}
		return point;
	}

	public void toXML(Point<?,?> point, XMLStreamWriter writer) throws XMLStreamException
	{
		if (point == null)
			return;
		
		writer.writeStartElement(POINT);
		writer.writeln();

		new QualifiedValueParser().toXML(ABSCISSA, point.getAbscissa(), writer);
		
		new QualifiedValueParser().toXML(ORDINATE, point.getOrdinate(), writer);

		writer.writeEndElement();
		writer.writeln();
	}

}
