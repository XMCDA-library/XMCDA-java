/**
 * 
 */
package org.xmcda.parsers.xml.xmcda_v3;

import java.util.Iterator;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;

import org.xmcda.CommonAttributes;

/**
 * @author Sébastien Bigaret
 *
 */
public class CommonAttributesParser
{
	public static final String ID           = "id";

	public static final String NAME         = "name";

	public static final String MCDA_CONCEPT = "mcdaConcept";

	public void handleAttributes(CommonAttributes obj, StartElement startElement)
	{
		Iterator<Attribute> attributes = startElement.getAttributes();
		while (attributes.hasNext()) {
			Attribute attribute = attributes.next();
			if (new CommonAttributesParser().handleAttribute(obj, attribute))
				continue;
			// TODO return list of handled attributes?
		}

	}
	
	public boolean handleAttribute(CommonAttributes obj, Attribute attribute)
	{
		final String attributeName = attribute.getName().toString();
		if (ID.equals(attributeName))
		{
			obj.setId(attribute.getValue());
			return true;
		}
		if (MCDA_CONCEPT.equals(attributeName))
		{
			obj.setMcdaConcept(attribute.getValue());
			return true;
		}
		if (NAME.equals(attributeName))
		{
			obj.setName(attribute.getValue());
			return true;
		}
		return false;
	}

	public void toXML(CommonAttributes obj, XMLStreamWriter writer) throws XMLStreamException
	{
		writer.writeNonNullAttribute(ID, obj.id());
		writer.writeNonNullAttribute(NAME, obj.name());
		writer.writeNonNullAttribute(MCDA_CONCEPT, obj.mcdaConcept());
	}
}
