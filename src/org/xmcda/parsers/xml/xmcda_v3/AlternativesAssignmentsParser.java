package org.xmcda.parsers.xml.xmcda_v3;


import java.util.List;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.AlternativeAssignment;
import org.xmcda.AlternativesAssignments;
import org.xmcda.Factory;
import org.xmcda.XMCDA;

/**
 * @author Sébastien Bigaret
 */
public class AlternativesAssignmentsParser<VALUE_TYPE>
{
	public static final String ALTERNATIVES_ASSIGNMENTS = AlternativesAssignments.TAG;

	public static final String ALTERNATIVE_ASSIGNMENT   = AlternativeAssignment.TAG;

	public static final String ALTERNATIVE_ID           = "alternativeID";

	public static final String CATEGORY_ID              = "categoryID";

	public static final String CATEGORIES_SET_ID        = "categoriesSetID";

	public AlternativesAssignments<VALUE_TYPE> fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader) throws XMLStreamException
	{
		AlternativesAssignments<VALUE_TYPE> altsAssignmts= Factory.alternativesAssignments();
		new CommonAttributesParser().handleAttributes(altsAssignmts, startElement);
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (ALTERNATIVES_ASSIGNMENTS.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			final String currentTagName = startElement.getName().getLocalPart();
			if (DescriptionParser.DESCRIPTION.equals(currentTagName))
			{
				altsAssignmts.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (ALTERNATIVE_ASSIGNMENT.equals(currentTagName))
			{
				altsAssignmts.add(alternativeAssignmentFromXML(xmcda, startElement, eventReader));
			}
		}
		return altsAssignmts;
	}

	protected AlternativeAssignment<VALUE_TYPE> alternativeAssignmentFromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader) throws XMLStreamException
	{
		AlternativeAssignment<VALUE_TYPE> altAssignt = Factory.alternativeAssignment();
		new CommonAttributesParser().handleAttributes(altAssignt, startElement);
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (ALTERNATIVE_ASSIGNMENT.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;
			startElement = event.asStartElement();
			final String currentTagName = startElement.getName().getLocalPart();
			if (DescriptionParser.DESCRIPTION.equals(currentTagName))
			{
				altAssignt.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			else if (ALTERNATIVE_ID.equals(currentTagName))
			{
				altAssignt.setAlternative(xmcda.alternatives.get(Utils.getTextContent(startElement, eventReader)));
			}
			else if (CATEGORY_ID.equals(currentTagName))
			{
				altAssignt.setCategory(xmcda.categories.get(Utils.getTextContent(startElement, eventReader)));
			}
			else if (CATEGORIES_SET_ID.equals(currentTagName))
			{
				altAssignt.setCategoriesSet(xmcda.categoriesSets.get(Utils.getTextContent(startElement, eventReader)));
			}
			else if (CategoriesIntervalParser.CATEGORIES_INTERVAL.equals(currentTagName))
			{
				altAssignt.setCategoryInterval(new CategoriesIntervalParser().fromXML(xmcda, startElement, eventReader));
			}
			else if (QualifiedValuesParser.VALUES.equals(currentTagName))
			{
				altAssignt.setValues(new QualifiedValuesParser<VALUE_TYPE>().fromXML(xmcda, startElement, eventReader));
			}
		}
		return altAssignt;
	}

	public void toXML(List<AlternativesAssignments<VALUE_TYPE>> list, XMLStreamWriter writer) throws XMLStreamException
	{
		if (list == null || list.size()==0 )
			return;
		for (AlternativesAssignments<VALUE_TYPE> alternativesAssignments: list)
			toXML(alternativesAssignments, writer);
	}

	public void toXML(AlternativesAssignments<VALUE_TYPE> alternativesAssignements, XMLStreamWriter writer) throws XMLStreamException
	{
		if (alternativesAssignements == null)
		    return;

		writer.writeStartElement(ALTERNATIVES_ASSIGNMENTS);
		new CommonAttributesParser().toXML(alternativesAssignements, writer);
		writer.writeln();
		new DescriptionParser().toXML(alternativesAssignements.getDescription(), writer);

		for (AlternativeAssignment<VALUE_TYPE> alternativeAssignment: alternativesAssignements)
			toXML(alternativeAssignment, writer);

		writer.writeEndElement(); // ALTERNATIVES_ASSIGNMENTS
		writer.writeln();
	}

	public void toXML(AlternativeAssignment<VALUE_TYPE> alternativeAssignement, XMLStreamWriter writer) throws XMLStreamException
	{
		if (alternativeAssignement == null)
		    return;

		writer.writeStartElement(ALTERNATIVE_ASSIGNMENT);
		new CommonAttributesParser().toXML(alternativeAssignement, writer);
		writer.writeln();
		new DescriptionParser().toXML(alternativeAssignement.getDescription(), writer);

		if (alternativeAssignement.getAlternative() != null)
			writer.writeElementChars(ALTERNATIVE_ID, alternativeAssignement.getAlternative().id());
		if (alternativeAssignement.getCategory() != null)
			writer.writeElementChars(CATEGORY_ID, alternativeAssignement.getCategory().id());
		else if (alternativeAssignement.getCategoriesSet() != null)
			writer.writeElementChars(CATEGORIES_SET_ID, alternativeAssignement.getCategoriesSet().id());
		else if (alternativeAssignement.getCategoryInterval() != null)
			new CategoriesIntervalParser().toXML(alternativeAssignement.getCategoryInterval(), writer);

		if (alternativeAssignement.getValues() != null)
			new QualifiedValuesParser<VALUE_TYPE>().toXML(alternativeAssignement.getValues(), writer);

		writer.writeEndElement(); // ALTERNATIVES_ASSIGNMENTS
		writer.writeln();
	}
}
