package org.xmcda.parsers.xml.xmcda_v3;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.CommonAttributes;
import org.xmcda.Criterion;
import org.xmcda.XMCDA;

public class CriterionParser
{
	public static final String CRITERION = "criterion";
	public static final String ACTIVE      = "active";

	public Criterion fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader) throws XMLStreamException
	{
		CommonAttributes attributesHolder = new ReferenceableParser().getAttributes(startElement);
		Criterion criterion = xmcda.criteria.get(attributesHolder.id());
		criterion.setName(attributesHolder.name());
		criterion.setMcdaConcept(attributesHolder.mcdaConcept());

		while ( eventReader.hasNext() )
		{
			// description
			// active
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			{
				if (CRITERION.equals(event.asEndElement().getName().getLocalPart()))
					break;
			}
			if ( ! event.isStartElement() )
				continue;

			startElement = event.asStartElement();
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				criterion.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (ACTIVE.equals(startElement.getName().getLocalPart()))
			{
				event = eventReader.nextEvent();
				final String active = event.asCharacters().getData();
				criterion.setIsActive("true".equals(active) || "1".equals(active));
				continue;
			}

		}
		return criterion;
	}

	public void toXML(Criterion criterion, XMLStreamWriter writer) throws XMLStreamException
	{
		if ( criterion == null )
			return;
		writer.writeStartElement(CRITERION);
		new ReferenceableParser().toXML(criterion, writer);
		writer.writeln();
		new DescriptionParser().toXML(criterion.getDescription(), writer);
		writer.writeElementBoolean(ACTIVE, criterion.isActive());
		writer.writeEndElement();
		writer.writeln();
	}
	
	@SuppressWarnings("rawtypes")
	public boolean canConvert(Class type)
	{
		return type == Criterion.class;
	}

}
