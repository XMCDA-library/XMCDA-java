package org.xmcda.parsers.xml.xmcda_v3;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.Criteria;
import org.xmcda.Criterion;
import org.xmcda.XMCDA;

/**
 * @author Sébastien Bigaret
 */
public class CriteriaParser
{
	public static final String CRITERIA = "criteria";

	public void fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader) throws XMLStreamException
	{
		Criteria criteria = xmcda.criteria;
		new CommonAttributesParser().handleAttributes(criteria, startElement);
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (CRITERIA.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (DescriptionParser.DESCRIPTION.equals(startElement.getName().getLocalPart()))
			{
				criteria.setDescription(new DescriptionParser().fromXML(xmcda, startElement, eventReader));
			}
			if (CriterionParser.CRITERION.equals(startElement.getName().getLocalPart()))
			{
				criteria.merge(new CriterionParser().fromXML(xmcda, startElement, eventReader));
			}
		}
	}

	public void toXML(Criteria criteria, XMLStreamWriter writer) throws XMLStreamException
	{
		if (criteria == null)
		    return;

		if (criteria.isVoid())
		    return;

		writer.writeStartElement(CRITERIA);
		new CommonAttributesParser().toXML(criteria, writer);
		writer.writeln();
		new DescriptionParser().toXML(criteria.getDescription(), writer);

		for (Criterion criterion: criteria)
			new CriterionParser().toXML(criterion, writer);

		writer.writeEndElement();
		writer.writeln();
	}

	@SuppressWarnings("rawtypes")
	public boolean canConvert(Class type)
	{
		return type == Criteria.class;
	}

}
