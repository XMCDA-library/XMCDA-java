/**
 * 
 */
package org.xmcda.parsers.xml.xmcda_v3;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.Factory;
import org.xmcda.QualifiedValue;
import org.xmcda.QualifiedValues;
import org.xmcda.XMCDA;

/**
 * @author Sébastien Bigaret
 *
 */
public class QualifiedValuesParser<VALUE_TYPE>
{
	public static final String VALUES              = "values";

	public QualifiedValues<VALUE_TYPE> fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader) throws XMLStreamException
	{
		QualifiedValues<VALUE_TYPE> values = Factory.qualifiedValues();

		while (eventReader.hasNext())
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (VALUES.equals(event.asEndElement().getName().getLocalPart()))
			        break;
			
			if ( ! event.isStartElement())
				continue;
			
			startElement = event.asStartElement();
			if ( QualifiedValueParser.VALUE.equals(startElement.getName().getLocalPart()))
			{
				values.add(new QualifiedValueParser<VALUE_TYPE>().fromXML(xmcda, startElement, eventReader));
			}
		}
		return values;
	}

	public void toXML(QualifiedValues<VALUE_TYPE> qvalues, XMLStreamWriter writer) throws XMLStreamException
    {
		if (qvalues == null)
			return; // TODO normal ça?
		if (qvalues.size()==0)
			return;
		writer.writeStartElement(VALUES);
		writer.writeln();
		for (QualifiedValue<VALUE_TYPE> qvalue: qvalues)
			new QualifiedValueParser<VALUE_TYPE>().toXML(QualifiedValueParser.VALUE, qvalue, writer);
		writer.writeEndElement();
		writer.writeln();
    }
}
