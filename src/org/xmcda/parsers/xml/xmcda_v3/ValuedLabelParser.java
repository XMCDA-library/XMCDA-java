/**
 * 
 */
package org.xmcda.parsers.xml.xmcda_v3;

import org.xmcda.QualifiedValue;
import org.xmcda.XMCDA;
import org.xmcda.value.ValuedLabel;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/**
 * @author Sébastien Bigaret
 */
public class ValuedLabelParser
{
	public static final String VALUED_LABEL = QualifiedValue.XMCDATypes.VALUED_LABEL.getTag();

	public static final String LABEL        = "label";

	public static final String VALUE        = "value";

	public ValuedLabel fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader) throws XMLStreamException
	{
		ValuedLabel valuedLabel = new ValuedLabel();
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement() && VALUED_LABEL.equals(event.asEndElement().getName().getLocalPart()))
			    break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (LABEL.equals(startElement.getName().getLocalPart()))
			{
				valuedLabel.setLabel(Utils.getTextContent(startElement, eventReader));
				continue;
			}
			if (VALUE.equals(startElement.getName().getLocalPart()))
			{
				valuedLabel.setValue(new QualifiedValueParser().fromXML(xmcda, startElement, eventReader));
				continue;
			}
		}
		return valuedLabel;
	}

	public void toXML(ValuedLabel<?> valuedLabel, XMLStreamWriter writer) throws XMLStreamException
	{
		if (valuedLabel == null)
		    return;
		writer.writeStartElement(VALUED_LABEL);
		writer.writeln();
		writer.writeElementChars(LABEL, valuedLabel.getLabel());
		new QualifiedValueParser().toXML(QualifiedValueParser.VALUE, valuedLabel.getValue(), writer);
		writer.writeEndElement();
		writer.writeln();

	}

}
