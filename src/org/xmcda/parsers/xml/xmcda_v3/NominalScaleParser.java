package org.xmcda.parsers.xml.xmcda_v3;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.Factory;
import org.xmcda.NominalScale;
import org.xmcda.XMCDA;

/**
 * @author Sébastien Bigaret
 */
public class NominalScaleParser
{
	public static final String NOMINAL = "nominal";

	public static final String LABELS  = "labels";

	public static final String LABEL   = "label";

	public NominalScale fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader) throws XMLStreamException
	{
		NominalScale scale = Factory.nominalScale();

		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (NOMINAL.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;

			startElement = event.asStartElement();
			if (LABELS.equals(startElement.getName().getLocalPart()))
				labelsFromXML(xmcda, scale, startElement, eventReader);
		}
		return scale;
	}

	protected void labelsFromXML(XMCDA xmcda, NominalScale scale, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		// read labels
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (LABELS.equals(event.asEndElement().getName().getLocalPart()))
			        break;
			if (!event.isStartElement())
			    continue;
			startElement = event.asStartElement();
			if (LABEL.equals(startElement.getName().getLocalPart()))
				scale.add(Utils.getTextContent(startElement, eventReader));
		}
	}

	public void toXML(NominalScale scale, XMLStreamWriter writer) throws XMLStreamException
    {
		if (scale == null)
			return; // TODO normal ça?

		writer.writeStartElement(NOMINAL);
		writer.writeln();
		writer.writeStartElement(LABELS);
		writer.writeln();
		for (String label: scale)
			writer.writeElementChars(LABEL, label);
		writer.writeEndElement(); // LABELS
		writer.writeln();
		writer.writeEndElement(); // NOMINAL
		writer.writeln();
    }

}
