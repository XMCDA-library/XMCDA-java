package org.xmcda.parsers.xml.xmcda_v3;

import java.util.EnumSet;
import java.util.Optional;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;

/**
 * Defines the versions of XMCDA v3
 *
 */
public enum Version
{
    XMCDA_3_0_0("3.0.0",
                "/xsd/XMCDA-3.0.0.xsd",
                "http://www.decision-deck.org/2013/XMCDA-3.0.0",
                "http://www.decision-deck.org/xmcda/_downloads/XMCDA-3.0.0.xsd"),
    XMCDA_3_0_1("3.0.1",
                "/xsd/XMCDA-3.0.1.xsd",
                "http://www.decision-deck.org/2016/XMCDA-3.0.1",
                "http://www.decision-deck.org/xmcda/_downloads/XMCDA-3.0.1.xsd"),
    XMCDA_3_0_2("3.0.2",
                "/xsd/XMCDA-3.0.2.xsd",
                "http://www.decision-deck.org/2017/XMCDA-3.0.2",
                "http://www.decision-deck.org/xmcda/_downloads/XMCDA-3.0.2.xsd"),
    XMCDA_3_1_0("3.1.0",
                "/xsd/XMCDA-3.1.0.xsd",
                "http://www.decision-deck.org/2018/XMCDA-3.1.0",
                "http://www.decision-deck.org/xmcda/_downloads/XMCDA-3.1.0.xsd"),
    XMCDA_3_1_1("3.1.1",
                "/xsd/XMCDA-3.1.1.xsd",
                "http://www.decision-deck.org/2019/XMCDA-3.1.1",
                "http://www.decision-deck.org/xmcda/_downloads/XMCDA-3.1.1.xsd");

    public final String          name;

    public final String          localXSDLocation;

    public final String          namespace;

	public final String          namespacePrefix = "xmcda";

    public final String          schemaURL;

    /** The list of all existing XMCDA v3 version numbers */
	public static final String[] VERSIONS = EnumSet.allOf(Version.class).stream().map(v -> v.name).toArray(String[]::new);

	public static final Version  defaultVersion = XMCDA_3_1_1;

    Version(String name, String localXSDLocation, String namespace, String schemaURL)
    {
        this.name = name;
        this.localXSDLocation = localXSDLocation;
        this.namespace = namespace;
        this.schemaURL = schemaURL;
    }

    /**
     * Return the requested v3 Version.
     * @param version the version number; it should be amongst {@link #VERSIONS}.
     * @return the requested v3 Version.
     */
    public static Optional<Version> get(String version)
    {
		return EnumSet.allOf(Version.class).stream().filter(v -> v.name.equals(version)).findFirst();
    }
    
    public static Source [] allSchemas()
    {
		return EnumSet.allOf(Version.class).stream()
		        .map(v -> new StreamSource(Version.class.getResourceAsStream(v.localXSDLocation)))
		        .toArray(Source[]::new);
    }

}
