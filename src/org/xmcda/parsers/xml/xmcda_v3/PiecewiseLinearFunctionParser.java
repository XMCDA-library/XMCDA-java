package org.xmcda.parsers.xml.xmcda_v3;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.XMCDA;
import org.xmcda.value.EndPoint;
import org.xmcda.value.PiecewiseLinearFunction;
import org.xmcda.value.Segment;

public class PiecewiseLinearFunctionParser
{
	public static final String PIECEWISE_LINEAR = "piecewiseLinear";
	public static final String SEGMENT = "segment";
	public static final String HEAD = "head";
	public static final String TAIL = "tail";
	
	public PiecewiseLinearFunction<?, ?> fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		PiecewiseLinearFunction<?,?> function = new PiecewiseLinearFunction();
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (PIECEWISE_LINEAR.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;
			startElement = event.asStartElement();
			if (SEGMENT.equals(startElement.asStartElement().getName().getLocalPart()))
				function.add(segmentFromXML(xmcda, startElement, eventReader));
		}
		return function;
	}

	protected Segment segmentFromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader) throws XMLStreamException
	{
		Segment segment = new Segment();
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			    if (SEGMENT.equals(event.asEndElement().getName().getLocalPart()))
			        break;

			if (!event.isStartElement())
			    continue;
			startElement = event.asStartElement();
			if (HEAD.equals(startElement.asStartElement().getName().getLocalPart()))
			{
				final EndPoint head = new EndPointParser().fromXML(xmcda, startElement, eventReader);
				segment.setHead(head);
			}
			if (TAIL.equals(startElement.asStartElement().getName().getLocalPart()))
			{
				final EndPoint tail = new EndPointParser().fromXML(xmcda, startElement, eventReader);
				segment.setTail(tail);
			}
		}
		return segment;
	}
	
	public void toXML(PiecewiseLinearFunction<?, ?> function, XMLStreamWriter writer) throws XMLStreamException
	{
		if (function == null)
			return;
		
		writer.writeStartElement(PIECEWISE_LINEAR);
		writer.writeln();
		for (Segment segment: function)
		{
			writer.writeStartElement(SEGMENT);
			writer.writeln();

			new EndPointParser().toXML(HEAD, segment.getHead(), writer);
			new EndPointParser().toXML(TAIL, segment.getTail(), writer);

			writer.writeEndElement();
			writer.writeln();
		}
		writer.writeEndElement();
		writer.writeln();
	}
}
