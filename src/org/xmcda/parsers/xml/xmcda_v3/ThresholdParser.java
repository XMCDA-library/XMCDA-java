package org.xmcda.parsers.xml.xmcda_v3;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xmcda.Factory;
import org.xmcda.QualifiedValue;
import org.xmcda.Threshold;
import org.xmcda.XMCDA;
import org.xmcda.value.NA;

/**
 * @author Sébastien Bigaret
 */
public class ThresholdParser
{
	public static final String THRESHOLD = "threshold";

	public static final String CONSTANT  = "constant";

	public static final String AFFINE    = "affine";

	public static final String TYPE      = "type";

	public static final String SLOPE     = "slope";

	public static final String INTERCEPT = "intercept";

	public Threshold<?> fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader) throws XMLStreamException
	{
		Threshold threshold = Factory.threshold(new QualifiedValue(NA.na));
		new CommonAttributesParser().handleAttributes(threshold, startElement);

		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			{
				if (THRESHOLD.equals(event.asEndElement().getName().getLocalPart()))
					break;
			}
			if ( ! event.isStartElement() )
				continue;

			startElement = event.asStartElement();
			if (CONSTANT.equals(startElement.getName().getLocalPart()))
			{
				threshold.setConstant(new QualifiedValueParser().fromXML(xmcda, startElement, eventReader));
			}
			else if (AFFINE.equals(startElement.getName().getLocalPart()))
			{
				affineFromXML(xmcda, threshold, startElement, eventReader);
			}
		}
		return threshold;
	}

	protected void affineFromXML(XMCDA xmcda, Threshold<?> threshold, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		Threshold.Type type = null;
		QualifiedValue slope = null, intercept = null;
		while ( eventReader.hasNext() )
		{
			XMLEvent event = eventReader.nextEvent();
			if (event.isEndElement())
			{
				if (AFFINE.equals(event.asEndElement().getName().getLocalPart()))
					break;
			}
			if ( ! event.isStartElement() )
				continue;

			startElement = event.asStartElement();
			if (TYPE.equals(startElement.getName().getLocalPart()))
			{
				type = Threshold.Type.valueOf(Utils.getTextContent(startElement, eventReader).toUpperCase());
			}
			else if (SLOPE.equals(startElement.getName().getLocalPart()))
			{
				slope = new QualifiedValueParser().fromXML(xmcda, startElement, eventReader);
			}
			else if (INTERCEPT.equals(startElement.getName().getLocalPart()))
			{
				intercept = new QualifiedValueParser().fromXML(xmcda, startElement, eventReader);
			}
		}
		if (type != null)
			threshold.setAffine(type, slope, intercept);
		else
			threshold.setAffine(slope, intercept);
	}

	public void toXML(Threshold<?> threshold, XMLStreamWriter writer) throws XMLStreamException
	{
		if ( threshold == null )
			return;
		writer.writeStartElement(THRESHOLD);
		new CommonAttributesParser().toXML(threshold, writer);
		writer.writeln();
		if (threshold.isConstant())
		{
			new QualifiedValueParser().toXML(CONSTANT, threshold.getConstant(), writer);
		}
		else // affine
		{
			writer.writeStartElement(AFFINE);
			writer.writeln();
			writer.writeElementChars(TYPE, threshold.getType().name().toLowerCase());
			new QualifiedValueParser().toXML(SLOPE, threshold.getSlope(), writer);
			new QualifiedValueParser().toXML(INTERCEPT, threshold.getIntercept(), writer);
			writer.writeEndElement(); // AFFINE
			writer.writeln();
		}
		writer.writeEndElement(); // THRESHOLD
		writer.writeln();
	}
	

}
