/**
 * 
 */
package org.xmcda.parsers.xml.xmcda_v3;

import java.util.List;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;

import org.xmcda.CategoriesMatrix;
import org.xmcda.Category;
import org.xmcda.Factory;
import org.xmcda.XMCDA;
import org.xmcda.utils.Matrix;

/**
 * @author Sébastien Bigaret
 */
public class CategoriesMatrixParser <VALUE_TYPE>
    extends MatrixParser<Category, VALUE_TYPE>
{
	public static final String CATEGORIES_MATRIX = CategoriesMatrix.TAG;

	@Override
	public String rootTag()
	{
		return CATEGORIES_MATRIX;
	}

	@Override
	public String dimensionTag()
	{
		return "categoryID";
	}

	@Override
	public Category buildDimension(XMCDA xmcda, String id)
	{
		return xmcda.categories.get(id);
	}

	@Override
	public String dimensionID(Category category)
	{
		return category.id();
	}

	@Override
	public CategoriesMatrix<VALUE_TYPE> fromXML(XMCDA xmcda, StartElement startElement, XMLEventReader eventReader)
	    throws XMLStreamException
	{
		Matrix<Category, VALUE_TYPE> matrix = super.fromXML(xmcda, startElement, eventReader);
		CategoriesMatrix<VALUE_TYPE> categoriesMatrix = Factory.categoriesMatrix();

		categoriesMatrix.setId(matrix.id());
		categoriesMatrix.setName(matrix.name());
		categoriesMatrix.setMcdaConcept(matrix.mcdaConcept());
		categoriesMatrix.setDescription(matrix.getDescription());
		categoriesMatrix.setValuation(matrix.getValuation());
		categoriesMatrix.putAll(matrix);
		return categoriesMatrix;
	}

	public void toXML(List<CategoriesMatrix<VALUE_TYPE>> list, XMLStreamWriter writer) throws XMLStreamException
	{
		if (list == null || list.size()==0 )
			return;
		for (CategoriesMatrix<VALUE_TYPE> matrix: list)
			toXML(matrix, writer);
	}

	public void toXML(CategoriesMatrix<VALUE_TYPE> matrix, XMLStreamWriter writer) throws XMLStreamException
	{
		super.toXML(matrix, writer);
	}
}
