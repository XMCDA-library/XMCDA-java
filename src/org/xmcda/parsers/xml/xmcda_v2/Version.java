package org.xmcda.parsers.xml.xmcda_v2;

import java.util.EnumSet;
import java.util.Optional;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;

public enum Version
{
    XMCDA_2_0_0("2.0.0",
                "/xsd/XMCDA-2.0.0.xsd",
                "http://www.decision-deck.org/2009/XMCDA-2.0.0",
                "http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.0.0.xsd"),
    XMCDA_2_1_0("2.1.0",
                "/xsd/XMCDA-2.1.0.xsd",
                "http://www.decision-deck.org/2009/XMCDA-2.1.0",
                "http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.1.0.xsd"),
    XMCDA_2_2_0("2.2.0",
                "/xsd/XMCDA-2.2.0.xsd",
                "http://www.decision-deck.org/2012/XMCDA-2.2.0",
                "http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.2.0.xsd"),
    XMCDA_2_2_1("2.2.1",
                "/xsd/XMCDA-2.2.1.xsd",
                "http://www.decision-deck.org/2012/XMCDA-2.2.1",
                "http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.2.1.xsd"),
    XMCDA_2_2_2("2.2.2",
                "/xsd/XMCDA-2.2.2.xsd",
                "http://www.decision-deck.org/2017/XMCDA-2.2.2",
                "http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.2.2.xsd"),
    XMCDA_2_2_3("2.2.3",
                "/xsd/XMCDA-2.2.3.xsd",
                "http://www.decision-deck.org/2019/XMCDA-2.2.3",
                "http://www.decision-deck.org/xmcda/_downloads/XMCDA-2.2.3.xsd");

    public final String          name;

    public final String          localXSDLocation;

    public final String          namespace;

    public final String          schemaURL;

    /** The list of all existing XMCDA v2 version numbers */
	public static final String[] VERSIONS = EnumSet.allOf(Version.class).stream().map(v -> v.name).toArray(String[]::new);

	public static final Version  defaultVersion = XMCDA_2_2_3;

	Version(String name, String local_xsd_location, String namespace, String schemaURL)
    {
        this.name = name;
        this.localXSDLocation = local_xsd_location;
        this.namespace = namespace;
        this.schemaURL = schemaURL;
    }

    /**
     * Return the requested v2 Version.
     * @param version the version number; it should be amongst {@link #VERSIONS}.
     * @return the requested v3 Version.
     */
    public static Optional<Version> get(String version)
    {
		return EnumSet.allOf(Version.class).stream().filter(v -> v.name.equals(version)).findFirst();
    }

    public static Source [] allSchemas()
    {
		return EnumSet.allOf(Version.class).stream()
		        .map(v -> new StreamSource(Version.class.getResourceAsStream(v.localXSDLocation)))
		        .toArray(Source[]::new);
    }

}