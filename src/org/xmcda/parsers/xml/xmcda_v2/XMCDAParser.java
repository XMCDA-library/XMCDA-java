package org.xmcda.parsers.xml.xmcda_v2;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Logger;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xmcda.v2.XMCDA;
import org.xml.sax.SAXException;

/**
 * @author Sébastien Bigaret
 */
public class XMCDAParser
{
	private final static Logger LOGGER = Logger.getLogger(XMCDAParser.class.getName());

	/**
	 * <b>DO NOT USE DIRECTLY, USE {@link #getJAXBContext()} INSTEAD!</b>.<br>
	 * The JAXBContext used by {@link #readXMCDA(File)} and {@link #writeXMCDA(XMCDA, File, String...)}: creating a
	 * JAXBContext is time-consuming, we create it once and for all (writeXMCDA is called as many times as the number
	 * of v2 files converted and written by the {@link org.xmcda.converters.v2_v3.XMCDAConverter XMCDAConverter} e.g.
	 */
	private static JAXBContext __cached_xmcdav2_jaxbContext;

	/** The JAXBContext to use when reading/writing XMCDA v2 files */
	private static synchronized JAXBContext getJAXBContext() throws JAXBException
	{
		if (__cached_xmcdav2_jaxbContext == null)
			__cached_xmcdav2_jaxbContext = JAXBContext.newInstance(org.xmcda.v2.XMCDA.class);
		return __cached_xmcdav2_jaxbContext;
	}

	/**
	 * <b>DO NOT USE DIRECTLY, USE {@link #getValidator()} INSTEAD!</b>.<br>
	 * The Validator used by {@link #readXMCDA(File)}. Creating a Validator is time-consuming, we create it once
	 * and for all (for example, readXMCDA is called as many times as the number of v2 files read by the
	 * {@link org.xmcda.converters.v2_v3.XMCDAConverter XMCDAConverter} before being converted to XMCDA v3.
	 */
	private static Validator   __cached_xmcdav2_validator = null;

	/** The validator to use when validating XMCDA v2 files 
	 * @throws SAXException */
	private static synchronized Validator getValidator() throws SAXException
	{
		if (__cached_xmcdav2_validator == null)
		{
			SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = schemaFactory.newSchema(Version.allSchemas());
			__cached_xmcdav2_validator = schema.newValidator();
		}
		return __cached_xmcdav2_validator;
	}

	public static XMCDA readXMCDA(String filename)
	throws IOException, JAXBException, SAXException
	{
		return readXMCDA(new File(filename), (String[]) null);
	}

	/**
	 * Reads an XMCDA file and builds the corresponding {@link XMCDA} object from it, keeping the tags in {@code
	 * tagsOnly} only.
	 *
	 * @param filename the name of the XMCDA file to read
	 * @param tagsOnly the list of tags that should be read from the file
	 * @return the XMCDA object build from the file where XML tags not in {@code tagsOnly} are filtered out.
	 * @throws FileNotFoundException
	 * @throws JAXBException
	 * @throws SAXException
	 */
	public static XMCDA readXMCDA(String filename, String... tagsOnly)
	throws IOException, JAXBException, SAXException
	{
		return readXMCDA(new File(filename), tagsOnly);
	}

	/**
	 * Reads an XMCDA file and builds the corresponding {@link XMCDA} object.
	 *
	 * @param file the XMCDA file to read
	 * @throws JAXBException
	 * @throws FileNotFoundException
	 * @throws SAXException
	 */
	public static XMCDA readXMCDA(File file) throws JAXBException, IOException, SAXException
	{
		return readXMCDA(file, (String[]) null);
	}

	/**
	 * Indicates whether {@code xmlTag} is in {@code tagsList}.
	 *
	 * @param xmlTag   an xml tag
	 * @param tagsList the list of tag in which the {@code xmlTag} is searched.
	 * @return {@code true} if the tag is in the list, {@code false} otherwise.
	 * @throws NullPointerException if {@code xmlTag} is {@code null}, or if tagsOnly contain {@code null} elements.
	 */
	private static boolean isTagInList(String xmlTag, String[] tagsList)
	throws NullPointerException
	{
		if ( xmlTag == null )
			throw new NullPointerException("xmlTag cannot be null");
		if ( tagsList == null || tagsList.length == 0 )
			return false;
		for ( String tagOnly : tagsList )
		{
			if ( tagOnly == null )
				throw new NullPointerException("tagsOnly cannot contain null values");
			if ( xmlTag.equals(tagOnly) )
				return true;
		}
		return false;
	}

	/**
	 * Removes all root elements in the xmcda object which are not in the supplied list tagsOnly.
	 *
	 * @param xmcda    the xmcda object to filter
	 * @param tagsOnly a list of non-null String. The list itself may be {@code null}: in this case, the {@code
	 *                 xmcda} object is left untouched.
	 * @throws NullPointerException tagsOnly contain {@code null} elements.
	 */
	private static void filterXMCDA(XMCDA xmcda, String... tagsOnly)
	{
		if ( tagsOnly == null || tagsOnly.length == 0 )
			return;

		// copy array since we're gonna iterate and remove element in it
		final ArrayList<JAXBElement<?>> xmcda_copy =
				new ArrayList<>(xmcda.getProjectReferenceOrMethodMessagesOrMethodParameters());

		for ( JAXBElement<?> tag : xmcda_copy )
		{
			final String tagName = tag.getName().getLocalPart();
			if ( !isTagInList(tagName, tagsOnly) )
				xmcda.getProjectReferenceOrMethodMessagesOrMethodParameters().remove(tag);
		}
	}

	/**
	 * Reads an XMCDA file and builds the corresponding {@link XMCDA} object from it, keeping the tags in {@code
	 * tagsOnly} only (all others are filtered out).
	 *
	 * @param file     the XMCDA v2 file to read and load
	 * @param tagsOnly a list of XMCDA root tags to keep
	 * @return the {@link XMCDA} v2 object
	 * @throws IOException
	 * @throws JAXBException
	 * @throws SAXException
	 */
	public static synchronized XMCDA readXMCDA(File file, String... tagsOnly)
	throws IOException, JAXBException, SAXException
	{
		validate(file);
		return readXMCDA_noValidation(new FileInputStream(file), tagsOnly);
	}

	public static synchronized void validate(File file) throws IOException, SAXException
	{
		validate(new StreamSource(file));
	}

	protected static synchronized void validate(InputStream inputStream) throws IOException, SAXException
	{
		validate(new StreamSource(new BufferedInputStream(inputStream) {
			@Override
			public void close() throws IOException
			{
				// ignore close() so that the stream can be mark()'ed/reset() and re-read, if possible
			}
		}));
	}

	protected static synchronized void validate(StreamSource streamSource) throws IOException, SAXException
	{
		Validator validator = getValidator();
		synchronized (validator) { // don't know whether .validate() is MT-safe or not
		try
		{
			getValidator().validate(streamSource);
		}
		catch (SAXException exc)
		{
			throw new SAXException("Validation failed", exc);
		}
		}
	}

	/** The maximum size of the stream that can be read by {@link #readXMCDA(XMCDA, InputStream, Set, String...)} */
	protected static int readXMCDAInputStreamMaxSize = 8192;
	
	protected static XMCDA readXMCDA(InputStream inputStream, String... tagsOnly)
	throws IOException, JAXBException, SAXException
	{
		BufferedInputStream bis = new BufferedInputStream(inputStream, readXMCDAInputStreamMaxSize);
		bis.mark(0);
		validate(bis);
		bis.reset();
		return readXMCDA_noValidation(bis, tagsOnly);
	}

	protected static XMCDA readXMCDA_noValidation(InputStream inputStream, String... tagsOnly) throws JAXBException
	{
		Unmarshaller unmarshaller = getJAXBContext().createUnmarshaller();
		// Force unmarshalling into v2.2.3 jaxb structure any source conforming to XMCDA v2.x.y
		final XMCDA xmcda = unmarshaller.unmarshal(new StreamSource(inputStream), org.xmcda.v2.XMCDA.class).getValue();

		filterXMCDA(xmcda, tagsOnly);
		return xmcda;
	}

	// Writing XMCDA

	public static final String XMCDAv2_VERSION = "XMCDAv2_VERSION";
	
    /**
	 * The default version to use when writing an XMCDA v2 file. The default is 2.2.3, except when the environment
	 * variable {@value #XMCDAv2_VERSION} is set and has a valid value, in which case this value takes precedence the
	 * first time the class is loaded (there won't be any changes afterwards, except if someone programmatically
	 * change the default.
	 */
	static private Version defaultXMCDAv2Version = Version.XMCDA_2_2_3;

	static
	{
	    String sys_env_v2 = System.getenv().getOrDefault(XMCDAv2_VERSION, null);
	    if ( sys_env_v2 != null )
	    {
	    	Optional<Version> env_version = Version.get(sys_env_v2);
	    	if ( env_version.isPresent() )
	    		defaultXMCDAv2Version = env_version.get();
	    	else
	    		LOGGER.warning(() -> "Invalid value of environment variable "+XMCDAv2_VERSION+" ("+sys_env_v2+")");
	    }
	}

	static public Version getDefaultXMCDAv2Version()
	{
	    return defaultXMCDAv2Version;
	}

    static public void setDefaultXMCDAv2Version(Version version)
    {
        if ( version == null )
            throw new NullPointerException("Parameter 'version' cannot be null");
        defaultXMCDAv2Version = version;
    }

	public static void writeXMCDA(XMCDA xmcda, String filename)
	throws JAXBException
	{
		writeXMCDA(xmcda, new File(filename), (String[]) null);
	}

	public static void writeXMCDA(XMCDA xmcda, String filename, String... tagsOnly)
	throws JAXBException
	{
		writeXMCDA(xmcda, new File(filename), tagsOnly);
	}

	public static void writeXMCDA(XMCDA xmcda, File file)
	throws JAXBException
	{
		writeXMCDA(xmcda, file, (String[]) null);
	}

	public static void writeXMCDA(XMCDA xmcda, File file, String... tagsOnly)
	throws JAXBException
	{
		writeXMCDA(xmcda, file, getDefaultXMCDAv2Version(), tagsOnly);
	}

	public static void writeXMCDA(XMCDA xmcda, File file, Version version, String... tagsOnly)
	throws JAXBException
	{
		Marshaller marshaller = getJAXBContext().createMarshaller();
		/*
		 * Note: in the marshalled XML the indentation is reset to 0 after level 8, this is hard-coded in the
		 * implementation. There are solutions but this is not that important, This note is here for anyone searching
		 * here why the indentation level is back to 0 at some point.
		 * cf. https://community.oracle.com/thread/2351779
		 *     https://stackoverflow.com/questions/53217170/jaxb-marshaller-and-formatting-of-output-xml/53217583
		 */
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		// keep a copy so that we can restore it unchanged before returning (it is modified by filterXMCDA)
		final ArrayList<JAXBElement<?>> xmcda_copy =
				new ArrayList<>(xmcda.getProjectReferenceOrMethodMessagesOrMethodParameters());
		try
		{
			filterXMCDA(xmcda, tagsOnly);
			XMCDAv2OutputStream xmcda2outs = new XMCDAv2OutputStream(new FileOutputStream(file), version);
			marshaller.marshal(xmcda, xmcda2outs);
			xmcda2outs.close();
		}
		catch (IOException jexc)
		{
			jexc.printStackTrace();
		}
		finally
		{
			// Restore the initial content
			xmcda.getProjectReferenceOrMethodMessagesOrMethodParameters().clear();
			xmcda.getProjectReferenceOrMethodMessagesOrMethodParameters().addAll(xmcda_copy);
		}
	}

	/**
	 * Returns the list of the root tags present in {@code xmcda_v2}.
	 * @param xmcda_v2 the XMCDA v2 object to examine
	 * @return The list of root tags present in {@code xmcda_v2}
	 */
	public static Set<String> rootTags(XMCDA xmcda_v2)
	{
		Set<String> rootTags = new HashSet<String>();
		for (JAXBElement<?> element: xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters())
			rootTags.add(element.getName().getLocalPart());
		return rootTags;
	}

	public static void main(String[] args) throws Exception
	{
		readXMCDA("alternatives.xml");
	}
}
