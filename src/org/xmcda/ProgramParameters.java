package org.xmcda;

import java.util.ArrayList;

/**
 * xmcda:type:programParameters
 *
 * @author Sébastien Bigaret
 */
public class ProgramParameters <VALUE_TYPE>
    extends ArrayList<ProgramParameter<VALUE_TYPE>>
    implements HasDescription, CommonAttributes, java.io.Serializable, XMCDARootElement
{
	private static final long serialVersionUID = 1L;

	public static final String TAG = "programParameters";

	public ProgramParameters()
	{ super(); }

	/**
	 * Returns the parameter with the supplied id, or {@code null} if it does not exist
	 * @param id the id of the parameter to return
	 * @return the parameter with the supplied id, or null if there is no such parameter.
	 */
	public ProgramParameter<VALUE_TYPE> getParameter(String id)
	{
		for (ProgramParameter<VALUE_TYPE> parameter: this)
			if ( id.equals(parameter.id()) )
				return parameter;
		return null;
	}

	// CommonAttributes (start)

	/** The id attribute allows to identify the underlying piece of data by a program. */
	private String            id;

	/** The name attribute contains the human readable name of the object or concept. */
	private String            name;

	/**
	 * The mcdaConcept attribute allows to specify to what mcda concept a tag is related. It is used by an algorithm to
	 * make choices which will have an influence on the output. The documentation of the program should therefore
	 * specify, if necessary, what mcdaConcept should be used for the input data. In particular, if an algorithm
	 * requires, among other things, twice the same input tag, they can be differenciated by the mcdaConcept (this is
	 * even mandatory if they are present in the same file, but should be optional if the two tags can be in different
	 * input files, or originate from two different programs). The algorithm should therefore not be too strict on these
	 * mcdaConcepts, as this will reduce the compatibility between the various programs.
	 */
	private String            mcdaConcept;

	@Override
	public String id()
	{
		return id;
	}

	@Override
	public void setId(String id)
	{
		this.id = id;
	}

	@Override
	public String name()
	{
		return name;
	}

	@Override
	public String mcdaConcept()
	{
		return mcdaConcept;
	}

	@Override
	public void setName(String name)
	{
		this.name = name;
	}

	@Override
	public void setMcdaConcept(String mcdaConcept)
	{
		this.mcdaConcept = mcdaConcept;
	}

	// CommonAttributes (end)

	// HasDescription (start)

	private Description description;
	@Override
	public void setDescription(Description description)
	{
		this.description = description;
	}

	@Override
	public Description getDescription()
	{
		return description;
	}

	// HasDescription (end)

}
