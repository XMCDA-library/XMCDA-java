/**
 * 
 */
package org.xmcda;

import org.xmcda.utils.ValueConverters.ConversionException;

import java.util.ArrayList;

/**
 * @author Sébastien Bigaret
 *
 */
public class QualifiedValues<T> extends ArrayList<QualifiedValue<T>>
{
	// TODO ok on a une liste de valeurs, mais accès direct dans le cas d'une seule? à reporter sur performanceTable niveau API non?
    private static final long serialVersionUID = 1L;

    public QualifiedValues() { super(); }

    public QualifiedValues(QualifiedValue<T> value)
    {
    	super();
    	this.add(value);
    }
    
	// ArrayList should not be empty
	public boolean isValid()
	{
		return this.size() > 0;
	}

	/**
	 * Converts this object's values to Double. This a equivalent to {@link #convertTo(Class)
	 * this.convertTo(Double.class)}.
	 *
	 * @return this object, with all its values converted to Double.
	 * @throws ConversionException if the value could not be converted to a double. Note that this may happen even if
	 *             {@link #isNumeric()} is {@code True}: converting a rational with a denominator equal to zero throw
	 *             a ConversionException.
	 * @see #isNumeric()
	 */
	public QualifiedValues<Double> convertToDouble()
	throws ConversionException
	{
		return convertTo(Double.class);
	}

	/** 
	 * Tells if all values are {@link QualifiedValue#isNumeric() numeric}.
	 * 
	 * @return {@code true} if all values are numeric, false otherwise.
	 * @see #convertToDouble()
	 */
	public boolean isNumeric()
	{
		for (QualifiedValue<T> value: this)
			if ( ! value.isNumeric() )
				return false;
		return true;
	}

	/**
	 * Checks that the conversion can be done on all of the object's {@link QualifiedValue}, but do not modify these
	 * values.
	 * 
	 * @param <U> the class to which the value should be converted
	 * @param clazz the class to which the value should be converted
	 * @throws ConversionException raised if the conversion cannot be done
	 * @see #convertTo(Class)
	 */
	public <U> void checkConversion(Class <U> clazz) throws ConversionException
	{
		for (QualifiedValue<T> value: this)
			QualifiedValue.checkConversion(value, clazz);
	}

	/**
	 * Converts this object's values to the specified type. If the conversion cannot be done,
	 * {@code ConversionException} is raised, and the values are left untouched.
	 * 
	 * @param <U> class of the converted values
	 * @param clazz the class of the converted values
	 * @return this object
	 * @throws ConversionException if at least one element could not be converted
	 * @see #checkConversion(Class)
	 * @see #convertToDouble()
	 */
	@SuppressWarnings("unchecked")
	public <U> QualifiedValues<U> convertTo(Class <U> clazz) throws ConversionException
	{
		// Try to convert all values: if a conversion fails, the values are not touched
		for (QualifiedValue<T> value: this)
			QualifiedValue.checkConversion(value, clazz);
		// Now convert
		for (QualifiedValue<T> value: this)
			value.convertTo(clazz);
		return (QualifiedValues<U>) this;
	}
}
