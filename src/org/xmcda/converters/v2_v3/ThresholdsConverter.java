/**
 *
 */
package org.xmcda.converters.v2_v3;

/**
 * @author Sébastien Bigaret <sebastien.bigaret@telecom-bretagne.eu>
 *
 */
public class ThresholdsConverter
extends Converter
{
	public static final String THRESHOLDS = "thresholds";
	public static final String THRESHOLD  = "threshold";

	public ThresholdsConverter()
	{
		super(THRESHOLDS);
	}

	public void convertTo_v3(org.xmcda.v2.Thresholds thresholds,
	                         org.xmcda.CriterionThresholds critThresholds_v3, org.xmcda.XMCDA xmcda_v3)
	{
		getWarnings().pushTag(THRESHOLDS, thresholds.getId());
		critThresholds_v3.setId(thresholds.getId());
		critThresholds_v3.setName(thresholds.getName());
		critThresholds_v3.setMcdaConcept(thresholds.getMcdaConcept());
		critThresholds_v3.setDescription(new DescriptionConverter().convertTo_v3(thresholds.getDescription()));

		for (org.xmcda.v2.Function threshold_v2: thresholds.getThreshold())
		{
			critThresholds_v3.add(new ThresholdConverter().convertTo_v3(threshold_v2, xmcda_v3));
		}
		getWarnings().popTag(); // THRESHOLDS
	}

	public org.xmcda.v2.Thresholds convertTo_v2(org.xmcda.CriterionThresholds criterionThresholds_v3)
	{
		getWarnings().pushTag(THRESHOLDS, criterionThresholds_v3.id());
		final org.xmcda.v2.Thresholds thresholds_v2 = new org.xmcda.v2.Thresholds();
		for (org.xmcda.Threshold<?> criterionThreshold_v3: criterionThresholds_v3)
		{
			//new FunctionConverter().convertTo_v2(criterion_v2, criterionThreshold_v3);
			final org.xmcda.v2.Function threshold_v2 = new ThresholdConverter().convertTo_v2(criterionThreshold_v3);
			thresholds_v2.getThreshold().add(threshold_v2);
		}
		thresholds_v2.setId(criterionThresholds_v3.id());
		thresholds_v2.setName(criterionThresholds_v3.name());
		thresholds_v2.setMcdaConcept(criterionThresholds_v3.mcdaConcept());
		getWarnings().popTag(); // THRESHOLDS
		return thresholds_v2;
	}
}
