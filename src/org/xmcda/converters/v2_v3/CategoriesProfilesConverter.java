package org.xmcda.converters.v2_v3;

import java.util.LinkedHashMap;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

/**
 * @author Sébastien Bigaret
 */
public class CategoriesProfilesConverter
extends Converter
{
	public static final String CATEGORIES_PROFILES = org.xmcda.CategoriesProfiles.TAG;

	public static final String CATEGORY_PROFILE	   = org.xmcda.CategoryProfile.TAG;

	public CategoriesProfilesConverter()
    {
		super(CATEGORIES_PROFILES);
    }

	public <T> void convertTo_v3(org.xmcda.v2.CategoriesProfiles value, org.xmcda.XMCDA xmcda_v3)
	{
		getWarnings().pushTag(CATEGORIES_PROFILES, value.getId());

		org.xmcda.CategoriesProfiles<T> categoriesProfiles_v3 = org.xmcda.Factory.<T> categoriesProfiles();
		org.xmcda.CategoryProfile<T> categoryProfile_v3;

		// defaultAttributes
		categoriesProfiles_v3.setId(value.getId());
		categoriesProfiles_v3.setMcdaConcept(value.getMcdaConcept());
		categoriesProfiles_v3.setName(value.getName());

		// description
		categoriesProfiles_v3.setDescription(new DescriptionConverter().convertTo_v3(value.getDescription()));

		// categoryProfile 0..*
		// gather everything before creating v3:
		// v2 declares alternativeID then categoryID, and v3 goes the other way round

		/* map is structured as follows:
		   categoryID, { lower-alternativeID, lower-values, upper-alternativeID, upper-values } */
		LinkedHashMap<String, Object[]> map = new LinkedHashMap<>();
		for (org.xmcda.v2.CategoryProfile categoryProfile: value.getCategoryProfile())
		{
			final String alternativeID = categoryProfile.getAlternativeID();
			getWarnings().pushTag(CATEGORY_PROFILE, "alternativeID: "+alternativeID);

			/* None of the following can be mapped to the v3 version because they are attached to an alternativeID,
			 * not on (alternativeID, categoryID) */
			if (categoryProfile.getId() != null) getWarnings().attributeIgnored("id");
			if (categoryProfile.getName() != null) getWarnings().attributeIgnored("name");
			if (categoryProfile.getMcdaConcept() != null) getWarnings().attributeIgnored("mcdaConcept");
			if (categoryProfile.getDescription() != null) getWarnings().elementIgnored("description");

			final org.xmcda.v2.CategoryProfile.Limits limits = categoryProfile.getLimits();
			if ( limits != null )
			{
				final org.xmcda.v2.CategoryProfileBound[] bounds = { limits.getLowerCategory(),
				                                                     limits.getUpperCategory() };

				for ( int i=0; i<2; i++ )
				{
					if ( bounds[i] == null)
						continue;
					final String cID = bounds[i].getCategoryID();
					if ( ! map.containsKey(cID) )
						map.put(cID, new Object[]{null, null, null, null});

					map.get(cID)[2*i]   = alternativeID;
					map.get(cID)[2*i+1] = bounds[i].getValueOrValues();
				}
			}
			else
			{
				final org.xmcda.v2.CategoryProfile.Central central = categoryProfile.getCentral();

				categoryProfile_v3 = org.xmcda.Factory.categoryProfile(org.xmcda.CategoryProfile.Type.CENTRAL);

				categoryProfile_v3.setId(categoryProfile.getId());
				categoryProfile_v3.setName(categoryProfile.getName());
				categoryProfile_v3.setMcdaConcept(categoryProfile.getMcdaConcept());
				categoryProfile_v3.setDescription(new DescriptionConverter().convertTo_v3(categoryProfile.getDescription()));

				org.xmcda.CategoryProfile.Profile<T> profile_v3 = new org.xmcda.CategoryProfile.Profile<T>();
				profile_v3.setAlternative(xmcda_v3.alternatives.get(alternativeID));
				profile_v3.setValues(ValuesConverter.valueOrValues_convertTo_v3(central.getValueOrValues(),
				                                                                xmcda_v3, this.getWarnings()));
				categoryProfile_v3.setCategory(xmcda_v3.categories.get(central.getCategoryID()));
				categoryProfile_v3.setCentralProfile(profile_v3);

				categoriesProfiles_v3.add(categoryProfile_v3);
			}
			getWarnings().popTag(); // CATEGORY_PROFILE
		}

		// build limits
		for (String categoryID: map.keySet())
		{
			org.xmcda.CategoryProfile.Profile<T> profile_v3;
				categoryProfile_v3 = org.xmcda.Factory.categoryProfile(org.xmcda.CategoryProfile.Type.BOUNDING);

			final Object[] limit = map.get(categoryID);

			categoryProfile_v3.setCategory(xmcda_v3.categories.get(categoryID));

			if ( limit[0] != null)
			{
				profile_v3 = new org.xmcda.CategoryProfile.Profile<T>();
				profile_v3.setAlternative(xmcda_v3.alternatives.get((String) limit[0]));

				@SuppressWarnings("unchecked")
				final List<Object> values_v3 = (List<Object>) limit[1];
				profile_v3.setValues(ValuesConverter.valueOrValues_convertTo_v3(values_v3,
				                                                                xmcda_v3, this.getWarnings()));
				categoryProfile_v3.setUpperBound(profile_v3);
			}
			if ( limit[2] != null)
			{
				profile_v3 = new org.xmcda.CategoryProfile.Profile<T>();
				profile_v3.setAlternative(xmcda_v3.alternatives.get((String) limit[2]));

				@SuppressWarnings("unchecked")
				final List<Object> values_v3 = (List<Object>) limit[3];
				profile_v3.setValues(ValuesConverter.valueOrValues_convertTo_v3(values_v3,
				                                                                xmcda_v3, this.getWarnings()));
				categoryProfile_v3.setLowerBound(profile_v3);
			}
			categoriesProfiles_v3.add(categoryProfile_v3);
		}

		xmcda_v3.categoriesProfilesList.add(categoriesProfiles_v3);

		getWarnings().popTag(); // CATEGORIES_PROFILES
	}

	public void convertTo_v2(final List<org.xmcda.CategoriesProfiles<?>> categoriesProfiles_v3,
	                         org.xmcda.v2.XMCDA xmcda_v2)
	{
		for (org.xmcda.CategoriesProfiles<?> categoriesProfiles: categoriesProfiles_v3)
		{
			org.xmcda.v2.CategoriesProfiles catProfiles_v2 = convertTo_v2(categoriesProfiles, xmcda_v2);
			List<JAXBElement<?>> catProfilesList_v2 = xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters();
			catProfilesList_v2.add(new JAXBElement<org.xmcda.v2.CategoriesProfiles>(new QName(CATEGORIES_PROFILES), org.xmcda.v2.CategoriesProfiles.class, catProfiles_v2));
		}
	}

	protected <T> org.xmcda.v2.CategoriesProfiles convertTo_v2(final org.xmcda.CategoriesProfiles<T> categoriesProfiles_v3,
	                                                       org.xmcda.v2.XMCDA xmcda_v2)
	{
		getWarnings().pushTag(CATEGORIES_PROFILES, categoriesProfiles_v3.id());

		org.xmcda.v2.CategoriesProfiles categoriesProfiles_v2 = new org.xmcda.v2.CategoriesProfiles();

		// default attributes
		categoriesProfiles_v2.setId(categoriesProfiles_v3.id());
		categoriesProfiles_v2.setName(categoriesProfiles_v3.name());
		categoriesProfiles_v2.setMcdaConcept(categoriesProfiles_v3.mcdaConcept());

		// description
		categoriesProfiles_v2.setDescription(new DescriptionConverter().convertTo_v2(categoriesProfiles_v3.getDescription()));

		// categoryProfile 0..*
		// gather everything before creating v2:
		// v3 declares categoryID then alternativeID, and v2 goes the other way round.

		/* map is structured as follows:
		   categoryID, { lower-alternativeID, lower-values , upper-alternativeID, upper-values } */
		LinkedHashMap<String, Object[]> map = new LinkedHashMap<>();

		for (org.xmcda.CategoryProfile<T> categoryProfile_v3: categoriesProfiles_v3)
		{
			final String categoryID = categoryProfile_v3.getCategory().id();
			getWarnings().pushTag(CATEGORY_PROFILE, "categoryID: "+categoryID);

			if ( categoryProfile_v3.getType() == org.xmcda.CategoryProfile.Type.BOUNDING )
			{

				/* None of the following can be mapped to the v3 version because they are attached to an alternativeID,
				 * not on (alternativeID, categoryID) */
				if (categoryProfile_v3.id() != null) getWarnings().attributeIgnored("id");
				if (categoryProfile_v3.name() != null) getWarnings().attributeIgnored("name");
				if (categoryProfile_v3.mcdaConcept() != null) getWarnings().attributeIgnored("mcdaConcept");
				if (categoryProfile_v3.getDescription() != null) getWarnings().elementIgnored("description");

				@SuppressWarnings("rawtypes") // Cannot build array of generic Profile<T>
				final org.xmcda.CategoryProfile.Profile[] bounds = { categoryProfile_v3.getLowerBound(),
				                                                     categoryProfile_v3.getUpperBound() };

				for ( int i=0; i<2; i++ )
				{
					if ( bounds[i] == null)
						continue;
					final String aID = bounds[i].getAlternative().id();
					if ( ! map.containsKey(aID) )
						map.put(aID, new Object[]{null, null, null, null});

					map.get(aID)[2*i]   = categoryID;
					map.get(aID)[2*i+1] = bounds[i].getValues();
				}
			}
			else
			{
				// Central
				org.xmcda.v2.CategoryProfile categoryProfile_v2 = new org.xmcda.v2.CategoryProfile();
				org.xmcda.v2.CategoryProfile.Central central = new org.xmcda.v2.CategoryProfile.Central();
				central.setCategoryID(categoryID);
				org.xmcda.QualifiedValues<T> values = categoryProfile_v3.getCentralProfile().getValues();
				if ( values != null )
					central.getValueOrValues().add(ValuesConverter.convertTo_v2(values));
				categoryProfile_v2.setCentral(central);
				categoryProfile_v2.setAlternativeID(categoryProfile_v3.getCentralProfile().getAlternative().id());
				categoriesProfiles_v2.getCategoryProfile().add(categoryProfile_v2);
			}
			getWarnings().popTag(); // CATEGORY_PROFILE
		}
		// build bounds
		for (String alternativeID: map.keySet())
		{
			org.xmcda.v2.CategoryProfile categoryProfile_v2 = new org.xmcda.v2.CategoryProfile();
			org.xmcda.v2.CategoryProfile.Limits limits_v2 = new org.xmcda.v2.CategoryProfile.Limits();
			categoryProfile_v2.setAlternativeID(alternativeID);
			categoryProfile_v2.setLimits(limits_v2);

			final Object[] bounds = map.get(alternativeID);

			if ( bounds[0] != null )
			{
				org.xmcda.v2.CategoryProfileBound bound = new org.xmcda.v2.CategoryProfileBound();
				bound.setCategoryID((String) bounds[0]);

				@SuppressWarnings("unchecked")
				final org.xmcda.QualifiedValues<T> values = ( (org.xmcda.QualifiedValues<T>) bounds[1] );
				if ( values != null )
					bound.getValueOrValues().add(ValuesConverter.convertTo_v2(values));
				limits_v2.setUpperCategory(bound);
			}
			if ( bounds[2] != null )
			{
				org.xmcda.v2.CategoryProfileBound bound = new org.xmcda.v2.CategoryProfileBound();
				bound.setCategoryID((String) bounds[2]);

				@SuppressWarnings("unchecked")
				org.xmcda.QualifiedValues<T> values = (org.xmcda.QualifiedValues<T>) bounds[3];
				if ( values != null )
					bound.getValueOrValues().add(ValuesConverter.convertTo_v2(values));
				limits_v2.setLowerCategory(bound);
			}
			categoriesProfiles_v2.getCategoryProfile().add(categoryProfile_v2);
		}

		getWarnings().popTag(); // CATEGORIES_PROFILE
		return categoriesProfiles_v2;
	}

}
