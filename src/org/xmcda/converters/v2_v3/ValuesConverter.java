package org.xmcda.converters.v2_v3;

import java.util.List;

public final class ValuesConverter
{
	public static final String VALUES = "values";
	public static final String VALUE  = "value";

	/**
	 * 
	 */
	private static ThreadLocal<Boolean> embedMultipleXMCDAv2ValueInValues = new ThreadLocal<Boolean>() {
		@Override protected Boolean initialValue() { return false; }
	};

	/**
	 * Configures the behaviour of {@link #convertTo_v2(org.xmcda.QualifiedValues)}.
	 * 
	 * @param bool whether {@link #convertTo_v2(org.xmcda.QualifiedValues)}
	 */
	public static void setEmbedMultipleXMCDAv2ValueInValues(boolean bool)
	{
		embedMultipleXMCDAv2ValueInValues.set(bool);
	}

	/**
	 * Indicates whether the method {@link #convertTo_v2(org.xmcda.QualifiedValues)} should return
	 * {@link org.xmcda.v2.Value} when possible.
	 * 
	 * @return true if convertTo_v2() should return org.xmcda.v2.Value when possible.
	 */
	public static boolean embedMultipleXMCDAv2ValueInValues()
	{
		return embedMultipleXMCDAv2ValueInValues.get();
	}

	private ValuesConverter() {}

	/**
	 * Converts value or values in XMCDA v2 into LabelledQValues in XMCDA v3
	 * @param valueOrValues
	 * @param xmcda_v3
	 * @param warnings
	 * @return
	 */
	public static <T> org.xmcda.LabelledQValues<T> valueOrValues_convertTo_v3(@SuppressWarnings("rawtypes") List valueOrValues,
	                                                                          org.xmcda.XMCDA xmcda_v3,
	                                                                          Warnings warnings)
	{
		org.xmcda.LabelledQValues<T> values_v3 = org.xmcda.Factory.<T>labelledQValues();
//		if ( valueOrValues==null || valueOrValues.size()==0 )
//		{
//			// no value: assign N/A
//			@SuppressWarnings("rawtypes")
//			org.xmcda.QualifiedValue tmpValue = org.xmcda.Factory.qualifiedValue();
//			tmpValue.setValue(org.xmcda.value.NA.na);
//			values_v3.add(tmpValue);
//			return values_v3;
//		}

		for (Object o: valueOrValues)
		{
			if (o instanceof org.xmcda.v2.Value)
			{
				warnings.pushTag(VALUE);
				final org.xmcda.v2.Value value_v2 = (org.xmcda.v2.Value) o;
				values_v3.add(new QualifiedValueConverter().<T> convertTo_v3(value_v2, xmcda_v3));
				warnings.popTag(); // VALUE
			}
			else if (o instanceof org.xmcda.v2.Values)
			{
				final org.xmcda.v2.Values values_v2 = (org.xmcda.v2.Values) o;
				warnings.pushTag(VALUES, values_v2.getId());

				for (org.xmcda.v2.Value value_v2: values_v2.getValue())
				{
					values_v3.add(new QualifiedValueConverter().<T> convertTo_v3(value_v2, xmcda_v3));
				}

				// attributes {id, name, mcdaConcept}, child element <description> in v2 <values>: absent in v3
				if ( values_v2.getId() != null )
					warnings.attributeIgnored("id", Warnings.ABSENT_IN_V3_0);
				if ( values_v2.getName() != null )
					warnings.attributeIgnored("name", Warnings.ABSENT_IN_V3_0);
				if ( values_v2.getName() != null )
					warnings.attributeIgnored("mcdaConcept", Warnings.ABSENT_IN_V3_0);
				if ( values_v2.getDescription() != null )
					warnings.elementIgnored("description", Warnings.ABSENT_IN_V3_0);

				warnings.popTag(); // VALUES
			}
		}

		return values_v3;
	}

	/**
	 * Converts XMCDA v3 values to XMCDA v2 values or value. More precisely:
	 * <ul>
	 * <li>if {@code values_v3} is {@code null}, returns {@code null};
	 * <li>if {@code values_v3} is empty, returns {@code null}: an empty {@code <values/>} is <b>invalid</b> in XMCDA
	 * v2;
	 * <li>if {@link #embedMultipleXMCDAv2ValueInValues()} is true, returns a {@link org.xmcda.v2.Value xmcda v2 value}
	 * values when {@code values_v3} has one element;
	 * <li>otherwise, return a {@link org.xmcda.v2.Values xmcda v2 values}.
	 * </ul>
	 * 
	 * @param values_v3 the XMCDA v3 values to be converted.
	 * @return the converted object, or {@code null} (see above for details).
	 */
	public static <VALUE_TYPE> Object convertTo_v2(org.xmcda.QualifiedValues<VALUE_TYPE> values_v3)
	{
		if (values_v3 == null)
			return null;

		if (embedMultipleXMCDAv2ValueInValues() && values_v3.size()==1)
			return new QualifiedValueConverter().convertTo_v2(values_v3.get(0));

		if (values_v3.isEmpty()) // if there is no value, return null: an empty <values/> is *invalid* in XMCDA v2
			return null;

		org.xmcda.v2.Values values_v2 = new org.xmcda.v2.Values();
		for (org.xmcda.QualifiedValue<VALUE_TYPE> value_v3: values_v3)
		{
			org.xmcda.v2.Value value_v2 = new QualifiedValueConverter().convertTo_v2(value_v3);
			values_v2.getValue().add(value_v2);
		}
		return values_v2;
	}
}
