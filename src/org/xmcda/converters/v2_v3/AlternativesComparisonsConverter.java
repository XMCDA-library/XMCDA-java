/**
 * 
 */
package org.xmcda.converters.v2_v3;

import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import org.xmcda.Factory;
import org.xmcda.QualifiedValue;

/**
 * @author Sébastien Bigaret <sebastien.bigaret@telecom-bretagne.eu>
 *
 */
public class AlternativesComparisonsConverter
extends Converter
{
	public static final String ALTERNATIVES_COMPARISONS = "alternativesComparisons";
	public static final String PAIRS                    = "pairs";
	public static final String PAIR                     = "pair";

	public AlternativesComparisonsConverter()
	{
		super(ALTERNATIVES_COMPARISONS); // ajouter un constructeur super(tag_v2, tag_v3) ?
	}
	
	// v2 -> v3
	
	public void convertTo_v3(org.xmcda.v2.AlternativesComparisons value, org.xmcda.XMCDA xmcda_v3)
	{
		// determine whether we convert alternatives or alternativesSets/alternativesSetIDs
		final org.xmcda.v2.AlternativesComparisons.Pairs pairs = value.getPairs();
		if ( pairs == null || pairs.getPair().size()==0 )
		{
			convertAlternativesComparisonsTo_v3(value, xmcda_v3);
			return;
		}
		org.xmcda.v2.AlternativesComparisons.Pairs.Pair pair = pairs.getPair().get(0);
		String initial_id = pair.getInitial().getAlternativeID();
		if ( initial_id != null )
		{
			// either only alternativesSets ...
			convertAlternativesComparisonsTo_v3(value, xmcda_v3);
		}
		else
		{
			// ... or alternativesSets / alternativesSetIDs
			convertAlternativesSetsComparisonsTo_v3(value, xmcda_v3);
		}
	}

	// Implementation note:
	// convertAlternativesComparisonsTo_v3 & convertAlternativesSetsComparisonsTo_v3 are very similar
	// Changes in one probably means changing the other one 
	public void convertAlternativesSetsComparisonsTo_v3(org.xmcda.v2.AlternativesComparisons value, org.xmcda.XMCDA xmcda_v3)
	{
		getWarnings().pushTag(ALTERNATIVES_COMPARISONS, value.getId());
		
		org.xmcda.AlternativesSetsMatrix<?,?> alternativesSetsMatrix_v3 = Factory.alternativesSetsMatrix();
		// defaultAttributes
		alternativesSetsMatrix_v3.setId(value.getId());
		alternativesSetsMatrix_v3.setMcdaConcept(value.getMcdaConcept());
		alternativesSetsMatrix_v3.setName(value.getName());
		alternativesSetsMatrix_v3.setDescription(new DescriptionConverter().convertTo_v3(value.getDescription()));

		// valuation: scale
		if ( value.getValuation() != null )
			alternativesSetsMatrix_v3.setValuation(new ScaleConverter().convertTo_v3(value.getValuation(), xmcda_v3));

		// pair 0..*
		if (value.getPairs().getDescription() != null)
			getWarnings().elementIgnored("description", Warnings.ABSENT_IN_V3_0);

		// 
		final org.xmcda.v2.AlternativesComparisons.Pairs pairs = value.getPairs();

		// TODO v2 accepts empty Matrix
		if ( pairs == null || pairs.getPair().size()==0 )
		{
			getWarnings().elementUnimplemented("empty v2"); // TODO c'est pas unimplemented qu'il faut
			return;
		}

		getWarnings().pushTag(PAIRS, value.getId());

		/* comparisonType is present in examples of weightsFromCondorcetAndPreferences only
		 * and it is not used.  It is not present in v3.  The idea is that, if something like that
		 * is necessary, it should be passed as a methodParameter.
		 */
		final String comparisonType = value.getComparisonType();
		if ( comparisonType!=null && !"".equals(comparisonType) )
			getWarnings().elementIgnored("comparisonType");

		// pair[0..*]
		for (org.xmcda.v2.AlternativesComparisons.Pairs.Pair pair: pairs.getPair())
		{
			/* one of them */
			org.xmcda.AlternativesSet<?> initial = null;
			
			String initial_id = pair.getInitial().getAlternativeID();
			String initial_set_id = pair.getInitial().getAlternativesSetID();
			org.xmcda.v2.AlternativesSet initial_set = pair.getInitial().getAlternativesSet();
			
			if ( initial_id != null )
			{
				throw new IllegalArgumentException("alternativeID unexpected");
			}
			else if ( initial_set_id != null )
			{
				initial = xmcda_v3.alternativesSets.get(initial_set_id, true);
			}
			else if ( initial_set != null )
			{
				// the converter already assigns a generated id if there was none
				initial = new AlternativesSetConverter().convertTo_v3(initial_set, xmcda_v3);
				xmcda_v3.alternativesSets.add((org.xmcda.AlternativesSet)initial);
			}
			else
				throw new IllegalArgumentException("Invalid pair, no alternativesSetID nor alternativesSet in initial");
			/* */
			org.xmcda.AlternativesSet<?> terminal = null;

			String terminal_id = pair.getTerminal().getAlternativeID();
			String terminal_set_id = pair.getTerminal().getAlternativesSetID();
			org.xmcda.v2.AlternativesSet terminal_set = pair.getTerminal().getAlternativesSet();
			
			if ( terminal_id != null )
			{
				throw new IllegalArgumentException("alternativeID unexpected");
			}
			else if ( terminal_set_id != null )
			{
				terminal = xmcda_v3.alternativesSets.get(terminal_set_id, true);
			}
			else if ( terminal_set != null )
			{
				// the converter already assigns a generated id if there was none
				terminal = new AlternativesSetConverter().convertTo_v3(terminal_set, xmcda_v3);
				xmcda_v3.alternativesSets.add((org.xmcda.AlternativesSet)terminal);
			}
			else
				throw new IllegalArgumentException("Invalid pair, no alternativesSetID nor alternativesSet in initial");

			// alternativesSetsMatrix en v3 ne contient que des alternativeID row/column
			List<Object> pairValue_v2 = new java.util.ArrayList<Object>();

			for (Object _v: pair.getValue())
				pairValue_v2.add(_v);
			for (Object _v: pair.getValues())
				pairValue_v2.add(_v);

			org.xmcda.QualifiedValues values_v3 = Factory.qualifiedValues();
			if ( pairValue_v2==null || pairValue_v2.size()==0 )
			{
				// no value: assign N/A
				QualifiedValue tmpValue = Factory.qualifiedValue();
				tmpValue.setValue(org.xmcda.value.NA.na);
				values_v3.add(tmpValue);
			}
			else
			{
				// iterate on valueOrValues, convert them and and add them to values_v3
				for (Object valueOrValues: pairValue_v2)
				{
					if (valueOrValues instanceof org.xmcda.v2.Value)
					{
						org.xmcda.v2.Value tmpValue_v2 = (org.xmcda.v2.Value) valueOrValues;
						values_v3.add(new QualifiedValueConverter().convertTo_v3(tmpValue_v2, xmcda_v3));
					}
					else
					{
						org.xmcda.v2.Values tmpValues_v2 = (org.xmcda.v2.Values) valueOrValues;
						// TODO ignore description mcdaConcept etc.
						for (org.xmcda.v2.Value tmpValue_v2 : tmpValues_v2.getValue())
						{
							values_v3.add(new QualifiedValueConverter().convertTo_v3(tmpValue_v2, xmcda_v3));
						}
					}
				}
			}
			alternativesSetsMatrix_v3.put(new org.xmcda.utils.Coord(initial, terminal), values_v3);
		}
		getWarnings().popTag(); // PAIRS

		getWarnings().popTag(); // ALTERNATIVES_COMPARISONS
		xmcda_v3.alternativesSetsMatricesList.add(alternativesSetsMatrix_v3);
	}

	// Implementation note:
	// convertAlternativesComparisonsTo_v3 & convertAlternativesSetsComparisonsTo_v3 are very similar
	// Changes in one probably means changing the other one 
	public void convertAlternativesComparisonsTo_v3(org.xmcda.v2.AlternativesComparisons value, org.xmcda.XMCDA xmcda_v3)
	{
		getWarnings().pushTag(ALTERNATIVES_COMPARISONS, value.getId());

		org.xmcda.AlternativesMatrix<?> alternativesMatrix_v3 = Factory.alternativesMatrix();
		// defaultAttributes
		alternativesMatrix_v3.setId(value.getId());
		alternativesMatrix_v3.setMcdaConcept(value.getMcdaConcept());
		alternativesMatrix_v3.setName(value.getName());
		alternativesMatrix_v3.setDescription(new DescriptionConverter().convertTo_v3(value.getDescription()));

		// valuation: scale
		if ( value.getValuation() != null )
			alternativesMatrix_v3.setValuation(new ScaleConverter().convertTo_v3(value.getValuation(), xmcda_v3));
		
		// pair 0..*
		if (value.getPairs().getDescription() != null)
			getWarnings().elementIgnored("description", Warnings.ABSENT_IN_V3_0);

		// 
		final org.xmcda.v2.AlternativesComparisons.Pairs pairs = value.getPairs();

		// TODO v2 accepts empty Matrix
		if ( pairs == null || pairs.getPair().size()==0 )
		{
			getWarnings().elementUnimplemented("empty v2"); // TODO c'est pas unimplemented qu'il faut
			return;
		}

		getWarnings().pushTag(PAIRS, value.getId());

		/* comparisonType is present in examples of weightsFromCondorcetAndPreferences only
		 * and it is not used.  It is not present in v3.  The idea is that, if something like that
		 * is necessary, it should be passed as a programParameter.
		 */
		final String comparisonType = value.getComparisonType();
		if ( comparisonType!=null && !"".equals(comparisonType) )
			getWarnings().elementIgnored("comparisonType");

		// pair[0..*]
		for (org.xmcda.v2.AlternativesComparisons.Pairs.Pair pair: pairs.getPair())
		{
			/* one of them */
			Object initial = null;
			
			String initial_id = pair.getInitial().getAlternativeID();
			String initial_set_id = pair.getInitial().getAlternativesSetID();
			org.xmcda.v2.AlternativesSet initial_set = pair.getInitial().getAlternativesSet();
			
			if ( initial_id != null )
			{
				initial = xmcda_v3.alternatives.get(initial_id, true);
			}
			else if ( initial_set_id != null )
			{
				throw new IllegalArgumentException("alternativesSetID unexpected");
				//initial = xmcda_v3.alternativesSets.get(initial_set_id, true);
			}
			else if ( initial_set != null )
			{
				throw new IllegalArgumentException("alternativesSet unexpected");
				//initial = new AlternativesSetConverter().convertTo_v3(initial_set, xmcda_v3);
			}
			else
				throw new IllegalArgumentException("Invalid pair, no alternativesID in initial");
			/* */
			Object terminal = null;

			String terminal_id = pair.getTerminal().getAlternativeID();
			String terminal_set_id = pair.getTerminal().getAlternativesSetID();
			org.xmcda.v2.AlternativesSet terminal_set = pair.getTerminal().getAlternativesSet();
			
			if ( terminal_id != null )
			{
				terminal = xmcda_v3.alternatives.get(terminal_id, true);
			}
			else if ( terminal_set_id != null )
			{
				throw new IllegalArgumentException("alternativesSetID unexpected");
				//terminal = xmcda_v3.alternativesSets.get(terminal_set_id, true);
			}
			else if ( terminal_set != null )
			{
				throw new IllegalArgumentException("alternativesSet unexpected");
				//terminal = new AlternativesSetConverter().convertTo_v3(terminal_set, xmcda_v3);
			}
			else
				throw new IllegalArgumentException("Invalid pair, no alternativesID in terminal");

			// alternativesMatrix en v3 ne contient que des alternativeID row/column
			// alternativesSetsMatrix en v3 ne contient que des alternativeID row/column
			List<Object> pairValue_v2 = new java.util.ArrayList<Object>();

			for (Object _v: pair.getValue())
				pairValue_v2.add(_v);
			for (Object _v: pair.getValues())
				pairValue_v2.add(_v);

			org.xmcda.QualifiedValues values_v3 = Factory.qualifiedValues();
			if ( pairValue_v2==null || pairValue_v2.size()==0 )
			{
				// no value: assign N/A
				QualifiedValue tmpValue = Factory.qualifiedValue();
				tmpValue.setValue(org.xmcda.value.NA.na);
				values_v3.add(tmpValue);
			}
			else
			{
				// iterate on valueOrValues, convert them and and add them to values_v3
				for (Object valueOrValues: pairValue_v2)
				{
					if (valueOrValues instanceof org.xmcda.v2.Value)
					{
						org.xmcda.v2.Value tmpValue_v2 = (org.xmcda.v2.Value) valueOrValues;
						values_v3.add(new QualifiedValueConverter().convertTo_v3(tmpValue_v2, xmcda_v3));
					}
					else
					{
						org.xmcda.v2.Values tmpValues_v2 = (org.xmcda.v2.Values) valueOrValues;
						// TODO ignore description mcdaConcept etc.
						for (org.xmcda.v2.Value tmpValue_v2 : tmpValues_v2.getValue())
						{
							values_v3.add(new QualifiedValueConverter().convertTo_v3(tmpValue_v2, xmcda_v3));
						}
					}
				}
			}
			alternativesMatrix_v3.put(new org.xmcda.utils.Coord(initial, terminal), values_v3);
		}
		getWarnings().popTag(); // PAIRS

		getWarnings().popTag(); // ALTERNATIVES_COMPARISONS
		xmcda_v3.alternativesMatricesList.add(alternativesMatrix_v3);
	}

	// v3 -> v2

	public void convertAlternativesMatricesTo_v2(List<org.xmcda.AlternativesMatrix<?>> alternativesMatrices_v3, org.xmcda.v2.XMCDA xmcda_v2)
	{
		for (org.xmcda.AlternativesMatrix alternativesMatrix_v3: alternativesMatrices_v3)
		{
			convertAlternativesMatrixTo_v2(alternativesMatrix_v3, xmcda_v2);
		}
	}

	/**
	 * Converts a alternatives matrix (v3) into a alternatives comparison (v2)
	 * @param alternativesMatrix_v3
	 * @param xmcda_v2
	 */
	public void convertAlternativesMatrixTo_v2(org.xmcda.AlternativesMatrix alternativesMatrix_v3, org.xmcda.v2.XMCDA xmcda_v2)
	{
		getWarnings().pushTag(org.xmcda.AlternativesMatrix.TAG);
		org.xmcda.v2.AlternativesComparisons alternativesComparisons_v2 = new org.xmcda.v2.AlternativesComparisons();
		
		List<JAXBElement<?>> ccs = xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters();
		ccs.add(new JAXBElement<org.xmcda.v2.AlternativesComparisons>(new QName(ALTERNATIVES_COMPARISONS),
		                                                              org.xmcda.v2.AlternativesComparisons.class, alternativesComparisons_v2));

		// default attributes
		alternativesComparisons_v2.setId(alternativesMatrix_v3.id());
		alternativesComparisons_v2.setName(alternativesMatrix_v3.name());
		alternativesComparisons_v2.setMcdaConcept(alternativesMatrix_v3.mcdaConcept());

		// description
		alternativesComparisons_v2.setDescription(new DescriptionConverter().convertTo_v2(alternativesMatrix_v3.getDescription()));

		// valuation
		alternativesComparisons_v2.setValuation(new ScaleConverter().convertTo_v2(alternativesMatrix_v3.getValuation()));
		
		// rows
		org.xmcda.v2.AlternativesComparisons.Pairs pairs_v2 = new org.xmcda.v2.AlternativesComparisons.Pairs();
		alternativesComparisons_v2.setPairs(pairs_v2);
		
		for (Object _coord: alternativesMatrix_v3.keySet())
		{
			org.xmcda.utils.Coord coord = (org.xmcda.utils.Coord) _coord;
			org.xmcda.v2.AlternativesComparisons.Pairs.Pair pair_v2 = new org.xmcda.v2.AlternativesComparisons.Pairs.Pair();
			org.xmcda.v2.AlternativeReference alternativeRef_initial = new org.xmcda.v2.AlternativeReference();
			alternativeRef_initial.setAlternativeID( ((org.xmcda.Alternative) coord.x).id()) ;
			
			org.xmcda.v2.AlternativeReference alternativeRef_terminal = new org.xmcda.v2.AlternativeReference();
			alternativeRef_terminal.setAlternativeID( ((org.xmcda.Alternative) coord.y).id()) ;

			pair_v2.setInitial(alternativeRef_initial);
			pair_v2.setTerminal(alternativeRef_terminal);

			// values
			final org.xmcda.QualifiedValues<?> values_v3 = (org.xmcda.QualifiedValues<?>) alternativesMatrix_v3.get(_coord);			
			final Object _v = ValuesConverter.convertTo_v2(values_v3); // may be null

			if (_v instanceof org.xmcda.v2.Value)
				pair_v2.getValue().add((org.xmcda.v2.Value) _v);
			else if (_v instanceof org.xmcda.v2.Values)
				pair_v2.getValues().add((org.xmcda.v2.Values) _v);

			pairs_v2.getPair().add(pair_v2);
		}
		getWarnings().popTag(); // org.xmcda.AlternativesMatrix.TAG
	}
	
	public void convertAlternativesSetsMatricesTo_v2(List<org.xmcda.AlternativesSetsMatrix<?,?>> alternativesSetsMatrices_v3, org.xmcda.v2.XMCDA xmcda_v2)
	{
		for (org.xmcda.AlternativesSetsMatrix alternativesSetsMatrix_v3: alternativesSetsMatrices_v3)
		{
			convertAlternativesSetsMatrixTo_v2(alternativesSetsMatrix_v3, xmcda_v2);
		}
	}

	/**
	 * Converts a alternativesSets matrix (v3) into a alternatives comparison (v2)
	 * @param alternativesMatrix_v3
	 * @param xmcda_v2
	 */
	public void convertAlternativesSetsMatrixTo_v2(org.xmcda.AlternativesSetsMatrix alternativesSetsMatrix_v3, org.xmcda.v2.XMCDA xmcda_v2)
	{
		getWarnings().pushTag(org.xmcda.AlternativesMatrix.TAG);
		org.xmcda.v2.AlternativesComparisons alternativesComparisons_v2 = new org.xmcda.v2.AlternativesComparisons();
		
		List<JAXBElement<?>> ccs = xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters();
		ccs.add(new JAXBElement<org.xmcda.v2.AlternativesComparisons>(new QName(ALTERNATIVES_COMPARISONS),
		                                                              org.xmcda.v2.AlternativesComparisons.class, alternativesComparisons_v2));

		// default attributes
		alternativesComparisons_v2.setId(alternativesSetsMatrix_v3.id());
		alternativesComparisons_v2.setName(alternativesSetsMatrix_v3.name());
		alternativesComparisons_v2.setMcdaConcept(alternativesSetsMatrix_v3.mcdaConcept());

		// description
		alternativesComparisons_v2.setDescription(new DescriptionConverter().convertTo_v2(alternativesSetsMatrix_v3.getDescription()));

		// valuation
		alternativesComparisons_v2.setValuation(new ScaleConverter().convertTo_v2(alternativesSetsMatrix_v3.getValuation()));
		
		// rows
		org.xmcda.v2.AlternativesComparisons.Pairs pairs_v2 = new org.xmcda.v2.AlternativesComparisons.Pairs();
		alternativesComparisons_v2.setPairs(pairs_v2);
		
		for (Object _coord: alternativesSetsMatrix_v3.keySet())
		{
			org.xmcda.utils.Coord coord = (org.xmcda.utils.Coord) _coord;
			org.xmcda.v2.AlternativesComparisons.Pairs.Pair pair_v2 = new org.xmcda.v2.AlternativesComparisons.Pairs.Pair();
			org.xmcda.v2.AlternativeReference alternativeRef_initial = new org.xmcda.v2.AlternativeReference();
			alternativeRef_initial.setAlternativesSetID( ((org.xmcda.AlternativesSet) coord.x).id() ) ;
			
			org.xmcda.v2.AlternativeReference alternativeRef_terminal = new org.xmcda.v2.AlternativeReference();
			alternativeRef_terminal.setAlternativesSetID( ((org.xmcda.AlternativesSet) coord.y).id() ) ;

			pair_v2.setInitial(alternativeRef_initial);
			pair_v2.setTerminal(alternativeRef_terminal);

			// values
			final org.xmcda.QualifiedValues<?> values_v3 = (org.xmcda.QualifiedValues<?>) alternativesSetsMatrix_v3.get(_coord);			
			final Object _v = ValuesConverter.convertTo_v2(values_v3); // may be null

			if (_v instanceof org.xmcda.v2.Value)
				pair_v2.getValue().add((org.xmcda.v2.Value) _v);
			else if (_v instanceof org.xmcda.v2.Values)
				pair_v2.getValues().add((org.xmcda.v2.Values) _v);

			pairs_v2.getPair().add(pair_v2);
		}
		getWarnings().popTag(); // org.xmcda.AlternativesMatrix.TAG
	}
}


