/**
 * 
 */
package org.xmcda.converters.v2_v3;

import org.xmcda.Factory;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import java.util.List;

/**
 * @author Sébastien Bigaret <sebastien.bigaret@telecom-bretagne.eu>
 *
 */
public class MethodParametersConverter
extends Converter
{
	public static final String METHOD_PARAMETERS = "methodParameters";
	public static final String PARAMETER = "parameter";
	public static final String PROGRAM_PARAMETERS = org.xmcda.ProgramParameters.TAG;

	public MethodParametersConverter()
	{
		super(METHOD_PARAMETERS);
	}

	// v2 -> v3
	
	public void convertTo_v3(org.xmcda.v2.MethodParameters params_v2, org.xmcda.XMCDA xmcda_v3)
	{
		getWarnings().pushTag(METHOD_PARAMETERS, params_v2.getId());
		
		org.xmcda.ProgramParameters<?> params_v3 = Factory.programParameters();

		// Default attributes
		params_v3.setId(params_v2.getId());
		params_v3.setName(params_v2.getName());
		params_v3.setMcdaConcept(params_v2.getMcdaConcept());
		// description: see below
		
		for (JAXBElement _obj: params_v2.getDescriptionOrApproachOrProblematique())
		{
			if (_obj.getValue() instanceof org.xmcda.v2.Description)
			{
				params_v3.setDescription(new DescriptionConverter().convertTo_v3( (org.xmcda.v2.Description) _obj.getValue() ));
			}
			else if (_obj.getValue() instanceof org.xmcda.v2.Parameters)
			{
				for (org.xmcda.v2.Parameter param_v2: ((org.xmcda.v2.Parameters)_obj.getValue()).getParameter())
				{
					params_v3.add(convertParameterTo_v3(param_v2, xmcda_v3));
				}
			}
			else if (_obj.getValue() instanceof org.xmcda.v2.Parameter)
			{
				params_v3.add(convertParameterTo_v3((org.xmcda.v2.Parameter)_obj.getValue(), xmcda_v3));
			}
			// ignored: approach, problematique, methodology
		}

		getWarnings().popTag(); // METHOD_PARAMETERS
		xmcda_v3.programParametersList.add(params_v3);
	}

	public org.xmcda.ProgramParameter convertParameterTo_v3(org.xmcda.v2.Parameter param_v2, org.xmcda.XMCDA xmcda_v3)
	{
		getWarnings().pushTag(PARAMETER, param_v2.getId());
		org.xmcda.ProgramParameter programParameter = Factory.programParameter();
		
		// Default attributes
		programParameter.setId(param_v2.getId());
		programParameter.setName(param_v2.getName());
		programParameter.setMcdaConcept(param_v2.getMcdaConcept());
		programParameter.setDescription(new DescriptionConverter().convertTo_v3(param_v2.getDescription()));

		// function: ignored
		if (param_v2.getFunction()!=null && ! param_v2.getFunction().isEmpty())
			getWarnings().elementIgnored("function");

		// values
		programParameter.setValues(Factory.qualifiedValues());

		for (org.xmcda.v2.Value value_v2: param_v2.getValue())
		{
			 programParameter.getValues().add(new QualifiedValueConverter().convertTo_v3(value_v2, xmcda_v3));
		}
			
		getWarnings().popTag(); // PARAMETER
		return programParameter;
	}

	// v3 -> v2
	public void convertProgramParametersTo_v2(List<org.xmcda.ProgramParameters<?>> paramsList_v3, org.xmcda.v2.XMCDA xmcda_v2)
	{
		for (org.xmcda.ProgramParameters<?> params_v3: paramsList_v3)
		{
			convertProgramParametersTo_v2(params_v3, xmcda_v2);
		}
	}

	public void convertProgramParametersTo_v2(org.xmcda.ProgramParameters<?> params_v3, org.xmcda.v2.XMCDA xmcda_v2)
	{
		getWarnings().pushTag(PROGRAM_PARAMETERS);
		org.xmcda.v2.MethodParameters params_v2 = new org.xmcda.v2.MethodParameters();

		List<JAXBElement<?>> ccs = xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters();
		ccs.add(new JAXBElement<org.xmcda.v2.MethodParameters>(new QName(METHOD_PARAMETERS),
                org.xmcda.v2.MethodParameters.class, params_v2));

		// default attributes
		params_v2.setId(params_v3.id());
		params_v2.setName(params_v3.name());
		params_v2.setMcdaConcept(params_v3.mcdaConcept());
		if ( params_v3.getDescription() != null )
		{
			params_v2.getDescriptionOrApproachOrProblematique().add(new JAXBElement<org.xmcda.v2.Description>(new QName("description"),
					org.xmcda.v2.Description.class, new DescriptionConverter().convertTo_v2(params_v3.getDescription())));
		}
		// parameters
		for (org.xmcda.ProgramParameter<?> param_v3: params_v3)
		{
			org.xmcda.v2.Parameter param_v2 = new org.xmcda.v2.Parameter();
			params_v2.getDescriptionOrApproachOrProblematique().add(new JAXBElement<org.xmcda.v2.Parameter>(new QName("parameter"),
					org.xmcda.v2.Parameter.class, param_v2));
			param_v2.setId(param_v3.id());
			param_v2.setName(param_v3.name());
			param_v2.setMcdaConcept(param_v3.mcdaConcept());
			
			for (org.xmcda.QualifiedValue value: param_v3.getValues())
			{
				param_v2.getValue().add(new QualifiedValueConverter().convertTo_v2(value));
			}
		}
		getWarnings().popTag(); // PROGRAM_PARAMETERS
	}
}
