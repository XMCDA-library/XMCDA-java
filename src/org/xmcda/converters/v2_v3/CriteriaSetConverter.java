package org.xmcda.converters.v2_v3;

import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;


public class CriteriaSetConverter
    extends Converter
{
	public static final String CRITERIA_SET = "criteriaSet";

	public CriteriaSetConverter()
	{
		super(CRITERIA_SET);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
    public org.xmcda.CriteriaSet convertTo_v3(org.xmcda.v2.CriteriaSet criteriaSet_v2, org.xmcda.XMCDA xmcda_v3)
	{
		// TODO ignorés pour le moment: value et values
		String criteriaSetID = criteriaSet_v2.getId();
		getWarnings().pushTag(CRITERIA_SET, criteriaSetID);

		if (criteriaSetID == null || "".equals(criteriaSetID))
		{
			// we are called by with a criteriaSet with no ID, which may happen when the criteriaSet is embedded in another structure,
			// such as in <criteriaValues>
			// we need to find one which is not already assigned: criteriaSet must have an ID in v3
			final String baseID="criteriaSet_generatedID_";
			int idx=1;
			criteriaSetID = baseID+idx;
			while (xmcda_v3.criteriaSets.contains(criteriaSetID))
			{
				idx = idx+1;
				criteriaSetID = baseID+idx;
			}
		}
		if (!criteriaSet_v2.getValueOrValues().isEmpty())
			getWarnings().elementIgnored("value", "value ou values ignored on criteriaSet: they are NOT translated into v3 criteriaSetsValues by this converter");
		org.xmcda.CriteriaSet criteriaSet_v3 = org.xmcda.Factory.criteriaSet();
		criteriaSet_v3.setId(criteriaSetID);
		criteriaSet_v3.setName(criteriaSet_v2.getName());
		criteriaSet_v3.setMcdaConcept(criteriaSet_v2.getMcdaConcept());
		
		criteriaSet_v3.setDescription(new DescriptionConverter().convertTo_v3(criteriaSet_v2.getDescription()));
		
		for (org.xmcda.v2.CriteriaSet.Element element: criteriaSet_v2.getElement())
		{
			getWarnings().pushTag("element");
			// ignoring: description, rank, value, values
			if (element.getDescription()!=null)
				getWarnings().elementIgnored("description");
			criteriaSet_v3.put(xmcda_v3.criteria.get(element.getCriterionID()),
			                   values_convertTo_v3(element.getRankOrValueOrValues(), xmcda_v3));
			getWarnings().popTag(); // "element"
		}
		getWarnings().popTag(); // CRITERIA_SET
		xmcda_v3.criteriaSets.add(criteriaSet_v3);
		return criteriaSet_v3;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
    protected org.xmcda.QualifiedValues values_convertTo_v3(List<JAXBElement<?>> values, org.xmcda.XMCDA xmcda_v3)
	{
		org.xmcda.QualifiedValues values_v3 = org.xmcda.Factory.qualifiedValues();


		for (JAXBElement element: values)
		{
			getWarnings().pushTag(element.getName().getLocalPart());
			if ("rank".equals(element.getName().getLocalPart()))
				values_v3.add(new QualifiedValueConverter().convertTo_v3((org.xmcda.v2.Value)element.getValue(), xmcda_v3));
			if ("value".equals(element.getName().getLocalPart()))
				values_v3.add(new QualifiedValueConverter().convertTo_v3((org.xmcda.v2.Value)element.getValue(), xmcda_v3));
			if ("values".equals(element.getName().getLocalPart()))
			{
				final org.xmcda.v2.Values values_v2 = (org.xmcda.v2.Values) element.getValue();
				getWarnings().setTagID(values_v2.getId());
				if (values_v2.getId()!=null)
					getWarnings().attributeIgnored("id");
				if (values_v2.getName()!=null)
					getWarnings().attributeIgnored("name");
				if (values_v2.getMcdaConcept()!=null)
					getWarnings().attributeIgnored("mcdaConcept");
				if (values_v2.getDescription()!=null)
					getWarnings().elementIgnored("description");

				for (org.xmcda.v2.Value value: values_v2.getValue())
					values_v3.add(new QualifiedValueConverter().convertTo_v3(value, xmcda_v3));
			}
			getWarnings().popTag();
		}
		return values_v3;
	}
	
	public <T> org.xmcda.v2.CriteriaSet convertTo_v2(org.xmcda.CriteriaSet<T> criteriaSet_v3, org.xmcda.v2.XMCDA xmcda_v2)
	{
		org.xmcda.v2.CriteriaSet criteriaSet_v2 = new org.xmcda.v2.CriteriaSet();

		getWarnings().pushTag(CRITERIA_SET, criteriaSet_v3.id());

		// default attributes
		criteriaSet_v2.setId(criteriaSet_v3.id());
		criteriaSet_v2.setName(criteriaSet_v3.name());
		criteriaSet_v2.setMcdaConcept(criteriaSet_v3.mcdaConcept());

		// description
		final org.xmcda.Description desc_v3 = criteriaSet_v3.getDescription();
		criteriaSet_v2.setDescription(new DescriptionConverter().convertTo_v2(desc_v3));

		// elements
		for (org.xmcda.Criterion criterion_v3: criteriaSet_v3.keySet())
		{
			getWarnings().pushTag("criterion", criterion_v3.id());
/*
			org.xmcda.v2.Criterion criterion_v2 = new org.xmcda.v2.Criterion();
			criterion_v2.setId(criterion_v3.id());
			criterion_v2.setName(criterion_v3.name());
			criterion_v2.setMcdaConcept(criterion_v3.mcdaConcept());
			criterion_v2.setDescription(new DescriptionConverter().convertTo_v2(criterion_v3.getDescription()));
*/			
			org.xmcda.v2.CriteriaSet.Element element = new org.xmcda.v2.CriteriaSet.Element();
			element.setCriterionID(criterion_v3.id());

			getWarnings().pushTag("values");
			final Object _v = ValuesConverter.convertTo_v2(criteriaSet_v3.get(criterion_v3)); // may be null
			if (_v instanceof org.xmcda.v2.Value)
				element.getRankOrValueOrValues()
				        .add(new JAXBElement<>(new QName("value"), org.xmcda.v2.Value.class, (org.xmcda.v2.Value)_v));
			else if (_v instanceof org.xmcda.v2.Values)
			    element.getRankOrValueOrValues()
			            .add(new JAXBElement<>(new QName("values"), org.xmcda.v2.Values.class, (org.xmcda.v2.Values)_v));
			getWarnings().popTag(); // "values"

			criteriaSet_v2.getElement().add(element);

			getWarnings().popTag(); // "criterion"
		}
		getWarnings().popTag(); // CRITERIA_SET
		return criteriaSet_v2;
	}
}
