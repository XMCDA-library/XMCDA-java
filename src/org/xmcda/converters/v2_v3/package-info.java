/**
 * This package contains all the necessary classes to convert XMCDA v2.2.1 to XMCDA v3 and vice-versa.
 */
package org.xmcda.converters.v2_v3;
