/**
 * 
 */
package org.xmcda.converters.v2_v3;

import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import org.xmcda.Factory;
import org.xmcda.QualifiedValue;

/**
 * @author Sébastien Bigaret <sebastien.bigaret@telecom-bretagne.eu>
 *
 */
public class CriteriaComparisonsConverter
extends Converter
{
	public static final String CRITERIA_COMPARISONS = "criteriaComparisons";
	public static final String PAIRS                    = "pairs";
	public static final String PAIR                     = "pair";

	public CriteriaComparisonsConverter()
	{
		super(CRITERIA_COMPARISONS); // ajouter un constructeur super(tag_v2, tag_v3) ?
	}
	
	// v2 -> v3
	
	public void convertTo_v3(org.xmcda.v2.CriteriaComparisons value, org.xmcda.XMCDA xmcda_v3)
	{
		// determine whether we convert criteria or criteriaSets/criteriaSetIDs
		final org.xmcda.v2.CriteriaComparisons.Pairs pairs = value.getPairs();
		if ( pairs == null || pairs.getPair().size()==0 )
		{
			convertCriteriaComparisonsTo_v3(value, xmcda_v3);
			return;
		}
		org.xmcda.v2.CriteriaComparisons.Pairs.Pair pair = pairs.getPair().get(0);
		String initial_id = pair.getInitial().getCriterionID();
		if ( initial_id != null )
		{
			// either only criteriaSets ...
			convertCriteriaComparisonsTo_v3(value, xmcda_v3);
		}
		else
		{
			// ... or criteriaSets / criteriaSetIDs
			convertCriteriaSetsComparisonsTo_v3(value, xmcda_v3);
		}
	}

	// Implementation note:
	// convertCriteriaComparisonsTo_v3 & convertCriteriaSetsComparisonsTo_v3 are very similar
	// Changes in one probably means changing the other one 
	public void convertCriteriaSetsComparisonsTo_v3(org.xmcda.v2.CriteriaComparisons value, org.xmcda.XMCDA xmcda_v3)
	{
		getWarnings().pushTag(CRITERIA_COMPARISONS, value.getId());
		
		org.xmcda.CriteriaSetsMatrix<?,?> criteriaSetsMatrix_v3 = Factory.criteriaSetsMatrix();
		// defaultAttributes
		criteriaSetsMatrix_v3.setId(value.getId());
		criteriaSetsMatrix_v3.setMcdaConcept(value.getMcdaConcept());
		criteriaSetsMatrix_v3.setName(value.getName());
		criteriaSetsMatrix_v3.setDescription(new DescriptionConverter().convertTo_v3(value.getDescription()));

		// valuation: scale
		if ( value.getValuation() != null )
			criteriaSetsMatrix_v3.setValuation(new ScaleConverter().convertTo_v3(value.getValuation(), xmcda_v3));

		// pair 0..*
		if (value.getPairs().getDescription() != null)
			getWarnings().elementIgnored("description", Warnings.ABSENT_IN_V3_0);

		// 
		final org.xmcda.v2.CriteriaComparisons.Pairs pairs = value.getPairs();

		// TODO v2 accepts empty Matrix
		if ( pairs == null || pairs.getPair().size()==0 )
		{
			getWarnings().elementUnimplemented("empty v2"); // TODO c'est pas unimplemented qu'il faut
			return;
		}

		getWarnings().pushTag(PAIRS, value.getId());

		/* comparisonType is present in examples of weightsFromCondorcetAndPreferences only
		 * and it is not used.  It is not present in v3.  The idea is that, if something like that
		 * is necessary, it should be passed as a methodParameter.
		 */
		final String comparisonType = value.getComparisonType();
		if ( comparisonType!=null && !"".equals(comparisonType) )
			getWarnings().elementIgnored("comparisonType");

		// pair[0..*]
		for (org.xmcda.v2.CriteriaComparisons.Pairs.Pair pair: pairs.getPair())
		{
			/* one of them */
			org.xmcda.CriteriaSet<?> initial = null;
			
			String initial_id = pair.getInitial().getCriterionID();
			String initial_set_id = pair.getInitial().getCriteriaSetID();
			org.xmcda.v2.CriteriaSet initial_set = pair.getInitial().getCriteriaSet();
			
			if ( initial_id != null )
			{
				throw new IllegalArgumentException("criterionID unexpected");
			}
			else if ( initial_set_id != null )
			{
				initial = xmcda_v3.criteriaSets.get(initial_set_id, true);
			}
			else if ( initial_set != null )
			{
				// the converter already assigns a generated id if there was none
				initial = new CriteriaSetConverter().convertTo_v3(initial_set, xmcda_v3);
				xmcda_v3.criteriaSets.add((org.xmcda.CriteriaSet)initial);
			}
			else
				throw new IllegalArgumentException("Invalid pair, no criteriaSetID nor criteriaSet in initial");
			/* */
			org.xmcda.CriteriaSet<?> terminal = null;

			String terminal_id = pair.getTerminal().getCriterionID();
			String terminal_set_id = pair.getTerminal().getCriteriaSetID();
			org.xmcda.v2.CriteriaSet terminal_set = pair.getTerminal().getCriteriaSet();
			
			if ( terminal_id != null )
			{
				throw new IllegalArgumentException("criterionID unexpected");
			}
			else if ( terminal_set_id != null )
			{
				terminal = xmcda_v3.criteriaSets.get(terminal_set_id, true);
			}
			else if ( terminal_set != null )
			{
				// the converter already assigns a generated id if there was none
				terminal = new CriteriaSetConverter().convertTo_v3(terminal_set, xmcda_v3);
				xmcda_v3.criteriaSets.add((org.xmcda.CriteriaSet)terminal);
			}
			else
				throw new IllegalArgumentException("Invalid pair, no criteriaSetID nor criteriaSet in initial");

			// criteriaSetsMatrix en v3 ne contient que des criterionID row/column
			List<Object> pairValue_v2 = pair.getValueOrValues();
			org.xmcda.QualifiedValues values_v3 = Factory.qualifiedValues();
			if ( pairValue_v2==null || pairValue_v2.size()==0 )
			{
				// no value: assign N/A
				QualifiedValue tmpValue = Factory.qualifiedValue();
				tmpValue.setValue(org.xmcda.value.NA.na);
				values_v3.add(tmpValue);
			}
			else
			{
				// iterate on valueOrValues, convert them and and add them to values_v3
				for (Object valueOrValues: pairValue_v2)
				{
					if (valueOrValues instanceof org.xmcda.v2.Value)
					{
						org.xmcda.v2.Value tmpValue_v2 = (org.xmcda.v2.Value) valueOrValues;
						values_v3.add(new QualifiedValueConverter().convertTo_v3(tmpValue_v2, xmcda_v3));
					}
					else
					{
						org.xmcda.v2.Values tmpValues_v2 = (org.xmcda.v2.Values) valueOrValues;
						// TODO ignore description mcdaConcept etc.
						for (org.xmcda.v2.Value tmpValue_v2 : tmpValues_v2.getValue())
						{
							values_v3.add(new QualifiedValueConverter().convertTo_v3(tmpValue_v2, xmcda_v3));
						}
					}
				}
			}
			criteriaSetsMatrix_v3.put(new org.xmcda.utils.Coord(initial, terminal), values_v3);
		}
		getWarnings().popTag(); // PAIRS

		getWarnings().popTag(); // CRITERIA_COMPARISONS
		xmcda_v3.criteriaSetsMatricesList.add(criteriaSetsMatrix_v3);
	}

	// Implementation note:
	// convertCriteriaComparisonsTo_v3 & convertCriteriaSetsComparisonsTo_v3 are very similar
	// Changes in one probably means changing the other one 
	public void convertCriteriaComparisonsTo_v3(org.xmcda.v2.CriteriaComparisons value, org.xmcda.XMCDA xmcda_v3)
	{
		getWarnings().pushTag(CRITERIA_COMPARISONS, value.getId());

		org.xmcda.CriteriaMatrix<?> criteriaMatrix_v3 = Factory.criteriaMatrix();
		// defaultAttributes
		criteriaMatrix_v3.setId(value.getId());
		criteriaMatrix_v3.setMcdaConcept(value.getMcdaConcept());
		criteriaMatrix_v3.setName(value.getName());
		criteriaMatrix_v3.setDescription(new DescriptionConverter().convertTo_v3(value.getDescription()));

		// valuation: scale
		if ( value.getValuation() != null )
			criteriaMatrix_v3.setValuation(new ScaleConverter().convertTo_v3(value.getValuation(), xmcda_v3));
		
		// pair 0..*
		if (value.getPairs().getDescription() != null)
			getWarnings().elementIgnored("description", Warnings.ABSENT_IN_V3_0);

		// 
		final org.xmcda.v2.CriteriaComparisons.Pairs pairs = value.getPairs();

		// TODO v2 accepts empty Matrix
		if ( pairs == null || pairs.getPair().size()==0 )
		{
			getWarnings().elementUnimplemented("empty v2"); // TODO c'est pas unimplemented qu'il faut
			return;
		}

		getWarnings().pushTag(PAIRS, value.getId());

		/* comparisonType is present in examples of weightsFromCondorcetAndPreferences only
		 * and it is not used.  It is not present in v3.  The idea is that, if something like that
		 * is necessary, it should be passed as a programParameter.
		 */
		final String comparisonType = value.getComparisonType();
		if ( comparisonType!=null && !"".equals(comparisonType) )
			getWarnings().elementIgnored("comparisonType");

		// pair[0..*]
		for (org.xmcda.v2.CriteriaComparisons.Pairs.Pair pair: pairs.getPair())
		{
			/* one of them */
			Object initial = null;
			
			String initial_id = pair.getInitial().getCriterionID();
			String initial_set_id = pair.getInitial().getCriteriaSetID();
			org.xmcda.v2.CriteriaSet initial_set = pair.getInitial().getCriteriaSet();
			
			if ( initial_id != null )
			{
				initial = xmcda_v3.criteria.get(initial_id, true);
			}
			else if ( initial_set_id != null )
			{
				throw new IllegalArgumentException("criteriaSetID unexpected");
				//initial = xmcda_v3.criteriaSets.get(initial_set_id, true);
			}
			else if ( initial_set != null )
			{
				throw new IllegalArgumentException("criteriaSet unexpected");
				//initial = new CriteriaSetConverter().convertTo_v3(initial_set, xmcda_v3);
			}
			else
				throw new IllegalArgumentException("Invalid pair, no criteriaID in initial");
			/* */
			Object terminal = null;

			String terminal_id = pair.getTerminal().getCriterionID();
			String terminal_set_id = pair.getTerminal().getCriteriaSetID();
			org.xmcda.v2.CriteriaSet terminal_set = pair.getTerminal().getCriteriaSet();
			
			if ( terminal_id != null )
			{
				terminal = xmcda_v3.criteria.get(terminal_id, true);
			}
			else if ( terminal_set_id != null )
			{
				throw new IllegalArgumentException("criteriaSetID unexpected");
				//terminal = xmcda_v3.criteriaSets.get(terminal_set_id, true);
			}
			else if ( terminal_set != null )
			{
				throw new IllegalArgumentException("criteriaSet unexpected");
				//terminal = new CriteriaSetConverter().convertTo_v3(terminal_set, xmcda_v3);
			}
			else
				throw new IllegalArgumentException("Invalid pair, no criteriaID in terminal");

			// criteriaMatrix en v3 ne contient que des criterionID row/column
			List<Object> pairValue_v2 = pair.getValueOrValues();
			org.xmcda.QualifiedValues values_v3 = Factory.qualifiedValues();
			if ( pairValue_v2==null || pairValue_v2.size()==0 )
			{
				// no value: assign N/A
				QualifiedValue tmpValue = Factory.qualifiedValue();
				tmpValue.setValue(org.xmcda.value.NA.na);
				values_v3.add(tmpValue);
			}
			else
			{
				// iterate on valueOrValues, convert them and and add them to values_v3
				for (Object valueOrValues: pairValue_v2)
				{
					if (valueOrValues instanceof org.xmcda.v2.Value)
					{
						org.xmcda.v2.Value tmpValue_v2 = (org.xmcda.v2.Value) valueOrValues;
						values_v3.add(new QualifiedValueConverter().convertTo_v3(tmpValue_v2, xmcda_v3));
					}
					else
					{
						org.xmcda.v2.Values tmpValues_v2 = (org.xmcda.v2.Values) valueOrValues;
						// TODO ignore description mcdaConcept etc.
						for (org.xmcda.v2.Value tmpValue_v2 : tmpValues_v2.getValue())
						{
							values_v3.add(new QualifiedValueConverter().convertTo_v3(tmpValue_v2, xmcda_v3));
						}
					}
				}
			}
			criteriaMatrix_v3.put(new org.xmcda.utils.Coord(initial, terminal), values_v3);
		}
		getWarnings().popTag(); // PAIRS

		getWarnings().popTag(); // CRITERIA_COMPARISONS
		xmcda_v3.criteriaMatricesList.add(criteriaMatrix_v3);
	}

	// v3 -> v2

	public void convertCriteriaMatricesTo_v2(List<org.xmcda.CriteriaMatrix<?>> criteriaMatrices_v3, org.xmcda.v2.XMCDA xmcda_v2)
	{
		for (org.xmcda.CriteriaMatrix criteriaMatrix_v3: criteriaMatrices_v3)
		{
			convertCriteriaMatrixTo_v2(criteriaMatrix_v3, xmcda_v2);
		}
	}

	/**
	 * Converts a criteria matrix (v3) into a criteria comparison (v2)
	 * @param criteriaMatrix_v3
	 * @param xmcda_v2
	 */
	public void convertCriteriaMatrixTo_v2(org.xmcda.CriteriaMatrix criteriaMatrix_v3, org.xmcda.v2.XMCDA xmcda_v2)
	{
		getWarnings().pushTag(org.xmcda.CriteriaMatrix.TAG);
		org.xmcda.v2.CriteriaComparisons criteriaComparisons_v2 = new org.xmcda.v2.CriteriaComparisons();
		
		List<JAXBElement<?>> ccs = xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters();
		ccs.add(new JAXBElement<org.xmcda.v2.CriteriaComparisons>(new QName(CRITERIA_COMPARISONS),
		                                                              org.xmcda.v2.CriteriaComparisons.class, criteriaComparisons_v2));

		// default attributes
		criteriaComparisons_v2.setId(criteriaMatrix_v3.id());
		criteriaComparisons_v2.setName(criteriaMatrix_v3.name());
		criteriaComparisons_v2.setMcdaConcept(criteriaMatrix_v3.mcdaConcept());

		// description
		criteriaComparisons_v2.setDescription(new DescriptionConverter().convertTo_v2(criteriaMatrix_v3.getDescription()));

		// valuation
		criteriaComparisons_v2.setValuation(new ScaleConverter().convertTo_v2(criteriaMatrix_v3.getValuation()));
		
		// rows
		org.xmcda.v2.CriteriaComparisons.Pairs pairs_v2 = new org.xmcda.v2.CriteriaComparisons.Pairs();
		criteriaComparisons_v2.setPairs(pairs_v2);
		
		for (Object _coord: criteriaMatrix_v3.keySet())
		{
			org.xmcda.utils.Coord coord = (org.xmcda.utils.Coord) _coord;
			org.xmcda.v2.CriteriaComparisons.Pairs.Pair pair_v2 = new org.xmcda.v2.CriteriaComparisons.Pairs.Pair();
			org.xmcda.v2.CriterionReference criterionRef_initial = new org.xmcda.v2.CriterionReference();
			criterionRef_initial.setCriterionID( ((org.xmcda.Criterion) coord.x).id()) ;
			
			org.xmcda.v2.CriterionReference criterionRef_terminal = new org.xmcda.v2.CriterionReference();
			criterionRef_terminal.setCriterionID( ((org.xmcda.Criterion) coord.y).id()) ;

			pair_v2.setInitial(criterionRef_initial);
			pair_v2.setTerminal(criterionRef_terminal);

			// values
			final org.xmcda.QualifiedValues<?> values_v3 = (org.xmcda.QualifiedValues<?>) criteriaMatrix_v3.get(_coord);			
			final Object _v = ValuesConverter.convertTo_v2(values_v3); // may be null

			if (_v instanceof org.xmcda.v2.Value)
				pair_v2.getValueOrValues().add((org.xmcda.v2.Value) _v);
			else if (_v instanceof org.xmcda.v2.Values)
				pair_v2.getValueOrValues().add((org.xmcda.v2.Values) _v);

			pairs_v2.getPair().add(pair_v2);
		}
		getWarnings().popTag(); // org.xmcda.CriteriaMatrix.TAG
	}
	
	public void convertCriteriaSetsMatricesTo_v2(List<org.xmcda.CriteriaSetsMatrix<?,?>> criteriaSetsMatrices_v3, org.xmcda.v2.XMCDA xmcda_v2)
	{
		for (org.xmcda.CriteriaSetsMatrix criteriaSetsMatrix_v3: criteriaSetsMatrices_v3)
		{
			convertCriteriaSetsMatrixTo_v2(criteriaSetsMatrix_v3, xmcda_v2);
		}
	}

	/**
	 * Converts a criteriaSets matrix (v3) into a criteria comparison (v2)
	 * @param criteriaMatrix_v3
	 * @param xmcda_v2
	 */
	public void convertCriteriaSetsMatrixTo_v2(org.xmcda.CriteriaSetsMatrix criteriaSetsMatrix_v3, org.xmcda.v2.XMCDA xmcda_v2)
	{
		getWarnings().pushTag(org.xmcda.CriteriaMatrix.TAG);
		org.xmcda.v2.CriteriaComparisons criteriaComparisons_v2 = new org.xmcda.v2.CriteriaComparisons();
		
		List<JAXBElement<?>> ccs = xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters();
		ccs.add(new JAXBElement<org.xmcda.v2.CriteriaComparisons>(new QName(CRITERIA_COMPARISONS),
		                                                              org.xmcda.v2.CriteriaComparisons.class, criteriaComparisons_v2));

		// default attributes
		criteriaComparisons_v2.setId(criteriaSetsMatrix_v3.id());
		criteriaComparisons_v2.setName(criteriaSetsMatrix_v3.name());
		criteriaComparisons_v2.setMcdaConcept(criteriaSetsMatrix_v3.mcdaConcept());

		// description
		criteriaComparisons_v2.setDescription(new DescriptionConverter().convertTo_v2(criteriaSetsMatrix_v3.getDescription()));

		// valuation
		criteriaComparisons_v2.setValuation(new ScaleConverter().convertTo_v2(criteriaSetsMatrix_v3.getValuation()));
		
		// rows
		org.xmcda.v2.CriteriaComparisons.Pairs pairs_v2 = new org.xmcda.v2.CriteriaComparisons.Pairs();
		criteriaComparisons_v2.setPairs(pairs_v2);
		
		for (Object _coord: criteriaSetsMatrix_v3.keySet())
		{
			org.xmcda.utils.Coord coord = (org.xmcda.utils.Coord) _coord;
			org.xmcda.v2.CriteriaComparisons.Pairs.Pair pair_v2 = new org.xmcda.v2.CriteriaComparisons.Pairs.Pair();
			org.xmcda.v2.CriterionReference criterionRef_initial = new org.xmcda.v2.CriterionReference();
			criterionRef_initial.setCriteriaSetID( ((org.xmcda.CriteriaSet) coord.x).id() ) ;
			
			org.xmcda.v2.CriterionReference criterionRef_terminal = new org.xmcda.v2.CriterionReference();
			criterionRef_terminal.setCriteriaSetID( ((org.xmcda.CriteriaSet) coord.y).id() ) ;

			pair_v2.setInitial(criterionRef_initial);
			pair_v2.setTerminal(criterionRef_terminal);

			// values
			final org.xmcda.QualifiedValues<?> values_v3 = (org.xmcda.QualifiedValues<?>) criteriaSetsMatrix_v3.get(_coord);			
			final Object _v = ValuesConverter.convertTo_v2(values_v3); // may be null

			if (_v instanceof org.xmcda.v2.Value)
				pair_v2.getValueOrValues().add((org.xmcda.v2.Value) _v);
			else if (_v instanceof org.xmcda.v2.Values)
				pair_v2.getValueOrValues().add((org.xmcda.v2.Values) _v);

			pairs_v2.getPair().add(pair_v2);
		}
		getWarnings().popTag(); // org.xmcda.CriteriaMatrix.TAG
	}
}


