package org.xmcda.converters.v2_v3;

import static org.xmcda.converters.v2_v3.HierarchyConverter.v3Tag;
import static org.xmcda.converters.v2_v3.HierarchyConverter.XMCDAv3HierarchyTag.*;

import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import org.xmcda.converters.v2_v3.HierarchyConverter.XMCDAv3HierarchyTag;

public class CriteriaHierarchyConverter
    extends Converter
{
	public static final String CRITERIA_HIERARCHY = org.xmcda.CriteriaHierarchy.TAG;
	public static final String HIERARCHY = org.xmcda.converters.v2_v3.HierarchyConverter.HIERARCHY;

	public CriteriaHierarchyConverter()
	{
		super(CRITERIA_HIERARCHY);
	}

	// v2 -> v3
	/*
	Example of an XMCDA file: (taken from RORUTA-ExtremeRanksHierarchical-PUT)
	<hierarchy>
    <node id="nodes">
            <node id="nodes1">
                    <node id="nodes11">
                            <criteriaSet>
                                    <element><criterionID>c01</criterionID></element>       
                            </criteriaSet>
                    </node>
                    <node id="nodes12">
                            <criteriaSet>
                                    <element><criterionID>c02</criterionID></element>       
                            </criteriaSet>
                    </node>
            </node>
            <node id="nodes2">
                    <criteriaSet>
                            <element><criterionID>c03</criterionID></element>
                            <element><criterionID>c04</criterionID></element>
                    </criteriaSet>
            </node>
    </node>
    </hierarchy>
   */

	/**
	 * @param hierarchy_v2
	 * @param xmcda_v3
	 * @param tag
	 * @throw IllegalArgumentException if the v2 hierarchy contains attributes (there is no such thing in XMCDA v3),
	 *        or if it mixes alternatives, criteria and/or categories.
	 */
	public void convertTo_v3(org.xmcda.v2.Hierarchy hierarchy_v2, org.xmcda.XMCDA xmcda_v3)
	{
		convertTo_v3(hierarchy_v2, xmcda_v3, null);
	}

	/**
	 * @param hierarchy_v2
	 * @param xmcda_v3
	 * @param tag the v3 tag to use. If it is {@code null} it is calculated with
	 *            {@link HierarchyConverter#v3Tag(org.xmcda.v2.Hierarchy)}.
	 * @throw IllegalArgumentException if the v2 hierarchy contains attributes (there is no such thing in XMCDA v3),
	 *        or if it mixes alternatives, criteria and/or categories.
	 */
	void convertTo_v3(org.xmcda.v2.Hierarchy hierarchy_v2, org.xmcda.XMCDA xmcda_v3, XMCDAv3HierarchyTag v3tag)
	{
		getWarnings().pushTag(HIERARCHY, hierarchy_v2.getId());

		if (v3tag == null)
			v3tag = v3Tag(hierarchy_v2);

		if (v3tag == CRITERION_ID)
		{
			org.xmcda.CriteriaHierarchy criteriaHierarchy_v3 = org.xmcda.Factory.criteriaHierarchy();
			criteriaHierarchy_v3.setId(hierarchy_v2.getId());
			criteriaHierarchy_v3.setName(hierarchy_v2.getName());
			criteriaHierarchy_v3.setMcdaConcept(hierarchy_v2.getMcdaConcept());

			criteriaHierarchy_v3.setDescription(new DescriptionConverter().convertTo_v3(hierarchy_v2.getDescription()));

			for (org.xmcda.v2.Node node_v2: hierarchy_v2.getNode())
				convertTo_v3_criteriaID(node_v2, criteriaHierarchy_v3, xmcda_v3);
			xmcda_v3.criteriaHierarchiesList.add(criteriaHierarchy_v3);
		}
		if (v3tag == CRITERIA_SET_ID)
		{
			new CriteriaSetsHierarchyConverter().convertTo_v3(hierarchy_v2, xmcda_v3);
		}
			;//convertTo_v3_criteriaSetID(hierarchy_v2, criteriaHierarchy_v3, xmcda_v3);

		getWarnings().popTag(); // HIERARCHY
	}

	private void convertTo_v3_criteriaID(org.xmcda.v2.Node node_v2,
	                                     org.xmcda.CriterionHierarchyNode node_v3, org.xmcda.XMCDA xmcda_v3)
	{
		org.xmcda.Criterion criterion = getCriterion(node_v2, xmcda_v3);
		org.xmcda.CriterionHierarchyNode node = org.xmcda.Factory.criterionHierarchyNode(criterion);
		if (!node_v2.getValue().isEmpty())
			getWarnings().elementIgnored("value",
					"There is no <values> attached to a criteria hierarchy node in XMCDA v3");
		if (!node_v2.getValues().isEmpty())
			getWarnings().elementIgnored("values",
					"There is no <values> attached to a criteria hierarchy node in XMCDA v3");


		if (!node_v3.addChild(node))
			getWarnings().elementIgnored("node", "Duplicate hierarchy node for criterion "+criterion.id());

		for (org.xmcda.v2.Node nv2: node_v2.getNode())
			convertTo_v3_criteriaID(nv2, node, xmcda_v3);
	}

	/**
	 * Returns the xmcda v3 criterion associated to an XMCDA v2 node, using this node's id if possible. If the XMCDA
	 * v2 node does not declare a criterionID, a new one is created (this is needed because XMCDA v3 criteria
	 * hierarchy nodes all refer to a criterion (but xmcda v2 allows node to have nodes only).
	 * 
	 * @param node_v2 an XMCDA v2 node.
	 * @param xmcda_v3 the XMCDA v3 object in which the criterion should be retrieved or created.
	 * @return the corresponding XMCDA v3 criterion
	 */
	private org.xmcda.Criterion getCriterion(org.xmcda.v2.Node node_v2, org.xmcda.XMCDA xmcda_v3)
	{
		org.xmcda.Criterion criterion;
		if (node_v2.getCriterionID() == null)
		{
			String fake_crit_id_basename = node_v2.getId();
			if (fake_crit_id_basename==null || fake_crit_id_basename.length()==0)
				fake_crit_id_basename = "hierarchy criterion";
			else
				fake_crit_id_basename = "hierarchy criterion "+fake_crit_id_basename;
				
			String fake_crit_id = fake_crit_id_basename;
			int i = 0;
			while ( xmcda_v3.criteria.get(fake_crit_id, false) != null ) // search a name that is not registered
				fake_crit_id = fake_crit_id_basename + " " + ++i;
			criterion = xmcda_v3.criteria.get(fake_crit_id, true);
		}
		else
			criterion = xmcda_v3.criteria.get(node_v2.getCriterionID());
		return criterion;
	}

	// ----    v3 -> v2    ----
	public void convertTo_v2(List<org.xmcda.CriteriaHierarchy> criteriaHierarchies_v3, org.xmcda.v2.XMCDA xmcda_v2)
	{
		for (org.xmcda.CriteriaHierarchy criteriaHierarchy: criteriaHierarchies_v3)
		{
			org.xmcda.v2.Hierarchy hierarchy_v2 = convertTo_v2(criteriaHierarchy);
			xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters()
			        .add(new JAXBElement<>(new QName(HIERARCHY), org.xmcda.v2.Hierarchy.class, hierarchy_v2));
		}
	}

	private org.xmcda.v2.Hierarchy convertTo_v2(org.xmcda.CriteriaHierarchy criteriaHierarchy_v3)
	{
		org.xmcda.v2.Hierarchy hierarchy_v2 = new org.xmcda.v2.Hierarchy();

		getWarnings().pushTag(CRITERIA_HIERARCHY, criteriaHierarchy_v3.id());

		// default attributes
		hierarchy_v2.setId(criteriaHierarchy_v3.id());
		hierarchy_v2.setName(criteriaHierarchy_v3.name());
		hierarchy_v2.setMcdaConcept(criteriaHierarchy_v3.mcdaConcept());

		// description
		final org.xmcda.Description desc_v3 = criteriaHierarchy_v3.getDescription();
		hierarchy_v2.setDescription(new DescriptionConverter().convertTo_v2(desc_v3));

		// nodes 1..*
		getWarnings().pushTag(HierarchyConverter.NODES, criteriaHierarchy_v3.id());
		for (org.xmcda.CriterionHierarchyNode rootNodes_v2: criteriaHierarchy_v3.getRootNodes())
			hierarchy_v2.getNode().add(convert_node_to_v2(rootNodes_v2));
		getWarnings().popTag(); // NODES
			
		getWarnings().popTag(); // HIERARCHY
		return hierarchy_v2;
	}

	private org.xmcda.v2.Node convert_node_to_v2(org.xmcda.CriterionHierarchyNode node_v3)
	{
		// criterion
		org.xmcda.v2.Node node_v2 = new org.xmcda.v2.Node();
		final String node_id = node_v3.getCriterion().id();
		getWarnings().pushTag(HierarchyConverter.NODE, node_id);
		node_v2.setCriterionID(node_id);
		// nodes 1..*
		for (org.xmcda.CriterionHierarchyNode childNode_v3: node_v3.getChildren())
			node_v2.getNode().add(convert_node_to_v2(childNode_v3));
		getWarnings().popTag(); // NODE
		return node_v2;
	}

}
