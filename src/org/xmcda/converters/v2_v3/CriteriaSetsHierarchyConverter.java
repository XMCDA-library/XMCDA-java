package org.xmcda.converters.v2_v3;

import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

public class CriteriaSetsHierarchyConverter
    extends Converter
{
	public static final String CRITERIA_SETS_HIERARCHY = org.xmcda.CriteriaSetsHierarchy.TAG;
	public static final String HIERARCHY = "hierarchy";

	CriteriaSetsHierarchyConverter()
	{
		super(CRITERIA_SETS_HIERARCHY);
	}

	/**
	 * @param hierarchy_v2
	 * @param xmcda_v3
	 * @throw IllegalArgumentException if the v2 hierarchy contains attributes (there is no such thing in XMCDA v3),
	 *        or if it mixes alternatives, criteria and/or categories.
	 */
	void convertTo_v3(org.xmcda.v2.Hierarchy hierarchy_v2, org.xmcda.XMCDA xmcda_v3)
	{
		getWarnings().pushTag(HIERARCHY, hierarchy_v2.getId());
		org.xmcda.CriteriaSetsHierarchy criteriaSetsHierarchy_v3 = org.xmcda.Factory.criteriaSetsHierarchy();
		criteriaSetsHierarchy_v3.setId(hierarchy_v2.getId());
		criteriaSetsHierarchy_v3.setName(hierarchy_v2.getName());
		criteriaSetsHierarchy_v3.setMcdaConcept(hierarchy_v2.getMcdaConcept());

		criteriaSetsHierarchy_v3.setDescription(new DescriptionConverter().convertTo_v3(hierarchy_v2.getDescription()));

		for (org.xmcda.v2.Node node_v2: hierarchy_v2.getNode())
			convertTo_v3_criteriaSetsOrCriteriaSetsID(node_v2, criteriaSetsHierarchy_v3, xmcda_v3);
		xmcda_v3.criteriaSetsHierarchiesList.add(criteriaSetsHierarchy_v3);

		getWarnings().popTag(); // HIERARCHY
	}

	private void convertTo_v3_criteriaSetsOrCriteriaSetsID(org.xmcda.v2.Node node_v2,
	                                                       org.xmcda.CriteriaSetHierarchyNode node_v3,
	                                                       org.xmcda.XMCDA xmcda_v3)
	{
		org.xmcda.CriteriaSet<?> criteriaSet;
		if (node_v2.getCriteriaSet() != null)
		{
			criteriaSet = new CriteriaSetConverter().convertTo_v3(node_v2.getCriteriaSet(), xmcda_v3);
			
		}
		else
			criteriaSet = getCriteriaSet(node_v2, xmcda_v3);
		org.xmcda.CriteriaSetHierarchyNode node = org.xmcda.Factory.criteriaSetHierarchyNode(criteriaSet);
		
		if (!node_v2.getValue().isEmpty())
			getWarnings().elementIgnored("value",
					"There is no <values> attached to a criteria sets hierarchy node in XMCDA v3");
		if (!node_v2.getValues().isEmpty())
			getWarnings().elementIgnored("values",
					"There is no <values> attached to a criteria sets hierarchy node in XMCDA v3");


		if (!node_v3.addChild(node))
			getWarnings().elementIgnored("node", "Duplicate hierarchy node for criteria set "+criteriaSet.id());

		for (org.xmcda.v2.Node nv2: node_v2.getNode())
			convertTo_v3_criteriaSetsOrCriteriaSetsID(nv2, node, xmcda_v3);
		
	}

	/**
	 * Returns the xmcda v3 criterion associated to an XMCDA v2 node, using this node's id if possible. If the XMCDA
	 * v2 node does not declare a criterionID, a new one is created (this is needed because XMCDA v3 criteria
	 * hierarchy nodes all refer to a criterion (but xmcda v2 allows node to have nodes only).
	 * 
	 * @param node_v2 an XMCDA v2 node.
	 * @param xmcda_v3 the XMCDA v3 object in which the criterion should be retrieved or created.
	 * @return the corresponding XMCDA v3 criterion
	 */
	private org.xmcda.CriteriaSet<?> getCriteriaSet(org.xmcda.v2.Node node_v2, org.xmcda.XMCDA xmcda_v3)
	{
		org.xmcda.CriteriaSet<?> criteriaSet;
		if (node_v2.getCriteriaSetID() == null)
		{
			String fake_critSet_id_basename = node_v2.getId();
			if (fake_critSet_id_basename==null || fake_critSet_id_basename.length()==0)
				fake_critSet_id_basename = "hierarchy criteriaSet";
			else
				fake_critSet_id_basename = "hierarchy criteriaSet "+fake_critSet_id_basename;
				
			String fake_critSet_id = fake_critSet_id_basename;
			int i = 0;
			while ( xmcda_v3.criteriaSets.get(fake_critSet_id, false) != null ) // search a name that is not registered
				fake_critSet_id = fake_critSet_id_basename + " " + ++i;
			criteriaSet = xmcda_v3.criteriaSets.get(fake_critSet_id, true);
		}
		else
			criteriaSet = xmcda_v3.criteriaSets.get(node_v2.getCriteriaSetID());
		return criteriaSet;
	}

	// ----    v3 -> v2    ----
	public void convertTo_v2(List<org.xmcda.CriteriaSetsHierarchy> criteriaSetsHierarchies_v3,
	                         org.xmcda.v2.XMCDA xmcda_v2)
	{
		for (org.xmcda.CriteriaSetsHierarchy criteriaSetsHierarchy: criteriaSetsHierarchies_v3)
		{
			org.xmcda.v2.Hierarchy hierarchy_v2 = convertTo_v2(criteriaSetsHierarchy);
			xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters()
			        .add(new JAXBElement<>(new QName(HIERARCHY), org.xmcda.v2.Hierarchy.class, hierarchy_v2));
		}
	}

	private org.xmcda.v2.Hierarchy convertTo_v2(org.xmcda.CriteriaSetsHierarchy criteriaSetsHierarchy_v3)
	{
		org.xmcda.v2.Hierarchy hierarchy_v2 = new org.xmcda.v2.Hierarchy();

		getWarnings().pushTag(CRITERIA_SETS_HIERARCHY, criteriaSetsHierarchy_v3.id());

		// default attributes
		hierarchy_v2.setId(criteriaSetsHierarchy_v3.id());
		hierarchy_v2.setName(criteriaSetsHierarchy_v3.name());
		hierarchy_v2.setMcdaConcept(criteriaSetsHierarchy_v3.mcdaConcept());

		// description
		final org.xmcda.Description desc_v3 = criteriaSetsHierarchy_v3.getDescription();
		hierarchy_v2.setDescription(new DescriptionConverter().convertTo_v2(desc_v3));

		// nodes 1..*
		getWarnings().pushTag(HierarchyConverter.NODES, criteriaSetsHierarchy_v3.id());
		for (org.xmcda.CriteriaSetHierarchyNode rootNodes_v2: criteriaSetsHierarchy_v3.getRootNodes())
			hierarchy_v2.getNode().add(convert_node_to_v2(rootNodes_v2));
		getWarnings().popTag(); // NODES
			
		getWarnings().popTag(); // HIERARCHY
		return hierarchy_v2;
	}

	private org.xmcda.v2.Node convert_node_to_v2(org.xmcda.CriteriaSetHierarchyNode node_v3)
	{
		// criteriaSet
		org.xmcda.v2.Node node_v2 = new org.xmcda.v2.Node();
		final String node_id = node_v3.getCriteriaSet().id();
		getWarnings().pushTag(HierarchyConverter.NODE, node_id);
		node_v2.setCriteriaSetID(node_id);
		// nodes 1..*
		for (org.xmcda.CriteriaSetHierarchyNode childNode_v3: node_v3.getChildren())
			node_v2.getNode().add(convert_node_to_v2(childNode_v3));
		getWarnings().popTag(); // NODE
		return node_v2;
	}
}
