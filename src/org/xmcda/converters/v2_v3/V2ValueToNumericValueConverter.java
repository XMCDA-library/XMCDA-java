/**
 * 
 */
package org.xmcda.converters.v2_v3;

import org.xmcda.v2.NumericValue;
import org.xmcda.v2.Value;

/**
 * @author Sébastien Bigaret <sebastien.bigaret@telecom-bretagne.eu>
 *
 */
public class V2ValueToNumericValueConverter
{
	/**
	 * Converts a Value to a Numeric value (xml type 'numeric' is a subset of 'value', with only integer, real, rational
	 * an NA)
	 * 
	 * @param value
	 *            the value to convert
	 * @return the corrresponding NumericValue
	 */
	public static NumericValue convert(Value value)
	{
		NumericValue nValue = new NumericValue();

		nValue.setId(value.getId());
		nValue.setName(value.getName());
		nValue.setMcdaConcept(value.getMcdaConcept());
		nValue.setDescription(value.getDescription());
		nValue.setInteger(value.getInteger());
		nValue.setReal(value.getReal());
		nValue.setRational(value.getRational());
		nValue.setNA(value.getNA());
		
		return nValue;
	}
	public static Value convert(NumericValue nvalue)
	{
		Value value = new Value();

		value.setId(nvalue.getId());
		value.setName(nvalue.getName());
		value.setMcdaConcept(nvalue.getMcdaConcept());
		value.setDescription(nvalue.getDescription());
		value.setInteger(nvalue.getInteger());
		value.setReal(nvalue.getReal());
		value.setRational(nvalue.getRational());
		value.setNA(nvalue.getNA());

		return value;
	}
}
