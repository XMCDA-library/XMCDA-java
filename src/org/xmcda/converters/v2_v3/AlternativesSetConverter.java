package org.xmcda.converters.v2_v3;

import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;


public class AlternativesSetConverter
    extends Converter
{
	public static final String ALTERNATIVES_SET = "alternativesSet";

	public AlternativesSetConverter()
	{
		super(ALTERNATIVES_SET);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
    public org.xmcda.AlternativesSet convertTo_v3(org.xmcda.v2.AlternativesSet alternativesSet_v2, org.xmcda.XMCDA xmcda_v3)
	{
		// TODO ignorés pour le moment: value et values
		String alternativesSetID = alternativesSet_v2.getId();
		getWarnings().pushTag(ALTERNATIVES_SET, alternativesSetID);

		if (alternativesSetID == null || "".equals(alternativesSetID))
		{
			// we are called by with a alternativesSet with no ID, which may happen when the alternativesSet is embedded in another structure,
			// such as in <alternativesValues>
			// we need to find one which is not already assigned: alternativesSet must have an ID in v3
			final String baseID="alternativesSet_generatedID_";
			int idx=1;
			alternativesSetID = baseID+idx;
			while (xmcda_v3.alternativesSets.contains(alternativesSetID))
			{
				idx = idx+1;
				alternativesSetID = baseID+idx;
			}
		}
		if (!alternativesSet_v2.getValueOrValues().isEmpty())
			getWarnings().elementIgnored("value", "value ou values ignored on alternativesSet: they are NOT translated into v3 alternativesSetsValues by this converter");
		org.xmcda.AlternativesSet alternativesSet_v3 = org.xmcda.Factory.alternativesSet();
		alternativesSet_v3.setId(alternativesSetID);
		alternativesSet_v3.setName(alternativesSet_v2.getName());
		alternativesSet_v3.setMcdaConcept(alternativesSet_v2.getMcdaConcept());
		
		alternativesSet_v3.setDescription(new DescriptionConverter().convertTo_v3(alternativesSet_v2.getDescription()));
		
		for (org.xmcda.v2.AlternativesSet.Element element: alternativesSet_v2.getElement())
		{
			getWarnings().pushTag("element");
			// ignoring: description, rank, value, values
			if (element.getDescription()!=null)
				getWarnings().elementIgnored("description");
			alternativesSet_v3.put(xmcda_v3.alternatives.get(element.getAlternativeID()),
			                   values_convertTo_v3(element.getRankOrValueOrValues(), xmcda_v3));
			getWarnings().popTag(); // "element"
		}
		getWarnings().popTag(); // ALTERNATIVES_SET
		xmcda_v3.alternativesSets.add(alternativesSet_v3);
		return alternativesSet_v3;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
    protected org.xmcda.QualifiedValues values_convertTo_v3(List<JAXBElement<?>> values, org.xmcda.XMCDA xmcda_v3)
	{
		org.xmcda.QualifiedValues values_v3 = org.xmcda.Factory.qualifiedValues();


		for (JAXBElement element: values)
		{
			getWarnings().pushTag(element.getName().getLocalPart());
			if ("rank".equals(element.getName().getLocalPart()))
				values_v3.add(new QualifiedValueConverter().convertTo_v3((org.xmcda.v2.Value)element.getValue(), xmcda_v3));
			if ("value".equals(element.getName().getLocalPart()))
				values_v3.add(new QualifiedValueConverter().convertTo_v3((org.xmcda.v2.Value)element.getValue(), xmcda_v3));
			if ("values".equals(element.getName().getLocalPart()))
			{
				final org.xmcda.v2.Values values_v2 = (org.xmcda.v2.Values) element.getValue();
				getWarnings().setTagID(values_v2.getId());
				if (values_v2.getId()!=null)
					getWarnings().attributeIgnored("id");
				if (values_v2.getName()!=null)
					getWarnings().attributeIgnored("name");
				if (values_v2.getMcdaConcept()!=null)
					getWarnings().attributeIgnored("mcdaConcept");
				if (values_v2.getDescription()!=null)
					getWarnings().elementIgnored("description");

				for (org.xmcda.v2.Value value: values_v2.getValue())
					values_v3.add(new QualifiedValueConverter().convertTo_v3(value, xmcda_v3));
			}
			getWarnings().popTag();
		}
		return values_v3;
	}
	
	public <T> org.xmcda.v2.AlternativesSet convertTo_v2(org.xmcda.AlternativesSet<T> alternativesSet_v3, org.xmcda.v2.XMCDA xmcda_v2)
	{
		org.xmcda.v2.AlternativesSet alternativesSet_v2 = new org.xmcda.v2.AlternativesSet();

		getWarnings().pushTag(ALTERNATIVES_SET, alternativesSet_v3.id());

		// default attributes
		alternativesSet_v2.setId(alternativesSet_v3.id());
		alternativesSet_v2.setName(alternativesSet_v3.name());
		alternativesSet_v2.setMcdaConcept(alternativesSet_v3.mcdaConcept());

		// description
		final org.xmcda.Description desc_v3 = alternativesSet_v3.getDescription();
		alternativesSet_v2.setDescription(new DescriptionConverter().convertTo_v2(desc_v3));

		// elements
		for (org.xmcda.Alternative alternative_v3: alternativesSet_v3.keySet())
		{
			getWarnings().pushTag("alternative", alternative_v3.id());

			org.xmcda.v2.AlternativesSet.Element element = new org.xmcda.v2.AlternativesSet.Element();
			element.setAlternativeID(alternative_v3.id());

			getWarnings().pushTag("values");
			final Object _v = ValuesConverter.convertTo_v2(alternativesSet_v3.get(alternative_v3)); // may be null
			if (_v instanceof org.xmcda.v2.Value)
				element.getRankOrValueOrValues()
				        .add(new JAXBElement<>(new QName("value"), org.xmcda.v2.Value.class, (org.xmcda.v2.Value)_v));
			else if (_v instanceof org.xmcda.v2.Values)
			    element.getRankOrValueOrValues()
			            .add(new JAXBElement<>(new QName("values"), org.xmcda.v2.Values.class, (org.xmcda.v2.Values)_v));
			getWarnings().popTag(); // "values"

			alternativesSet_v2.getElement().add(element);

			getWarnings().popTag(); // "alternative"
		}
		getWarnings().popTag(); // ALTERNATIVES_SET
		return alternativesSet_v2;
	}
}
