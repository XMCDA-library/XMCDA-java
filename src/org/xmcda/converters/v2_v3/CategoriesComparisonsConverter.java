/**
 * 
 */
package org.xmcda.converters.v2_v3;

import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import org.xmcda.Factory;
import org.xmcda.QualifiedValue;

/**
 * @author Sébastien Bigaret <sebastien.bigaret@telecom-bretagne.eu>
 *
 */
public class CategoriesComparisonsConverter
extends Converter
{
	public static final String CATEGORIES_COMPARISONS = "categoriesComparisons";
	public static final String PAIRS                    = "pairs";
	public static final String PAIR                     = "pair";

	public CategoriesComparisonsConverter()
	{
		super(CATEGORIES_COMPARISONS); // ajouter un constructeur super(tag_v2, tag_v3) ?
	}
	
	// v2 -> v3
	
	public void convertTo_v3(org.xmcda.v2.CategoriesComparisons value, org.xmcda.XMCDA xmcda_v3)
	{
		// determine whether we convert categories or categoriesSets/categoriesSetIDs
		final org.xmcda.v2.CategoriesComparisons.Pairs pairs = value.getPairs();
		if ( pairs == null || pairs.getPair().size()==0 )
		{
			convertCategoriesComparisonsTo_v3(value, xmcda_v3);
			return;
		}
		org.xmcda.v2.CategoriesComparisons.Pairs.Pair pair = pairs.getPair().get(0);
		String initial_id = pair.getInitial().getCategoryID();
		if ( initial_id != null )
		{
			// either only categoriesSets ...
			convertCategoriesComparisonsTo_v3(value, xmcda_v3);
		}
		else
		{
			// ... or categoriesSets / categoriesSetIDs
			convertCategoriesSetsComparisonsTo_v3(value, xmcda_v3);
		}
	}

	// Implementation note:
	// convertCategoriesComparisonsTo_v3 & convertCategoriesSetsComparisonsTo_v3 are very similar
	// Changes in one probably means changing the other one 
	public void convertCategoriesSetsComparisonsTo_v3(org.xmcda.v2.CategoriesComparisons value, org.xmcda.XMCDA xmcda_v3)
	{
		getWarnings().pushTag(CATEGORIES_COMPARISONS, value.getId());
		
		org.xmcda.CategoriesSetsMatrix<?,?> categoriesSetsMatrix_v3 = Factory.categoriesSetsMatrix();
		// defaultAttributes
		categoriesSetsMatrix_v3.setId(value.getId());
		categoriesSetsMatrix_v3.setMcdaConcept(value.getMcdaConcept());
		categoriesSetsMatrix_v3.setName(value.getName());
		categoriesSetsMatrix_v3.setDescription(new DescriptionConverter().convertTo_v3(value.getDescription()));

		// valuation: scale
		if ( value.getValuation() != null )
			categoriesSetsMatrix_v3.setValuation(new ScaleConverter().convertTo_v3(value.getValuation(), xmcda_v3));

		// pair 0..*
		if (value.getPairs().getDescription() != null)
			getWarnings().elementIgnored("description", Warnings.ABSENT_IN_V3_0);

		// 
		final org.xmcda.v2.CategoriesComparisons.Pairs pairs = value.getPairs();

		// TODO v2 accepts empty Matrix
		if ( pairs == null || pairs.getPair().size()==0 )
		{
			getWarnings().elementUnimplemented("empty v2"); // TODO c'est pas unimplemented qu'il faut
			return;
		}

		getWarnings().pushTag(PAIRS, value.getId());

		/* comparisonType is present in examples of weightsFromCondorcetAndPreferences only
		 * and it is not used.  It is not present in v3.  The idea is that, if something like that
		 * is necessary, it should be passed as a methodParameter.
		 */
		final String comparisonType = value.getComparisonType();
		if ( comparisonType!=null && !"".equals(comparisonType) )
			getWarnings().elementIgnored("comparisonType");

		// pair[0..*]
		for (org.xmcda.v2.CategoriesComparisons.Pairs.Pair pair: pairs.getPair())
		{
			/* one of them */
			org.xmcda.CategoriesSet<?> initial = null;
			
			String initial_id = pair.getInitial().getCategoryID();
			String initial_set_id = pair.getInitial().getCategoriesSetID();
			org.xmcda.v2.CategoriesSet initial_set = pair.getInitial().getCategoriesSet();
			
			if ( initial_id != null )
			{
				throw new IllegalArgumentException("categoryID unexpected");
			}
			else if ( initial_set_id != null )
			{
				initial = xmcda_v3.categoriesSets.get(initial_set_id, true);
			}
			else if ( initial_set != null )
			{
				// the converter already assigns a generated id if there was none
				initial = new CategoriesSetConverter().convertTo_v3(initial_set, xmcda_v3);
				xmcda_v3.categoriesSets.add((org.xmcda.CategoriesSet)initial);
			}
			else
				throw new IllegalArgumentException("Invalid pair, no categoriesSetID nor categoriesSet in initial");
			/* */
			org.xmcda.CategoriesSet<?> terminal = null;

			String terminal_id = pair.getTerminal().getCategoryID();
			String terminal_set_id = pair.getTerminal().getCategoriesSetID();
			org.xmcda.v2.CategoriesSet terminal_set = pair.getTerminal().getCategoriesSet();
			
			if ( terminal_id != null )
			{
				throw new IllegalArgumentException("categoryID unexpected");
			}
			else if ( terminal_set_id != null )
			{
				terminal = xmcda_v3.categoriesSets.get(terminal_set_id, true);
			}
			else if ( terminal_set != null )
			{
				// the converter already assigns a generated id if there was none
				terminal = new CategoriesSetConverter().convertTo_v3(terminal_set, xmcda_v3);
				xmcda_v3.categoriesSets.add((org.xmcda.CategoriesSet)terminal);
			}
			else
				throw new IllegalArgumentException("Invalid pair, no categoriesSetID nor categoriesSet in initial");

			// categoriesSetsMatrix en v3 ne contient que des categoryID row/column
			List<Object> pairValue_v2 = pair.getValueOrValues();
			org.xmcda.QualifiedValues values_v3 = Factory.qualifiedValues();
			if ( pairValue_v2==null || pairValue_v2.size()==0 )
			{
				// no value: assign N/A
				QualifiedValue tmpValue = Factory.qualifiedValue();
				tmpValue.setValue(org.xmcda.value.NA.na);
				values_v3.add(tmpValue);
			}
			else
			{
				// iterate on valueOrValues, convert them and and add them to values_v3
				for (Object valueOrValues: pairValue_v2)
				{
					if (valueOrValues instanceof org.xmcda.v2.Value)
					{
						org.xmcda.v2.Value tmpValue_v2 = (org.xmcda.v2.Value) valueOrValues;
						values_v3.add(new QualifiedValueConverter().convertTo_v3(tmpValue_v2, xmcda_v3));
					}
					else
					{
						org.xmcda.v2.Values tmpValues_v2 = (org.xmcda.v2.Values) valueOrValues;
						// TODO ignore description mcdaConcept etc.
						for (org.xmcda.v2.Value tmpValue_v2 : tmpValues_v2.getValue())
						{
							values_v3.add(new QualifiedValueConverter().convertTo_v3(tmpValue_v2, xmcda_v3));
						}
					}
				}
			}
			categoriesSetsMatrix_v3.put(new org.xmcda.utils.Coord(initial, terminal), values_v3);
		}
		getWarnings().popTag(); // PAIRS

		getWarnings().popTag(); // CATEGORIES_COMPARISONS
		xmcda_v3.categoriesSetsMatricesList.add(categoriesSetsMatrix_v3);
	}

	// Implementation note:
	// convertCategoriesComparisonsTo_v3 & convertCategoriesSetsComparisonsTo_v3 are very similar
	// Changes in one probably means changing the other one 
	public void convertCategoriesComparisonsTo_v3(org.xmcda.v2.CategoriesComparisons value, org.xmcda.XMCDA xmcda_v3)
	{
		getWarnings().pushTag(CATEGORIES_COMPARISONS, value.getId());

		org.xmcda.CategoriesMatrix<?> categoriesMatrix_v3 = Factory.categoriesMatrix();
		// defaultAttributes
		categoriesMatrix_v3.setId(value.getId());
		categoriesMatrix_v3.setMcdaConcept(value.getMcdaConcept());
		categoriesMatrix_v3.setName(value.getName());
		categoriesMatrix_v3.setDescription(new DescriptionConverter().convertTo_v3(value.getDescription()));

		// valuation: scale
		if ( value.getValuation() != null )
			categoriesMatrix_v3.setValuation(new ScaleConverter().convertTo_v3(value.getValuation(), xmcda_v3));
		
		// pair 0..*
		if (value.getPairs().getDescription() != null)
			getWarnings().elementIgnored("description", Warnings.ABSENT_IN_V3_0);

		// 
		final org.xmcda.v2.CategoriesComparisons.Pairs pairs = value.getPairs();

		// TODO v2 accepts empty Matrix
		if ( pairs == null || pairs.getPair().size()==0 )
		{
			getWarnings().elementUnimplemented("empty v2"); // TODO c'est pas unimplemented qu'il faut
			return;
		}

		getWarnings().pushTag(PAIRS, value.getId());

		/* comparisonType is present in examples of weightsFromCondorcetAndPreferences only
		 * and it is not used.  It is not present in v3.  The idea is that, if something like that
		 * is necessary, it should be passed as a programParameter.
		 */
		final String comparisonType = value.getComparisonType();
		if ( comparisonType!=null && !"".equals(comparisonType) )
			getWarnings().elementIgnored("comparisonType");

		// pair[0..*]
		for (org.xmcda.v2.CategoriesComparisons.Pairs.Pair pair: pairs.getPair())
		{
			/* one of them */
			Object initial = null;
			
			String initial_id = pair.getInitial().getCategoryID();
			String initial_set_id = pair.getInitial().getCategoriesSetID();
			org.xmcda.v2.CategoriesSet initial_set = pair.getInitial().getCategoriesSet();
			
			if ( initial_id != null )
			{
				initial = xmcda_v3.categories.get(initial_id, true);
			}
			else if ( initial_set_id != null )
			{
				throw new IllegalArgumentException("categoriesSetID unexpected");
				//initial = xmcda_v3.categoriesSets.get(initial_set_id, true);
			}
			else if ( initial_set != null )
			{
				throw new IllegalArgumentException("categoriesSet unexpected");
				//initial = new CategoriesSetConverter().convertTo_v3(initial_set, xmcda_v3);
			}
			else
				throw new IllegalArgumentException("Invalid pair, no categoriesID in initial");
			/* */
			Object terminal = null;

			String terminal_id = pair.getTerminal().getCategoryID();
			String terminal_set_id = pair.getTerminal().getCategoriesSetID();
			org.xmcda.v2.CategoriesSet terminal_set = pair.getTerminal().getCategoriesSet();
			
			if ( terminal_id != null )
			{
				terminal = xmcda_v3.categories.get(terminal_id, true);
			}
			else if ( terminal_set_id != null )
			{
				throw new IllegalArgumentException("categoriesSetID unexpected");
				//terminal = xmcda_v3.categoriesSets.get(terminal_set_id, true);
			}
			else if ( terminal_set != null )
			{
				throw new IllegalArgumentException("categoriesSet unexpected");
				//terminal = new CategoriesSetConverter().convertTo_v3(terminal_set, xmcda_v3);
			}
			else
				throw new IllegalArgumentException("Invalid pair, no categoriesID in terminal");

			// categoriesMatrix en v3 ne contient que des categoryID row/column
			List<Object> pairValue_v2 = pair.getValueOrValues();
			org.xmcda.QualifiedValues values_v3 = Factory.qualifiedValues();
			if ( pairValue_v2==null || pairValue_v2.size()==0 )
			{
				// no value: assign N/A
				QualifiedValue tmpValue = Factory.qualifiedValue();
				tmpValue.setValue(org.xmcda.value.NA.na);
				values_v3.add(tmpValue);
			}
			else
			{
				// iterate on valueOrValues, convert them and and add them to values_v3
				for (Object valueOrValues: pairValue_v2)
				{
					if (valueOrValues instanceof org.xmcda.v2.Value)
					{
						org.xmcda.v2.Value tmpValue_v2 = (org.xmcda.v2.Value) valueOrValues;
						values_v3.add(new QualifiedValueConverter().convertTo_v3(tmpValue_v2, xmcda_v3));
					}
					else
					{
						org.xmcda.v2.Values tmpValues_v2 = (org.xmcda.v2.Values) valueOrValues;
						// TODO ignore description mcdaConcept etc.
						for (org.xmcda.v2.Value tmpValue_v2 : tmpValues_v2.getValue())
						{
							values_v3.add(new QualifiedValueConverter().convertTo_v3(tmpValue_v2, xmcda_v3));
						}
					}
				}
			}
			categoriesMatrix_v3.put(new org.xmcda.utils.Coord(initial, terminal), values_v3);
		}
		getWarnings().popTag(); // PAIRS

		getWarnings().popTag(); // CATEGORIES_COMPARISONS
		xmcda_v3.categoriesMatricesList.add(categoriesMatrix_v3);
	}

	// v3 -> v2

	public void convertCategoriesMatricesTo_v2(List<org.xmcda.CategoriesMatrix<?>> categoriesMatrices_v3, org.xmcda.v2.XMCDA xmcda_v2)
	{
		for (org.xmcda.CategoriesMatrix categoriesMatrix_v3: categoriesMatrices_v3)
		{
			convertCategoriesMatrixTo_v2(categoriesMatrix_v3, xmcda_v2);
		}
	}

	/**
	 * Converts a categories matrix (v3) into a categories comparison (v2)
	 * @param categoriesMatrix_v3
	 * @param xmcda_v2
	 */
	public void convertCategoriesMatrixTo_v2(org.xmcda.CategoriesMatrix categoriesMatrix_v3, org.xmcda.v2.XMCDA xmcda_v2)
	{
		getWarnings().pushTag(org.xmcda.CategoriesMatrix.TAG);
		org.xmcda.v2.CategoriesComparisons categoriesComparisons_v2 = new org.xmcda.v2.CategoriesComparisons();
		
		List<JAXBElement<?>> ccs = xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters();
		ccs.add(new JAXBElement<org.xmcda.v2.CategoriesComparisons>(new QName(CATEGORIES_COMPARISONS),
		                                                              org.xmcda.v2.CategoriesComparisons.class, categoriesComparisons_v2));

		// default attributes
		categoriesComparisons_v2.setId(categoriesMatrix_v3.id());
		categoriesComparisons_v2.setName(categoriesMatrix_v3.name());
		categoriesComparisons_v2.setMcdaConcept(categoriesMatrix_v3.mcdaConcept());

		// description
		categoriesComparisons_v2.setDescription(new DescriptionConverter().convertTo_v2(categoriesMatrix_v3.getDescription()));

		// valuation
		categoriesComparisons_v2.setValuation(new ScaleConverter().convertTo_v2(categoriesMatrix_v3.getValuation()));
		
		// rows
		org.xmcda.v2.CategoriesComparisons.Pairs pairs_v2 = new org.xmcda.v2.CategoriesComparisons.Pairs();
		categoriesComparisons_v2.setPairs(pairs_v2);
		
		for (Object _coord: categoriesMatrix_v3.keySet())
		{
			org.xmcda.utils.Coord coord = (org.xmcda.utils.Coord) _coord;
			org.xmcda.v2.CategoriesComparisons.Pairs.Pair pair_v2 = new org.xmcda.v2.CategoriesComparisons.Pairs.Pair();
			org.xmcda.v2.CategoryReference categoryRef_initial = new org.xmcda.v2.CategoryReference();
			categoryRef_initial.setCategoryID( ((org.xmcda.Category) coord.x).id()) ;
			
			org.xmcda.v2.CategoryReference categoryRef_terminal = new org.xmcda.v2.CategoryReference();
			categoryRef_terminal.setCategoryID( ((org.xmcda.Category) coord.y).id()) ;

			pair_v2.setInitial(categoryRef_initial);
			pair_v2.setTerminal(categoryRef_terminal);

			// values
			final org.xmcda.QualifiedValues<?> values_v3 = (org.xmcda.QualifiedValues<?>) categoriesMatrix_v3.get(_coord);			
			final Object _v = ValuesConverter.convertTo_v2(values_v3); // may be null

			if (_v instanceof org.xmcda.v2.Value)
				pair_v2.getValueOrValues().add((org.xmcda.v2.Value) _v);
			else if (_v instanceof org.xmcda.v2.Values)
				pair_v2.getValueOrValues().add((org.xmcda.v2.Values) _v);

			pairs_v2.getPair().add(pair_v2);
		}
		getWarnings().popTag(); // org.xmcda.CategoriesMatrix.TAG
	}
	
	public void convertCategoriesSetsMatricesTo_v2(List<org.xmcda.CategoriesSetsMatrix<?,?>> categoriesSetsMatrices_v3, org.xmcda.v2.XMCDA xmcda_v2)
	{
		for (org.xmcda.CategoriesSetsMatrix categoriesSetsMatrix_v3: categoriesSetsMatrices_v3)
		{
			convertCategoriesSetsMatrixTo_v2(categoriesSetsMatrix_v3, xmcda_v2);
		}
	}

	/**
	 * Converts a categoriesSets matrix (v3) into a categories comparison (v2)
	 * @param categoriesMatrix_v3
	 * @param xmcda_v2
	 */
	public void convertCategoriesSetsMatrixTo_v2(org.xmcda.CategoriesSetsMatrix categoriesSetsMatrix_v3, org.xmcda.v2.XMCDA xmcda_v2)
	{
		getWarnings().pushTag(org.xmcda.CategoriesMatrix.TAG);
		org.xmcda.v2.CategoriesComparisons categoriesComparisons_v2 = new org.xmcda.v2.CategoriesComparisons();
		
		List<JAXBElement<?>> ccs = xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters();
		ccs.add(new JAXBElement<org.xmcda.v2.CategoriesComparisons>(new QName(CATEGORIES_COMPARISONS),
		                                                              org.xmcda.v2.CategoriesComparisons.class, categoriesComparisons_v2));

		// default attributes
		categoriesComparisons_v2.setId(categoriesSetsMatrix_v3.id());
		categoriesComparisons_v2.setName(categoriesSetsMatrix_v3.name());
		categoriesComparisons_v2.setMcdaConcept(categoriesSetsMatrix_v3.mcdaConcept());

		// description
		categoriesComparisons_v2.setDescription(new DescriptionConverter().convertTo_v2(categoriesSetsMatrix_v3.getDescription()));

		// valuation
		categoriesComparisons_v2.setValuation(new ScaleConverter().convertTo_v2(categoriesSetsMatrix_v3.getValuation()));
		
		// rows
		org.xmcda.v2.CategoriesComparisons.Pairs pairs_v2 = new org.xmcda.v2.CategoriesComparisons.Pairs();
		categoriesComparisons_v2.setPairs(pairs_v2);
		
		for (Object _coord: categoriesSetsMatrix_v3.keySet())
		{
			org.xmcda.utils.Coord coord = (org.xmcda.utils.Coord) _coord;
			org.xmcda.v2.CategoriesComparisons.Pairs.Pair pair_v2 = new org.xmcda.v2.CategoriesComparisons.Pairs.Pair();
			org.xmcda.v2.CategoryReference categoryRef_initial = new org.xmcda.v2.CategoryReference();
			categoryRef_initial.setCategoriesSetID( ((org.xmcda.CategoriesSet) coord.x).id() ) ;
			
			org.xmcda.v2.CategoryReference categoryRef_terminal = new org.xmcda.v2.CategoryReference();
			categoryRef_terminal.setCategoriesSetID( ((org.xmcda.CategoriesSet) coord.y).id() ) ;

			pair_v2.setInitial(categoryRef_initial);
			pair_v2.setTerminal(categoryRef_terminal);

			// values
			final org.xmcda.QualifiedValues<?> values_v3 = (org.xmcda.QualifiedValues<?>) categoriesSetsMatrix_v3.get(_coord);			
			final Object _v = ValuesConverter.convertTo_v2(values_v3); // may be null

			if (_v instanceof org.xmcda.v2.Value)
				pair_v2.getValueOrValues().add((org.xmcda.v2.Value) _v);
			else if (_v instanceof org.xmcda.v2.Values)
				pair_v2.getValueOrValues().add((org.xmcda.v2.Values) _v);

			pairs_v2.getPair().add(pair_v2);
		}
		getWarnings().popTag(); // org.xmcda.CategoriesMatrix.TAG
	}
}


