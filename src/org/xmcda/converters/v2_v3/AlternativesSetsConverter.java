package org.xmcda.converters.v2_v3;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import java.util.List;

/**
 * @author Sébastien Bigaret
 */
public class AlternativesSetsConverter
extends Converter
{
	public static final String ALTERNATIVES_SETS = org.xmcda.AlternativesSets.TAG;

	public AlternativesSetsConverter()
    {
		super(ALTERNATIVES_SETS);
    }

	public void convertTo_v3(org.xmcda.v2.AlternativesSets value, org.xmcda.XMCDA xmcda_v3)
	{
		getWarnings().pushTag(ALTERNATIVES_SETS, value.getId());
		
		org.xmcda.AlternativesSets alternativesSets_v3 = xmcda_v3.alternativesSets;

		// defaultAttributes
		alternativesSets_v3.setId(value.getId());
		alternativesSets_v3.setMcdaConcept(value.getMcdaConcept());
		alternativesSets_v3.setName(value.getName());

		// description
		alternativesSets_v3.setDescription(new DescriptionConverter().convertTo_v3(value.getDescription()));
		
		// alternativesSet 0..*
		for (org.xmcda.v2.AlternativesSet alternativesSet: value.getAlternativesSet())
		{
			xmcda_v3.alternativesSets.add(new AlternativesSetConverter().convertTo_v3(alternativesSet, xmcda_v3));
		}

		getWarnings().popTag(); // ALTERNATIVES_SETS
	}

	public <T> void convertTo_v2(final org.xmcda.AlternativesSets<T> alternativesSets_v3, org.xmcda.v2.XMCDA xmcda_v2)
	{
		if (alternativesSets_v3==null || alternativesSets_v3.isVoid())
			return;

		getWarnings().pushTag(ALTERNATIVES_SETS, alternativesSets_v3.id());
		
		org.xmcda.v2.AlternativesSets alternativesSets_v2 = new org.xmcda.v2.AlternativesSets();
		List<JAXBElement<?>> crits = xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters();
		crits.add(new JAXBElement<org.xmcda.v2.AlternativesSets>(new QName(ALTERNATIVES_SETS), org.xmcda.v2.AlternativesSets.class,
				alternativesSets_v2));

		// default attributes
		alternativesSets_v2.setId(alternativesSets_v3.id());
		alternativesSets_v2.setName(alternativesSets_v3.name());
		alternativesSets_v2.setMcdaConcept(alternativesSets_v3.mcdaConcept());

		// description
		alternativesSets_v2.setDescription(new DescriptionConverter().convertTo_v2(alternativesSets_v3.getDescription()));

		// criteria
		AlternativesSetConverter alternativesSetConverter = new AlternativesSetConverter();

		for (org.xmcda.AlternativesSet<T> alternativesSet_v3: alternativesSets_v3)
		{
			alternativesSets_v2.getAlternativesSet().add(alternativesSetConverter.convertTo_v2(alternativesSet_v3, xmcda_v2));
		}
		getWarnings().popTag(); // CRITERIA
	}

	
}
