package org.xmcda.converters.v2_v3;

import java.util.List;


/**
 * @author Sébastien Bigaret
 */
public class BibliographyConverter
    extends Converter
{
	public static final String BIBLIOGRAPHY = "bibliography";

	public static final String DESCRIPTION  = "description";

	public static final String BIB_ENTRY    = "bibEntry";

	public BibliographyConverter()
	{
		super(BIBLIOGRAPHY);
	}

	public org.xmcda.BibliographyEntry convertTo_v3(org.xmcda.v2.Bibliography bibliography)
	{
		if (bibliography == null)
		    return null;

		getWarnings().pushTag(BIBLIOGRAPHY, bibliography.getId());
		org.xmcda.BibliographyEntry bibEntry = org.xmcda.Factory.bibliographyEntry();
		/*
		 * Take the first description has the one we will use, ignore all others
		 */
		bibEntry.setId(bibliography.getId());
		bibEntry.setName(bibliography.getName());
		bibEntry.setMcdaConcept(bibliography.getMcdaConcept());

		for (Object element: bibliography.getDescriptionOrBibEntry())
		{
			final Object _value = element;

			if (_value != null && _value instanceof org.xmcda.v2.Description)
			{
				if (bibEntry.getDescription() == null)
					bibEntry.setDescription(new DescriptionConverter()
					        .convertTo_v3((org.xmcda.v2.Description) _value));
				else
					getWarnings().elementIgnored(DESCRIPTION, Warnings.ALL_BUT_FIRST_IGNORED);
			}

			if (_value != null && _value instanceof String)
			    bibEntry.getBibEntries().add((String) _value);
		}
		getWarnings().popTag(); // BIBLIOGRAPHY
		return bibEntry;
	}

	public org.xmcda.v2.Bibliography convertTo_v2(org.xmcda.BibliographyEntry bibliography_v3)
	{
		if (bibliography_v3 == null)
		    return null;

		getWarnings().pushTag(BIBLIOGRAPHY, bibliography_v3.id());
		
		org.xmcda.v2.Bibliography bibliography_v2 = new org.xmcda.v2.Bibliography();

		bibliography_v2.setId(bibliography_v3.id());
		bibliography_v2.setName(bibliography_v3.name());
		bibliography_v2.setMcdaConcept(bibliography_v3.mcdaConcept());

		List<Object> list = bibliography_v2.getDescriptionOrBibEntry();

		// description
		if (bibliography_v3.getDescription() != null)
		{
			list.add(new DescriptionConverter().convertTo_v2(bibliography_v3.getDescription()));
		}

		// bibEntry
		if (bibliography_v3.getBibEntries() != null)
		{
			for (String bibEntry: bibliography_v3.getBibEntries())
				list.add(bibEntry);
		}

		getWarnings().popTag(); // BIBLIOGRAPHY
		return bibliography_v2;
	}
}
