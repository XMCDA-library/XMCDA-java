package org.xmcda.value;

/**
 * @author Sébastien Bigaret
 * @param <T1>
 * @param <T2>
 */
public class Segment <T1, T2>
{
	private EndPoint<T1, T2> head;

	private EndPoint<T1, T2> tail;

	/**
	 * @return the head
	 */
	public EndPoint<T1, T2> getHead()
	{
		return head;
	}

	/**
	 * @param head
	 *            the head to set
	 */
	public void setHead(EndPoint<T1, T2> head)
	{
		this.head = head;
	}

	/**
	 * @return the tail
	 */
	public EndPoint<T1, T2> getTail()
	{
		return tail;
	}

	/**
	 * @param tail
	 *            the tail to set
	 */
	public void setTail(EndPoint<T1, T2> tail)
	{
		this.tail = tail;
	}
}
