/**
 * 
 */
package org.xmcda.utils;

import org.xmcda.Alternative;
import org.xmcda.Criterion;
import org.xmcda.Description;
import org.xmcda.HasDescription;

/**
 * @author Sébastien Bigaret
 *
 */
public class PerformanceTableCoord extends Coord<Alternative, Criterion> implements HasDescription
{
	public PerformanceTableCoord(Alternative a, Criterion c)
    {
		super(a, c);
		if (a==null && c==null)
			throw new NullPointerException("Invalid null value for alternative and criterion");
		else if (a==null)
			throw new NullPointerException("Invalid null value for alternative");
		else if (c==null)
			throw new NullPointerException("Invalid null value for criterion");
    }
	
	// HasDescription (start)

	private Description description;
	public void setDescription(Description description)
	{
		this.description = description;
	}

	public Description getDescription()
	{
		return description;
	}

	// HasDescription (end)

}

