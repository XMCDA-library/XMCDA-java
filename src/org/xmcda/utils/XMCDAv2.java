package org.xmcda.utils;

import org.xmcda.v2.XMCDA;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Sébastien Bigaret <sebastien.bigaret@telecom-bretagne.eu>
 */
public class XMCDAv2
{
	public static Map<String, Integer> map = new HashMap<>();

	public static Map<String, Integer> rootElementsSize(XMCDA xmcdav2)
	{
		Map<String, Integer> map = new HashMap<>();
		Integer value;
		for ( JAXBElement<?> tag: xmcdav2.getProjectReferenceOrMethodMessagesOrMethodParameters() )
		{
			String key = tag.getName().getLocalPart();

			if ( ( value = map.putIfAbsent(key, 1) ) != null )
				map.put(key, ++value);
		}
		return map;
	}

	/**
	 * Return a map of root XMCDA tags whose number of element in the initial and final status are different.
	 * Equivalent to {@link #difference(Map, Map, boolean) difference(initialStatus, finalStatus, false)}
	 * @param initialStatus
	 * @param finalStatus
	 * @return the differences between the two status
	 * @see #difference(Map, Map, boolean)
	 */
	public static Map<String, Integer> difference(Map<String, Integer> initialStatus,
												  Map<String, Integer> finalStatus)
	{
		return difference(initialStatus, finalStatus, false);
	}

	/**
	 * Compares two different status as returned by {@link #rootElementsSize(XMCDA)} and returns the difference of
	 * elements declared in each tag between the two status. The difference is calculated on the basis of the content
	 * of {@code finalStatus}.  When both maps comes from {@link #rootElementsSize(XMCDA)} they obviously have the same
	 * size and the set of keys. If it not the case, it may happen that a tag/key in the {@code finalStatus} is not
	 * present in {@code initialStatus}, it defaults to zero.
	 * <br/>
	 * This may be useful when loading different files one after the other e.g., to check which (and how
	 * many) rootElements have been added.
	 * @param initialStatus the initial status
	 * @param finalStatus   the final status
	 * @param includeAll    if {@code False}, every tag in final Status is returned with its difference, even when it
	 *                      is zero.  When {@code true}, only tags with a non-zero difference are included in the
	 *                      returned map.
	 * @return a map associating tags in status to the difference between the two parameters.
	 */
	public static Map<String, Integer> difference(Map<String, Integer> initialStatus,
												  Map<String, Integer> finalStatus,
												  boolean includeAll)
	{
		return org.xmcda.XMCDA.difference(initialStatus, finalStatus, includeAll);
	}

	public static void main(String args[]) throws IOException, JAXBException, SAXException
	{
		// Read performanceTable.xml
		org.xmcda.v2.XMCDA xmcda_v2 = new org.xmcda.v2.XMCDA();
		final File performanceTableFile = new File("tests/tests_XMCDAv2/performanceTable.xml");
		xmcda_v2.getProjectReferenceOrMethodMessagesOrMethodParameters()
		        .addAll(org.xmcda.parsers.xml.xmcda_v2.XMCDAParser.readXMCDA(performanceTableFile)
		                .getProjectReferenceOrMethodMessagesOrMethodParameters());
		rootElementsSize(xmcda_v2);
	}

}
