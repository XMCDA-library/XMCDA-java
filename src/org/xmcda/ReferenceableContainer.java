package org.xmcda;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;

/**
 * A set of {@link Referenceable} elements.
 * 
 * @author Sébastien Bigaret
 * @param <ELEMENT> the type of the elements contained in this container.
 */
public abstract class ReferenceableContainer <ELEMENT extends Referenceable>
    extends LinkedHashSet<ELEMENT>
    implements CommonAttributes, HasDescription, java.io.Serializable
{
	private static final long serialVersionUID = 1L;

	public boolean            getCreates       = true;

	public abstract Class<ELEMENT> elementClass();

	public ELEMENT get(String id)
	{
		return get(id, getCreates);
	}

	/**
	 * Returns the existing element stored in the container with the specified id, or if there is no such element,
	 * creates it if {@code create} is {@code true}, otherwise returns {@code null}.
	 * 
	 * @param id the id of the element to be searched in the container
	 * @param create determines whether an element with the specified id should be created and returned if it does not
	 *            already exist.
	 * @return the searched element, or {code null} if the container has no such element and {@code create} is
	 *         {@code false}.
	 */
	public ELEMENT get(String id, boolean create)
	{
		for (ELEMENT element: this)
			if ( element.id().equals(id) )
				return element;
		if (!create)
		    return null;
		ELEMENT element = Factory.build(elementClass(), id);
		this.add(element);
		return element;
	}

	/**
	 * Returns the ids of the elements in the container.
	 * 
	 * @return the ids of the elements in the container.
	 */
	public ArrayList<String> getIDs()
	{
		ArrayList<String> ids = new ArrayList<>();
		for (ELEMENT element: this)
			ids.add(element.id());
		return ids;
	}

	/**
	 * Returns the names of the elements in the container.
	 * 
	 * @return the names of the elements in the container.
	 */
	public ArrayList<String> getNames()
	{
		ArrayList<String> names = new ArrayList<>();
		for (ELEMENT element: this)
			names.add(element.name());
		return names;
	}

	/**
	 * Returns {@code true} if this container has an element with the specified id
	 * 
	 * @param id the id of the element to search within the container
	 * @return {@code true} if this container has an element with the specified id
	 */
	public boolean contains(String id)
	{
		return this.get(id, false) != null;
	}

	/**
	 * Adds the specified element to the container if there is no element with the same id, or merges the specified
	 * element into the existing one.
	 * 
	 * @param element the element whose content should be put within the container.
	 * @see Referenceable#merge(Referenceable)
	 */
	public void merge(ELEMENT element)
	{
		if (add(element))
		    return; // successful
		ELEMENT this_element = get(element.id());
		this_element.merge(element);
	}

	public boolean isVoid()
	{
		return this.size()==0 && this.id()==null && this.name()==null && this.mcdaConcept() == null && this.getDescription()==null;
	}

	/* LinkedHashSet in ReferenceableContainers overrides (start) */

	@Override
	public boolean add(ELEMENT e)
	{
		boolean r;
		if (r = super.add(e))
		    e.setContainer(this);
		return r;
	}

	@Override
	public boolean addAll(Collection<? extends ELEMENT> elementsCollection)
	{
		boolean modified = false;
		Iterator<? extends ELEMENT> it = elementsCollection.iterator();
		while (it.hasNext())
		{
			if (this.add(it.next()))
			    modified = true;
		}
		return modified;
	}

	@Override
	public Iterator<ELEMENT> iterator()
	{
		final Iterator<ELEMENT> it = super.iterator();
		return new Iterator<ELEMENT>()
		{
			ELEMENT current = null;

			@Override
			public boolean hasNext()
			{
				return it.hasNext();
			}

			@Override
			public ELEMENT next()
			{
				current = it.next();
				return current;
			}

			@Override
			public void remove()
			{
				it.remove();
				current.setContainer(null);
			}
		};
	}

	@Override
	public boolean remove(Object o)
	{
		boolean r;
		if (r = super.remove(o))
		{
			@SuppressWarnings("unchecked")
			final ELEMENT a = (ELEMENT) o;
			a.setContainer(null);
		}
		return r;
	}

	// retainAll() impl. uses Iterator.remove()

	@Override
	public boolean removeAll(Collection<?> collection)
	{
		boolean modified = false;
		for (Object o: collection)
			if (this.remove(o))
			{
				modified = true;
			}
		return modified;
	}

	/* LinkedHashSet in ReferenceableContainers overrides (end) */

	// CommonAttributes (start)

	/** The id attribute allows to identify the underlying piece of data by a program. */
	private String            id;

	/** The name attribute contains the human readable name of the object or concept. */
	private String            name;

	/**
	 * The mcdaConcept attribute allows to specify to what mcda concept a tag is related. It is used by an algorithm to
	 * make choices which will have an influence on the output. The documentation of the program should therefore
	 * specify, if necessary, what mcdaConcept should be used for the input data. In particular, if an algorithm
	 * requires, among other things, twice the same input tag, they can be differenciated by the mcdaConcept (this is
	 * even mandatory if they are present in the same file, but should be optional if the two tags can be in different
	 * input files, or originate from two different programs). The algorithm should therefore not be too strict on these
	 * mcdaConcepts, as this will reduce the compatibility between the various programs.
	 */
	private String            mcdaConcept;

	@Override
	public String id()
	{
		return id;
	}

	@Override
	public void setId(String id)
	{
		this.id = id;
	}

	@Override
	public String name()
	{
		return name;
	}

	@Override
	public String mcdaConcept()
	{
		return mcdaConcept;
	}

	@Override
	public void setName(String name)
	{
		this.name = name;
	}

	@Override
	public void setMcdaConcept(String mcdaConcept)
	{
		this.mcdaConcept = mcdaConcept;
	}

	// CommonAttributes (end)

	// HasDescription (start)

	private Description description;
	@Override
	public void setDescription(Description description)
	{
		this.description = description;
	}

	@Override
	public Description getDescription()
	{
		return description;
	}

	// HasDescription (end)

}
