package org.xmcda;

/**
 * @author Sébastien Bigaret
 *
 */
public interface HasDescription
{
	public void setDescription(Description description);

	public Description getDescription();
}
