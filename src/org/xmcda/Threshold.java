package org.xmcda;

import java.io.Serializable;

/**
 * xmcda:type:threshold
 * 
 * @author Sébastien Bigaret
 */
public class Threshold <T>
    implements CommonAttributes, Serializable
{
	/*
	 * Implementation note: a threshold is either a constant or an affine function. All its attributes should be reset()
	 * before setting one or more of them; this ensures that its nature (constant or affine) can always be derived from
	 * its attributes' values: a null value in constant means that it is an affine function, and that all three
	 * attributes 'type', 'slope' and 'intercept' are not null (and conversely, if the 'constant' is not null, all these
	 * three attributes are null).
	 */
	/**
	 * xmcda:element:threshold/affine/type
	 * 
	 * @author Sébastien Bigaret
	 */
	public static enum Type
	{
		DIRECT, INVERSE
	};

	private static final long serialVersionUID = 1L;

	private QualifiedValue<T> constant;

	private Type              type;

	private QualifiedValue<T> slope;

	private QualifiedValue<T> intercept;

	public Threshold(QualifiedValue<T> constant)
	{
		super();
		setConstant(constant);
	}

	public Threshold(QualifiedValue<T> slope, QualifiedValue<T> intercept)
	{
		super();
		setAffine(slope, intercept);
	}

	public Threshold(Type type, QualifiedValue<T> slope, QualifiedValue<T> intercept)
	{
		super();
		setAffine(type, slope, intercept);
	}

	/**
	 * Resets all attributes to {@code null}. This method must be called before setting any of the attributes
	 * 'constant', 'type', 'slope' or 'intercept'.
	 */
	protected void reset()
	{
		constant = null;
		type = null;
		slope = null;
		intercept = null;
	}

	public boolean isConstant()
	{
		return this.constant != null;
	}

	public boolean isAffine()
	{
		return !isConstant();
	}

	/**
	 * Returns the constant value for this threshold. Returned value is always {@code null} if the threshold is not a
	 * constant but an affine function.
	 * 
	 * @return the (non-null) constant value, or {@code null} if this is an affine threshold.
	 */
	public QualifiedValue<T> getConstant()
	{
		return this.constant;
	}

	/**
	 * Sets the value corresponding to a constant thresholds.
	 * 
	 * @param constant
	 *            the constant value for this threshold.
	 * @throws NullPointerException
	 *             if {@code constant]} is {@code null}
	 */
	public void setConstant(QualifiedValue<T> constant)
	{
		if (constant == null)
		    throw new NullPointerException("threshold's constant cannot be null");
		reset();
		this.constant = constant;
	}

	/**
	 * Returns the type of the affine threshold.
	 * 
	 * @return the (non-null) type of the affine threshold, or {@code null} if the threshold is a constant.
	 */
	public Type getType()
	{
		return this.type;
	}

	/**
	 * Returns the slope of the threshold's affine function. The returned value is always {@code null} if the threshold
	 * is a constant.
	 * 
	 * @return the (non-null) slope of the affine function, or {@code null} if the threshold is a constant.
	 */
	public QualifiedValue<T> getSlope()
	{
		return this.slope;
	}

	/**
	 * Returns the intercept of the threshold's affine function. The returned value is always {@code null} if the
	 * threshold is a constant.
	 * 
	 * @return the (non-null) intercept of the affine function, or {@code null} if the threshold is a constant.
	 */
	public QualifiedValue<T> getIntercept()
	{
		return this.intercept;
	}

	/**
	 * Sets the slope and intercept for an affine threshold. The threshold's {@link #getType() type} is set to
	 * {@value Type#DIRECT}.
	 * 
	 * @param slope
	 *            the slope for this affine threshold
	 * @param intercept
	 *            the intercept value for this affine threshold
	 * @throws NullPointerException
	 *             if either {@code slope} or {@code intercept} is {@code null}.
	 */
	public void setAffine(QualifiedValue<T> slope, QualifiedValue<T> intercept)
	{
		if (slope == null)
		    throw new NullPointerException("threshold's slope cannot be null");
		if (intercept == null)
		    throw new NullPointerException("threshold's intercept cannot be null");
		reset();
		this.type = Type.DIRECT;
		this.slope = slope;
		this.intercept = intercept;
	}

	/**
	 * Sets the slope, intercept and type for an affine threshold. The threshold's {@link #getType() type} is set to
	 * {@value Type#DIRECT}.
	 * 
	 * @param slope
	 *            the slope for this affine threshold. It may not be {@code null}.
	 * @param intercept
	 *            the intercept value for this affine threshold. It may not be {@code null}.
	 * @param type
	 *            the threshold's type. It may not be {@code null}.
	 * @throws NullPointerException
	 *             if either {@code slope}, {@code intercept} or {@code type} is {@code null}.
	 */
	public void setAffine(Type type, QualifiedValue<T> slope, QualifiedValue<T> intercept)
	{
		setAffine(slope, intercept);
		setType(type);
	}

	/**
	 * Sets the threshold's type.
	 * 
	 * @param type
	 *            the threshold's type. It may not be null.
	 * @throws IllegalStateException
	 *             if the threshold is not an {@link #isAffine() affine function}.
	 * @throws NullPointerException
	 *             if the type is {@code null}.
	 */
	public void setType(Type type)
	{
		if (!isAffine())
		    throw new IllegalStateException("setType() cannot be called on a constant threshold");
		if (type == null)
		    throw new NullPointerException("Threshold's type cannot be null");
		this.type = type;
	}
	// CommonAttributes (start)

	/** The id attribute allows to identify the underlying piece of data by a program. */
	private String            id;

	/** The name attribute contains the human readable name of the object or concept. */
	private String            name;

	/**
	 * The mcdaConcept attribute allows to specify to what mcda concept a tag is related. It is used by an algorithm to
	 * make choices which will have an influence on the output. The documentation of the program should therefore
	 * specify, if necessary, what mcdaConcept should be used for the input data. In particular, if an algorithm
	 * requires, among other things, twice the same input tag, they can be differenciated by the mcdaConcept (this is
	 * even mandatory if they are present in the same file, but should be optional if the two tags can be in different
	 * input files, or originate from two different programs). The algorithm should therefore not be too strict on these
	 * mcdaConcepts, as this will reduce the compatibility between the various programs.
	 */
	private String            mcdaConcept;

	public String id()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String name()
	{
		return name;
	}

	public String mcdaConcept()
	{
		return mcdaConcept;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setMcdaConcept(String mcdaConcept)
	{
		this.mcdaConcept = mcdaConcept;
	}

	// CommonAttributes (end)

}
