package org.xmcda;

import java.util.ArrayList;
import java.util.List;

/**
 * xmcda:type:alternatives
 *
 * @author Sébastien Bigaret
 */
public class Alternatives
    extends ReferenceableContainer<Alternative>
    implements CommonAttributes, HasDescription, XMCDARootElement
{
	private static final long serialVersionUID = 1L;

	public static final String TAG = "alternatives";

	public Alternatives()
	{ super(); }

	@Override
	public Class<Alternative> elementClass()
	{
		return Alternative.class;
	}

	public List<Alternative> getActiveAlternatives()
	{
		ArrayList<Alternative> activeAlternatives = new ArrayList<Alternative>(this);
		for (Alternative alternative: this)
			if (!alternative.isActive())
			    activeAlternatives.remove(alternative);
		return activeAlternatives;
	}

	public int getNumberOfActiveAlternatives()
	{
		int nb_active = 0;
		for (Alternative alternative: this)
			if (alternative.isActive())
			    nb_active++;
		return nb_active;
	}

	public void merge(Alternatives alternatives)
	{
		for (Alternative alternative: alternatives)
			this.merge(alternative);
	}

	@Override
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append("[");
		for (Alternative alternative: this)
			sb.append(alternative.toString()).append(", ");
		sb.append("]");
		return sb.toString();
	}

}
