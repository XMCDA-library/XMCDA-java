package org.xmcda;

import org.xmcda.utils.ValueConverters;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author Sébastien Bigaret
 */
public class CategoriesValues<VALUE_TYPE>
	extends LinkedHashMap<Category, LabelledQValues<VALUE_TYPE>>
	implements HasDescription, CommonAttributes, XMCDARootElement
{
	private static final long serialVersionUID = 1L;

	public static final String TAG = "categoriesValues";

	public CategoriesValues() { super(); }

	// CommonAttributes (start)

	/** The id attribute allows to identify the underlying piece of data by a program. */
	private String            id;

	/** The name attribute contains the human readable name of the object or concept. */
	private String            name;

	/**
	 * The mcdaConcept attribute allows to specify to what mcda concept a tag is related. It is used by an algorithm to
	 * make choices which will have an influence on the output. The documentation of the program should therefore
	 * specify, if necessary, what mcdaConcept should be used for the input data. In particular, if an algorithm
	 * requires, among other things, twice the same input tag, they can be differenciated by the mcdaConcept (this is
	 * even mandatory if they are present in the same file, but should be optional if the two tags can be in different
	 * input files, or originate from two different programs). The algorithm should therefore not be too strict on these
	 * mcdaConcepts, as this will reduce the compatibility between the various programs.
	 */
	private String            mcdaConcept;

	public String id()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String name()
	{
		return name;
	}

	public String mcdaConcept()
	{
		return mcdaConcept;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setMcdaConcept(String mcdaConcept)
	{
		this.mcdaConcept = mcdaConcept;
	}

	// CommonAttributes (end)

	// HasDescription (start)

	private Description description;
	public void setDescription(Description description)
	{
		this.description = description;
	}

	public Description getDescription()
	{
		return description;
	}

	// HasDescription (end)

	public LabelledQValues<VALUE_TYPE> put(Category category, QualifiedValue<VALUE_TYPE> qvalue)
	{
		return this.put(category, new LabelledQValues<VALUE_TYPE>(qvalue));
	}

	public LabelledQValues<VALUE_TYPE> put(Category category, VALUE_TYPE value)
	{
		if (value instanceof QualifiedValue)
			return this.put(category, new LabelledQValues<VALUE_TYPE>((QualifiedValue)value));
		if (value instanceof LabelledQValues)
			return super.put(category, (LabelledQValues)value);
		return this.put(category, new LabelledQValues<VALUE_TYPE>(new QualifiedValue(value)));
	}

	/**
	 * Return the value attached to the supplied category. If there is no such value, set the supplied default value
	 * before returning it. NOTE: The labels are not set at all
	 *
	 * @param category
	 *            the category for which the value is requested
	 * @param defaultValue
	 *            the default value to set if there is no value corresponding to the category already.
	 * @return the value attached to the alternative
	 */
	public LabelledQValues<VALUE_TYPE> setDefault(Category category, VALUE_TYPE defaultValue)
	{
		LabelledQValues<VALUE_TYPE> values = this.get(category);
		if (values == null)
		{
			values = Factory.<VALUE_TYPE> labelledQValues();
			values.add(new QualifiedValue<VALUE_TYPE>(defaultValue));
			this.put(category, values);
		}
		return this.get(category);
	}

	public boolean isNumeric()
	{
		for (LabelledQValues<VALUE_TYPE> values: this.values())
		{
			if (!values.isNumeric())
				return false;
		}
		return true;
	}

	public Set<Category> getCategories()
	{
		Set<Category> categories = new LinkedHashSet<Category>();
		for ( Category c : this.keySet() )
			categories.add(c);
		return categories;
	}

	public CategoriesValues<Double> asDouble() throws ValueConverters.ConversionException
	{
		return this.convertTo(Double.class);
	}

	public <U> CategoriesValues<U> convertTo(Class<U> clazz) throws ValueConverters.ConversionException
	{
		for (QualifiedValues<VALUE_TYPE> values: values())
		{
			values.convertTo(clazz);
		}
		return (CategoriesValues<U>) this;
	}

}
