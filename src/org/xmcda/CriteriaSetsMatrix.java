package org.xmcda;

import org.xmcda.utils.Matrix;

/**
 * @author Sébastien Bigaret
 */
public class CriteriaSetsMatrix<CRITERIA_SET_VALUE_TYPE, VALUE_TYPE>
	extends Matrix<CriteriaSet<CRITERIA_SET_VALUE_TYPE>, VALUE_TYPE>
	implements XMCDARootElement
{
    private static final long serialVersionUID = 1L;

	public static final String TAG = "criteriaSetsMatrix";

    public CriteriaSetsMatrix()
    {
    	super();
    }
}
