package org.xmcda;

import java.util.LinkedHashMap;
import java.util.Set;

public class CriteriaSet <VALUE_TYPE>
    extends LinkedHashMap<Criterion, QualifiedValues<VALUE_TYPE>>
    implements HasDescription, Referenceable
{
	private static final long serialVersionUID = 1L;

	public static CreationObserver creationObserver = Referenceable.defaultCreationObserver;

	public CriteriaSet()
	{
		super();
		if ( creationObserver != null )
			creationObserver.objectCreated(this);
	}

	public CriteriaSet(String id)
	{
		super();
		this.setId(id);
		if ( creationObserver != null )
			creationObserver.objectCreated(this);
	}

	public Set<Criterion> getElements()
	{
		return this.keySet();
	}

	public void merge(Referenceable object)
	{
		if (!( object instanceof CriteriaSet ))
		    throw new IllegalArgumentException("Argument should be a CriteriaSet");
        CriteriaSet criteriaSet = (CriteriaSet) object;
		if (!this.id().equals(criteriaSet.id()))
		    throw new IllegalArgumentException("Parameter's id() should be the same as this'");
		this.setDescription(criteriaSet.getDescription());
		this.setName(criteriaSet.name());
		this.setMcdaConcept(criteriaSet.mcdaConcept());
	}

	/**
	 * Two criteria sets are equal if they have the same id
	 */
	@Override
	public boolean equals(Object obj)
	{
		// this is needed where ValuedSet objects are stored as keys, such as in criteriaSetsMatrix
		if (obj == null || !( obj instanceof CriteriaSet ))
		    return false;
		// TODO check type <?,?> ?
		final CriteriaSet<?> criteriaSet = (CriteriaSet<?>) obj;
		return this.id().equals(criteriaSet.id());
	}

	@Override
	public int hashCode()
	{
		return this.id().hashCode();
	}

	@Override
	public String toString()
	{
		StringBuffer str = new StringBuffer("CriterionSet id:");
		str.append(id());
		if (name() != null)
		    str.append(" [name:").append(name()).append("]");
		return str.toString();
	}

	// HasDescription (start)

	private Description description;
	public void setDescription(Description description)
	{
		this.description = description;
	}

	public Description getDescription()
	{
		return description;
	}

	// HasDescription (end)

	// Referenceable (start)

	//   - implements CommonAttributes
	/** The id attribute allows to identify the underlying piece of data by a program. */
	private String            id;

	/** The name attribute contains the human readable name of the object or concept. */
	private String            name;

	/**
	 * The mcdaConcept attribute allows to specify to what mcda concept a tag is related. It is used by an algorithm to
	 * make choices which will have an influence on the output. The documentation of the program should therefore
	 * specify, if necessary, what mcdaConcept should be used for the input data. In particular, if an algorithm
	 * requires, among other things, twice the same input tag, they can be differenciated by the mcdaConcept (this is
	 * even mandatory if they are present in the same file, but should be optional if the two tags can be in different
	 * input files, or originate from two different programs). The algorithm should therefore not be too strict on these
	 * mcdaConcepts, as this will reduce the compatibility between the various programs.
	 */
	private String            mcdaConcept;

	public String id()
	{
		return id;
	}

	void _setId(String id)
	{
		this.id = id;
	}

	public String name()
	{
		return name;
	}

	public String mcdaConcept()
	{
		return mcdaConcept;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setMcdaConcept(String mcdaConcept)
	{
		this.mcdaConcept = mcdaConcept;
	}

	//   Referenceable specific part
	private Object           marker;

	private transient Object container;

	public void setId(String id)
	{
		if (this.container != null)
			throw new IllegalStateException("Cannot change this object's id while it's in a container");
		_setId(id);
	}

	public void setContainer(Object container)
	{
		if ( container == null)
		{
			this.container = null;
			return;
		}
		if ( this.container != null && this.container != container )
			throw new IllegalStateException("this object already belongs to a other alternatives");
		this.container = container;
	}

	public Object getContainer()
	{
		return this.container;
	}

	public void setMarker(Object marker)
	{
		this.marker = marker;
	}

	public Object getMarker()
	{
		return this.marker;
	}

	// Referenceable (end)

}
