package org.xmcda;

public class CategoriesSetsLinearConstraints <CATEGORIES_SET_VALUE_TYPE, VALUE_TYPE>
    extends LinearConstraints<CategoriesSet<CATEGORIES_SET_VALUE_TYPE>, VALUE_TYPE>
	implements XMCDARootElement
{
	private static final long     serialVersionUID = 1L;

	public static final String    TAG = "categoriesSetsLinearConstraints";

	public CategoriesSetsLinearConstraints()
	{ super(); }

}
