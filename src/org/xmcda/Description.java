package org.xmcda;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * @author Sébastien Bigaret
 *
 */
public class Description implements Serializable
{
    private static final long serialVersionUID = 1L;

	// may be empty
	private List<String> authors = new ArrayList<String>();

	// may be null
	private String comment;
	
	private List<String> keywords = new ArrayList<String>();
	
	// may be null
	private BibliographyEntry bibliography;
	
	// may be null
	private GregorianCalendar creationDate;

	private GregorianCalendar lastModificationDate;

	public Description() { super(); }

	/**
     * @return the authors
     */
    public List<String> getAuthors()
    {
    	return authors;
    }

	/**
     * @param authors the authors to set
     */
    public void setAuthors(List<String> authors)
    {
    	this.authors = authors;
    }

	/**
     * @return the comment
     */
    public String getComment()
    {
    	return comment;
    }

	/**
     * @param comment the comment to set
     */
    public void setComment(String comment)
    {
    	this.comment = comment;
    }

	/**
     * @return the keyword
     */
    public List<String> getKeywords()
    {
    	return keywords;
    }

	/**
     * @param keyword the keyword to set
     */
    public void setKeywords(List<String> keywords)
    {
    	this.keywords = keywords;
    }

	/**
     * @return the bibliography
     */
    public BibliographyEntry getBibliography()
    {
    	return bibliography;
    }

	/**
     * @param bibliography the bibliography to set
     */
    public void setBibliography(BibliographyEntry bibliography)
    {
    	this.bibliography = bibliography;
    }

	/**
     * @return the creationDate
     */
    public GregorianCalendar getCreationDate()
    {
    	return creationDate;
    }

	/**
     * @param creationDate the creationDate to set
     */
    public void setCreationDate(GregorianCalendar creationDate)
    {
    	this.creationDate = creationDate;
    }

	/**
     * @return the lastModificationDate
     */
    public GregorianCalendar getLastModificationDate()
    {
    	return lastModificationDate;
    }

	/**
     * @param lastModificationDate the lastModificationDate to set
     */
    public void setLastModificationDate(GregorianCalendar lastModificationDate)
    {
    	this.lastModificationDate = lastModificationDate;
    }
	
}
