<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

  <modelVersion>4.0.0</modelVersion>

  <groupId>org.decision-deck</groupId>
  <artifactId>xmcda</artifactId>
  <version>0.8</version>
  <packaging>jar</packaging>

  <name>xmcda</name>
  <description>XMCDA Java Reference Library</description>
  <inceptionYear>2013</inceptionYear>
  <url>https://www.xmcda.org</url>

  <organization>
    <name>Decision Deck Consortium</name>
    <url>https://www.decision-deck.org/</url>
  </organization>

  <developers>
    <developer>
      <id>sbigaret</id>
      <name>Sébastien Bigaret</name>
      <email>sebastien.bigaret@decision-deck.org</email>
      <roles>
        <role>developer</role>
      </roles>
      <timezone>Europe/Paris</timezone>
    </developer>
  </developers>

  <scm>
    <connection>scm:git:https://gitlab.com/XMCDA-library/XMCDA-java.git</connection>
    <developerConnection>scm:git:git@gitlab.com:XMCDA-library/XMCDA-java.git</developerConnection>
    <url>https://gitlab.com/XMCDA-library/XMCDA-java</url>
  </scm>

  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <timestamp>${maven.build.timestamp}</timestamp>
  </properties>

  <licenses>
    <license>
      <name>European Union Public Licence (EUPL v.1.1)</name>
      <url>https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11</url>
      <distribution>repo</distribution>
    </license>
  </licenses>

  <distributionManagement>
    <snapshotRepository>
      <id>ossrh</id>
      <url>https://oss.sonatype.org/content/repositories/snapshots</url>
    </snapshotRepository>
    <repository>
      <id>ossrh</id>
      <url>https://oss.sonatype.org/service/local/staging/deploy/maven2/</url>
    </repository>
  </distributionManagement>

  <!-- http://maven.apache.org/ref/3.6.0/maven-model/maven.html -->
  <build>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>3.8.0</version>
        <configuration>
          <source>1.8</source>
          <target>1.8</target>
 	  <compilerArgs>
	    <arg>-Xlint:all</arg>
	    <!-- Prevents recompilation due to missing package-info.class -->
	    <arg>-Xpkginfo:always</arg>
	  </compilerArgs>
        </configuration>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-source-plugin</artifactId>
        <version>3.0.1</version>
        <executions>
          <execution>
            <id>attach-sources</id>
            <goals>
              <goal>jar-no-fork</goal>
            </goals>
          </execution>
        </executions>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-javadoc-plugin</artifactId>
        <version>3.0.1</version>
        <configuration>
          <header><![CDATA[<b>XMCDA Java library - <a href="https://www.decision-deck.org/xmcda">XMCDA Standard</a></b>]]></header>
          <bottom><![CDATA[Copyright &#169; {inceptionYear}&#x2013;{currentYear} S&eacute;bastien Bigaret, Patrick Meyer. All rights reserved.]]></bottom>
          <doclint>none</doclint>
          <source>8</source>
        </configuration>
        <executions>
          <execution>
            <id>attach-javadocs</id>
            <goals>
              <goal>jar</goal>
            </goals>
          </execution>
        </executions>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-jar-plugin</artifactId>
        <version>3.1.1</version>
        <configuration>
          <archive>
            <manifest>
              <addClasspath>true</addClasspath>
              <mainClass>org.xmcda.converters.v2_v3.XMCDAConverter</mainClass>
            </manifest>

            <manifestEntries>
              <Specification-Title>XMCDA Java Reference Library</Specification-Title>
              <Specification-Vendor>Sébastien Bigaret &amp; Patrick Meyer</Specification-Vendor>
              <Specification-Version>XMCDA v3.1.0</Specification-Version>
              <Implementation-Title>org.xmcda</Implementation-Title>
              <Implementation-Vendor>Sébastien Bigaret</Implementation-Vendor>
              <Implementation-Version>${project.version}</Implementation-Version>
              <Build-Date>${timestamp}</Build-Date>
            </manifestEntries>
          </archive>
        </configuration>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-shade-plugin</artifactId>
        <version>3.2.1</version>
        <configuration>
          <outputFile>${project.build.directory}/${project.artifactId}-${project.version}.shaded.jar</outputFile>
          <transformers>
            <transformer implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
              <mainClass>org.xmcda.converters.v2_v3.XMCDAConverter</mainClass>
            </transformer>
          </transformers>
        </configuration>
        <executions>
          <execution>
            <phase>package</phase>
            <goals>
              <goal>shade</goal>
            </goals>
          </execution>
        </executions>
      </plugin>

      <plugin>
        <groupId>org.sonatype.plugins</groupId>
        <artifactId>nexus-staging-maven-plugin</artifactId>
        <version>1.6.8</version>
        <extensions>true</extensions>
        <configuration>
          <serverId>ossrh</serverId>
          <nexusUrl>https://oss.sonatype.org/</nexusUrl>
          <!--<autoReleaseAfterClose>true</autoReleaseAfterClose>-->
        </configuration>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-gpg-plugin</artifactId>
        <version>1.6</version>
        <executions>
          <execution>
            <id>sign-artifacts</id>
            <phase>verify</phase>
            <goals>
              <goal>sign</goal>
            </goals>
            <configuration>
              <keyname>0x404D12D302F75873</keyname>
            </configuration>
          </execution>
        </executions>
      </plugin>

    </plugins>

    <sourceDirectory>src</sourceDirectory>

    <resources>
      <resource>
        <directory>.</directory>
        <includes>
          <include>xsd/**</include>
        </includes>
      </resource>
    </resources>

    <testSourceDirectory>tests</testSourceDirectory>
    <testResources>
      <testResource>
        <directory>.</directory>
        <includes>
          <include>data/tests/**</include>
        </includes>
      </testResource>
    </testResources>

  </build>

  <profiles>
    <profile>
      <id>java-9-plus</id>
      <activation>
        <jdk>[1.9,]</jdk>
      </activation>

      <dependencies>

    <dependency>
      <groupId>javax.xml.bind</groupId>
      <artifactId>jaxb-api</artifactId>
      <version>2.3.1</version>
    </dependency>

    <dependency>
      <groupId>org.glassfish.jaxb</groupId>
      <artifactId>jaxb-runtime</artifactId>
      <version>2.3.2</version>
    </dependency>

      </dependencies>
    </profile>
  </profiles>

  <dependencies>

    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>4.13.1</version>
      <scope>test</scope>
    </dependency>

  </dependencies>


</project>
