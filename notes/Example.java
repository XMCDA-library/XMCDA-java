import org.xmcda.XMCDA;
import org.xmcda.parsers.xml.xmcda_v3.XMCDAParser;


public class Example
{

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception
	{
		XMCDA xmcda = new XMCDA();
		XMCDAParser reader = new XMCDAParser();
		reader.readXMCDA(xmcda, "test.xml");
		for (org.xmcda.Alternative alternative: xmcda.alternatives)
		{
			System.out.println(alternative);
		}
		reader.writeXMCDA(xmcda, "test-out.xml");
	}

}
