
# init.
import sys
jar_dir="..."
jar_dir="./"
sys.path.append(jar_dir+"aspectjrt.jar")
sys.path.append(jar_dir+"xmcda.jar")

# start to use it
import org.xmcda.XMCDA
from org.xmcda import XMCDA

# this is an XMCDA "project"
xmcda_input=XMCDA()

# load the input
parser=org.xmcda.parsers.xml.xmcda_v3.XMCDAParser()
parser.readXMCDA(xmcda, "test.xml");

# done: access the fields
# example below
for alternative in xmcda.alternatives:
     print alternative.id()

# performanceTable
perfTable = xmcda_input.performanceTablesList[0]


parser.writeXMCDA(xmcda, "test-out.xml")
