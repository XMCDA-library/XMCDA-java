Copyright Sébastien Bigaret, Patrick Meyer, 2013-2019

La bibliothèque XMCDA-Java est l'implémentation de référence pour la
lecture, l'écriture et la manipulation d'objets stockés dans le format
XMCDA v3

Elle est distribuée sous la Licence Publique de l'Union européenne (EUPL) v1.1.

Vous trouverez dans ce répertoire les versions anglaise et française.

L'EUPL est disponible dans 22 langues officielles de l'Union européenne; pour
plus d'informations, veuillez vous référer à son site web:
https://joinup.ec.europa.eu/community/eupl/home
