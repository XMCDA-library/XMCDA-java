#! /bin/bash

script_dir="$(pwd)/${0%/*}"

cd src/org/xmcda/

for target in Alternatives Criteria Categories; do
  for f in xxxSets.java.tmpl xxxSet.java.tmpl parsers/xml/xmcda_3_0/xxxSetsParser.java.tmpl parsers/xml/xmcda_3_0/xxxSetParser.java.tmpl; do
    target_file="${f/xxx/$target}"
    target_file="${target_file%.tmpl}"
    cp "$f" "${target_file}"
    sed -i -f "${script_dir}/to_${target}.sed" "${target_file}"
  done
done

# xxxValuesParser
for target in Alternatives Criteria Categories ; do
	for f in xxxValues.java.tmpl parsers/xml/xmcda_3_0/xxxsValuesParser.java.tmpl; do
    target_file="${f/xxxs/$target}"
    target_file="${target_file/xxx/$target}"
    target_file="${target_file%.tmpl}"
    cp "$f" "${target_file}"
    sed -i -f "${script_dir}/to_${target}.sed" "${target_file}"
  done
done

# xxxSetsValuesParser
for target in AlternativesSets CriteriaSets CategoriesSets; do
	for f in xxxSetsValues.java.tmpl parsers/xml/xmcda_3_0/xxxSetsValuesParser.java.tmpl; do
    target_file="${f/xxxSets/$target}"
    target_file="${target_file%.tmpl}"
    cp "$f" "${target_file}"
    sed -i -f "${script_dir}/to_${target}.sed" "${target_file}"
  done
done

# OBJECT_ValuesParser
tmpl=parsers/xml/xmcda_3_0/xxxValuesParser.java.tmpl
tmpl_set=parsers/xml/xmcda_3_0/xxxSetValuesParser.java.tmpl
tmpl_sets=parsers/xml/xmcda_3_0/xxxSetsValuesParser.java.tmpl

sed -f     "${script_dir}/to_Alternatives.sed" "$tmpl" \
     > parsers/xml/xmcda_3_0/AlternativeValuesParser.java
sed -f     "${script_dir}/to_AlternativesSets.sed" "$tmpl_set" \
     > parsers/xml/xmcda_3_0/AlternativesSetValuesParser.java
sed -f     "${script_dir}/to_AlternativesSets.sed" "$tmpl_sets" \
     > parsers/xml/xmcda_3_0/AlternativesSetsValuesParser.java

sed -f     "${script_dir}/to_Criteria.sed" "$tmpl" \
     > parsers/xml/xmcda_3_0/CriterionValuesParser.java
sed -f     "${script_dir}/to_CriteriaSets.sed" "$tmpl_set" \
     > parsers/xml/xmcda_3_0/CriteriaSetValuesParser.java
sed -f     "${script_dir}/to_CriteriaSets.sed" "$tmpl_sets" \
     > parsers/xml/xmcda_3_0/CriteriaSetsValuesParser.java

sed -f     "${script_dir}/to_Categories.sed" "$tmpl" \
     > parsers/xml/xmcda_3_0/CategoryValuesParser.java
sed -f     "${script_dir}/to_CategoriesSets.sed" "$tmpl_set" \
     > parsers/xml/xmcda_3_0/CategoriesSetValuesParser.java
sed -f     "${script_dir}/to_CategoriesSets.sed" "$tmpl_sets" \
     > parsers/xml/xmcda_3_0/CategoriesSetsValuesParser.java

# linear constraints
for target in Alternatives Criteria Categories; do
  script="${script_dir}/to_${target}.sed"
  sed -f "$script" xxxLinearConstraints.java.tmpl > ${target}LinearConstraints.java
  sed -f "$script" xxxSetsLinearConstraints.java.tmpl > ${target}SetsLinearConstraints.java
  sed -f "$script" parsers/xml/xmcda_3_0/xxxLinearConstraintsParser.java.tmpl \
                 > parsers/xml/xmcda_3_0/${target}LinearConstraintsParser.java
  sed -f "$script" parsers/xml/xmcda_3_0/xxxSetsLinearConstraintsParser.java.tmpl \
                 > parsers/xml/xmcda_3_0/${target}SetsLinearConstraintsParser.java
done

# matrix
for target in Alternatives Criteria Categories; do
  script="${script_dir}/to_${target}.sed"
  sed -f "$script" xxxMatrix.java.tmpl > ${target}Matrix.java
  sed -f "$script" xxxSetsMatrix.java.tmpl > ${target}SetsMatrix.java
  sed -f "$script" parsers/xml/xmcda_3_0/xxxMatrixParser.java.tmpl \
                 > parsers/xml/xmcda_3_0/${target}MatrixParser.java
  sed -f "$script" parsers/xml/xmcda_3_0/xxxSetsMatrixParser.java.tmpl \
                 > parsers/xml/xmcda_3_0/${target}SetsMatrixParser.java
done

# xxxSetsConverter
for target in Alternatives Criteria Categories; do
  script="${script_dir}/to_${target}.sed"
  sed -f "$script" converters/v2_2_1_v3_0/xxxSetConverter.java.tmpl \
                 > converters/v2_2_1_v3_0/${target}SetConverter.java.tmpl
#  sed -f "$script" src/org/xmcda/converters/v2_2_1_v3_0/xxxSetsConverter.java.tmpl \
#                 > src/org/xmcda/converters/v2_2_1_v3_0/${target}SetsConverter.java.tmpl
done
sed -f ~/Projets/XMCDA-java/tools/to_AlternativesSets.sed xxxSetsConverter.java.tmpl > converters/v2_2_1_v3_0/AlternativesSetsConverter.java
sed -f ~/Projets/XMCDA-java/tools/to_CriteriaSets.sed xxxSetsConverter.java.tmpl > converters/v2_2_1_v3_0/CriteriaSetsConverter.java
sed -f ~/Projets/XMCDA-java/tools/to_CategoriesSets.sed xxxSetsConverter.java.tmpl > converters/v2_2_1_v3_0/CategoriesSetsConverter.java

# xxxComparisonsConverter
for target in Alternatives Criteria Categories; do
  script="${script_dir}/to_${target}.sed"
  sed -f "$script" converters/v2_2_1_v3_0/xxxComparisonsConverter.java.tmpl \
      > converters/v2_2_1_v3_0/${target}ComparisonsConverter.java
done

# xxxMatrixConverter
for target in Alternatives Criteria Categories; do
  script="${script_dir}/to_${target}.sed"
  sed -f "$script" converters/v2_2_1_v3_0/xxxMatrixConverter.java.tmpl \
      > converters/v2_2_1_v3_0/${target}MatrixConverter.java
done


# END
