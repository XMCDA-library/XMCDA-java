#! /bin/bash

script_dir="$(pwd)/${0%/*}"
cd ${script_dir}/..
find src -name '*.java' -exec python ./tools/generate_common_code.py {} \;
find src -name '*.java.tmpl' -exec python ./tools/generate_common_code.py {} \;
./tools/generate_classes_for_sets.sh
